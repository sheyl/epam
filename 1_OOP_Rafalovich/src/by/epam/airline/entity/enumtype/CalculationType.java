package by.epam.airline.entity.enumtype;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public enum CalculationType {
    CAPACITY, PASSENGER_CAPACITY, PRICE
}
