package by.epam.airline.entity.technics.airplane;

/**
 * @version 1.1 08.05.2016
 * @author Dmitry Rafalovich
 */
public class Airfreighter extends CivilAirplane implements Cloneable{

    private String mechanizationOption;

    public Airfreighter(int engineCount, double length, double wingspan, double maxWeight,
                        double maxSpeed, double maxAltitude, double maxFlightRange,
                        double fuelConsumption, String model, String sideNumber,
                        double capacity, String mechanizationOption) {

        super(engineCount, length, wingspan, maxWeight, maxSpeed, maxAltitude,
                maxFlightRange, fuelConsumption, model, sideNumber, capacity);
        this.mechanizationOption = mechanizationOption;
    }

    public Airfreighter() {

    }

    public String getMechanizationOption() {
        return mechanizationOption;
    }

    public void setMechanizationOption(String mechanizationOption) {
        this.mechanizationOption = mechanizationOption;
    }

    @Override
    public Airfreighter clone() throws CloneNotSupportedException {
        return (Airfreighter) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airfreighter)) return false;
        if (!super.equals(o)) return false;

        Airfreighter that = (Airfreighter) o;

        return !(getMechanizationOption() != null ?
                !getMechanizationOption().equals(that.getMechanizationOption()) :
                that.getMechanizationOption() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getMechanizationOption() != null ? getMechanizationOption().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  "\n model = '" + getModel() + '\'' +
                ", sideNumber = '" + getSideNumber() + '\'' +
                ", mechanizationOption = '" + getMechanizationOption() + '\'' +
                ", capacity = " + getCapacity() + " ton";
    }
}
