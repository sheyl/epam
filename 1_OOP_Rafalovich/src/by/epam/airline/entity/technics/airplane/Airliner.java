package by.epam.airline.entity.technics.airplane;

/**
 * @version 1.2 08.05.2016
 * @author Dmitry Rafalovich
 */
public class Airliner extends CivilAirplane implements Cloneable{

    private int passengerCapacity;
    private int personalCount;

    public Airliner(int engineCount, double length, double wingspan, double maxWeight,
                    double maxSpeed, double maxAltitude, double maxFlightRange,
                    double fuelConsumption, String model, String sideNumber,
                    double capacity, int passengerCapacity, int personalCount) {

        super(engineCount, length, wingspan, maxWeight, maxSpeed, maxAltitude,
                maxFlightRange, fuelConsumption, model, sideNumber, capacity);
        this.passengerCapacity = passengerCapacity;
        this.personalCount = personalCount;
    }

    public Airliner() {

    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public int getPersonalCount() {
        return personalCount;
    }

    public void setPersonalCount(int personalCount) {
        this.personalCount = personalCount;
    }

    @Override
    public Airliner clone() throws CloneNotSupportedException {
        return (Airliner) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airliner)) return false;
        if (!super.equals(o)) return false;

        Airliner airliner = (Airliner) o;

        if (getPassengerCapacity() != airliner.getPassengerCapacity()) return false;
        return getPersonalCount() == airliner.getPersonalCount();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getPassengerCapacity();
        result = 31 * result + getPersonalCount();
        return result;
    }

    @Override
    public String toString() {
        return  "\n model = '" + getModel() + '\'' +
                ", sideNumber = '" + getSideNumber() + '\'' +
                ", passengerCapacity = " + passengerCapacity + " peoples"+
                ", personalCount = " + personalCount + " peoples"+
                ", capacity = " + getCapacity() + " ton";
    }
}
