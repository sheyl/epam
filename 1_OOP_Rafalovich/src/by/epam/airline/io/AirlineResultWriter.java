package by.epam.airline.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @version 1.1 17.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlineResultWriter {

    public final static Logger LOG = LogManager.getLogger();

    public static void write(Object obj, String filePath) {

        if (obj != null && filePath != null && !filePath.isEmpty()) {
            File outputFile = new File(filePath);
            try(FileWriter fw = new FileWriter(outputFile,true);
                PrintWriter writer = new PrintWriter(fw)) {
                    writer.println(obj.toString());
            } catch (IOException e){
                LOG.error(e +" Exception in file path " + filePath);
            }
        } else {
            LOG.warn("Null in object or path, or path is empty!");
        }
    }
}
