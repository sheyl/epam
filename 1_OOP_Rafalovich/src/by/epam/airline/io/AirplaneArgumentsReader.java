package by.epam.airline.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirplaneArgumentsReader {

    final static Logger LOG = LogManager.getLogger();

    public static ArrayList<String> read(String fileName) {

        ArrayList<String> data = new ArrayList<>();

        if (fileName != null && !fileName.isEmpty()) {
            try (Scanner scanner = new Scanner(new File(fileName))) {

                while (scanner.hasNextLine()) {
                    String s = scanner.nextLine();
                    if (!s.isEmpty()){
                        data.add(s);
                    }
                }
            } catch (FileNotFoundException e) {
                LOG.error(e + fileName + " - File isn't exist");
            }
        } else {
            LOG.debug("FilePath is null or empty.");
        }
        return data;
    }
}
