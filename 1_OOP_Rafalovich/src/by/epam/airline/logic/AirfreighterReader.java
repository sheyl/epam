package by.epam.airline.logic;

import by.epam.airline.entity.technics.airplane.Airfreighter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirfreighterReader {

    public static Logger LOG = LogManager.getLogger();

    public final static String FIELD_DOES_NOT_FIND = "Field %s doesn't find" +
            " in the string. It is impossible to create object!";
    public static Airfreighter readAirfreighter(String plane) {

        Airfreighter airfreighter = new Airfreighter();

        if (plane.length() > 50) {
            Scanner scanner = new Scanner(plane);

            scanner.findInLine("model:");
            if (scanner.hasNext()) {
                airfreighter.setModel(scanner.next());
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "model:"));
                return null;
            }

            scanner.findInLine("sideNumber:");
            if (scanner.hasNext()) {
                airfreighter.setSideNumber(scanner.next());
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "sideNumber:"));
                return null;
            }

            scanner.findInLine("engineCount:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setEngineCount(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field engineCount must be int.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "engineCount:"));
                return null;
            }

            scanner.findInLine("length:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setLength(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field length must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "length:"));
                return null;
            }

            scanner.findInLine("wingspan:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setWingspan(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field wingspan must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "wingspan:"));
                return null;
            }

            scanner.findInLine("maxWeight:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setMaxWeight(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxWeight must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxWeight:"));
                return null;
            }

            scanner.findInLine("maxSpeed:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setMaxSpeed(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxSpeed must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxSpeed:"));
                return null;
            }

            scanner.findInLine("maxAltitude:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setMaxAltitude(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxAltitude must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxAltitude:"));
                return null;
            }

            scanner.findInLine("maxFlightRange:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setMaxFlightRange(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxFlightRange must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxFlightRange:"));
                return null;
            }

            scanner.findInLine("fuelConsumption:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setFuelConsumption(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field fuelConsumption must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "fuelConsumption:"));
                return null;
            }

            scanner.findInLine("capacity:");
            if (scanner.hasNext()) {
                try {
                    airfreighter.setCapacity(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field capacity must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "capacity:"));
                return null;
            }

            scanner.findInLine("mechanizationOption:");
            if (scanner.hasNext()) {
                airfreighter.setMechanizationOption(scanner.next());
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "mechanizationOption:"));
                return null;
            }

        } else {
            LOG.info("String \"" + plane + "\" so short." +
                    " Check airplane's arguments.");
            return null;
        }
        return airfreighter;
    }
}
