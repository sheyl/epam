package by.epam.airline.logic;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.Airfreighter;
import by.epam.airline.entity.technics.airplane.Airliner;
import by.epam.airline.entity.technics.airplane.CivilAirplane;
import by.epam.airline.io.AirplaneArgumentsReader;
import java.util.ArrayList;

/**
 * @version 1.2 15.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlineCreator {

    public static Airline create(String companyName, String filename1, String filename2) {
        
        Airline airline = new Airline(companyName);
        ArrayList<String> airlinerArguments;
        ArrayList<String> airfreighterArguments;
        ArrayList<CivilAirplane> airplanes = new ArrayList<>();
        
        airlinerArguments = AirplaneArgumentsReader.read(filename1);
        for (String x : airlinerArguments) {
            Airliner time = AirlinerReader.readAirliner(x);
            if (time != null) {
                airplanes.add(time);
            }
        }

        airfreighterArguments = AirplaneArgumentsReader.read(filename2);
        for (String x : airfreighterArguments) {
            Airfreighter time = AirfreighterReader.readAirfreighter(x);
            if (time != null) {
                airplanes.add(time);
            }
        }

        airline.setAirplanes(airplanes);
        return airline;
    }
}
