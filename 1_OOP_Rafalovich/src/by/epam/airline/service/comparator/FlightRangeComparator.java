package by.epam.airline.service.comparator;

import by.epam.airline.entity.technics.airplane.CivilAirplane;

import java.util.Comparator;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class FlightRangeComparator implements Comparator<CivilAirplane> {

    @Override
    public int compare(CivilAirplane o1, CivilAirplane o2) {
        double maxFlightRange1 = o1.getMaxFlightRange();
        double maxFlightRange2 = o2.getMaxFlightRange();
        return (int) ((maxFlightRange1 - maxFlightRange2) / (Math.abs(maxFlightRange1 - maxFlightRange2)));
    }
}
