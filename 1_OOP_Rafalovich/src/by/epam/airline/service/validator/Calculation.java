package by.epam.airline.service.validator;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.Airliner;
import by.epam.airline.entity.technics.airplane.CivilAirplane;

/**
 * @version 1.2 16.05.2016
 * @author Dmitry Rafalovich
 */
public class Calculation {

    public static double calculateCapacity(Airline airline) {

        double  amount = 0.0;

        for (CivilAirplane x: airline.getAirplanes()) {
            amount += x.getCapacity();
        }
        return amount;
    }

    public static int calculatePassengerCapacity(Airline airline) {

        int  amount = 0;

        for (CivilAirplane x: airline.getAirplanes()) {
            if (x.getClass().getSimpleName().equals("Airliner")) {
                amount += ((Airliner) x).getPassengerCapacity();
            }
        }
        return amount;
    }
}
