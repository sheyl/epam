package by.epam.airline.start;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.Airfreighter;
import by.epam.airline.io.AirplaneArgumentsReader;
import by.epam.airline.logic.AirfreighterReader;
import by.epam.airline.logic.AirlineCreator;
import by.epam.airline.logic.AirlineManager;
import by.epam.airline.service.comparator.FlightRangeComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;


/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class Start {

    final static Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {

        String fileName1 = "data/airliners.txt";
        String fileName2 = "data/airfreighters.txt";
        //String fileName2 = "";
        Airline aeroflot = AirlineCreator.create("AEROFLOT", fileName1, fileName2);

        AirlineManager manager = new AirlineManager("Dmitry Rafalovich","plato4ek");
        //AirlineManager manager = new AirlineManager("Dmitry Rafalovich","plato4ek");
        manager.calculate(aeroflot, "pASSENGER_CAPACITY");
        manager.calculate(aeroflot, "cAPACITY");

        // ����� ��������� �������
        //Airfreighter airfreighters  = AirfreighterReader.readAirfreighter(AirplaneArgumentsReader.read("data/airfreighters.txt").get(0));
        //manager.addAirplane(airfreighters,aeroflot);


        //manager.calculate(aeroflot, "price2"); // herhya

        manager.sort(aeroflot, new FlightRangeComparator());
        manager.findForFuelConsumption(aeroflot, 100, 500);
        //AirlineManager man2 = new AirlineManager();

        //aeroflot.setAirplanes(null);

        // checking delete function
        /*manager.deleteAirplane("MP21AC", aeroflot);
        System.out.println(aeroflot);*/

        // checking get function
        /*ArrayList<CivilAirplane> s7 = aeroflot.getAirplanes();
        for (CivilAirplane x: s7) x.setCapacity(125);
        System.out.println(s7);
        System.out.println(aeroflot);*/

        //checking writer
        //AirlineResultWriter.write(aeroflot, "data/result.txt");
        //AirlineResultWriter.write(sumCap,"data/result.txt");
       // AirlineResultWriter.write(sumPass,"data/result.txt");
    }
}
