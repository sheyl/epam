//package by.epam.oop_1_rafalovich.test;

import junit.framework.TestCase;

import java.io.File;

/**
 * @version 1.1 11.05.2016
 * @author Dmitry Rafalovich
 */
public class ReaderTest extends TestCase {

    public void testRead() throws Exception {
        File file = new File("data/airliners.txt");
        assertTrue(file.exists());
    }
}