package by.epam.airline.entity;

import by.epam.airline.entity.technics.airplane.CivilAirplane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

/**
 * @version 1.3 15.05.2016
 * @author Dmitry Rafalovich
 */
public class Airline {

    final static Logger LOG = LogManager.getLogger();
    final static String DEFAULT_NAME = "New Airline ";
    static int defaultNameCount = 0;
    static long idCount = 0;

    private long id;
    private String name;
    private ArrayList<CivilAirplane> airplanes;
    // validation
    public Airline(String name) {

        id = idCount++;
        this.airplanes = new ArrayList<>();
        if ((name != null) && !name.trim().isEmpty()) {
            this.name = name;
        } else {
            defaultNameCount++;
            this.name = DEFAULT_NAME + defaultNameCount;
        }
        LOG.info(this.name + " Airline was created.");
    }

    public Airline() {

        id = idCount++;
        this.airplanes = new ArrayList<>();
        defaultNameCount++;
        this.name = DEFAULT_NAME + defaultNameCount;
        LOG.info(this.name + " Airline was created.");
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CivilAirplane> getAirplanes() {

        ArrayList<CivilAirplane> planes = new ArrayList<>();

        try {
            for (CivilAirplane x: airplanes) planes.add(x.clone());
        } catch (CloneNotSupportedException e) {
            LOG.error("It is impossible to get airplanes. " + e.getMessage());
        }
        return planes;
    }

    public void setAirplanes(ArrayList<CivilAirplane> airplanes) {

        if (airplanes != null) {
           this.airplanes = airplanes;
        } else {
            LOG.warn("It is impossible to set Airplanes.");
        }
    }

    public void addAirplane(CivilAirplane airplane) {
        if (airplanes != null) {
           airplanes.add(airplane);
        } else {
            LOG.warn("It is impossible to add Airplane.");
        }
    }

    public void deleteAirplane(String sideNumber) {

        if (sideNumber != null && !sideNumber.isEmpty()) {
            CivilAirplane airplane = null;

            for (CivilAirplane x: airplanes) {
                if (x.getSideNumber().equalsIgnoreCase(sideNumber)) {
                    airplane = x;
                } else {
                    LOG.warn(String.format("There are no airplanes with" +
                            " sideNumber = %s .",sideNumber));
                }
            }

            if (airplane != null) {
                airplanes.remove(airplane);
            }
        }
    }

    public int getAirplaneCount() {
        return airplanes.size();
    }

    public int getAirlinerCount() {

        int count = 0;
        for (CivilAirplane x: airplanes) {
            if (x.getClass().getSimpleName().equals("Airliner")) {
                count++;
            }
        }
        return count;
    }

    public int getAirfreighterCount() {

        int count = 0;
        for (CivilAirplane x: airplanes) {
            if (x.getClass().getSimpleName().equals("Airfreighter")) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        return name + " Airline: \n" +
                getAirlinerCount() + " airliner(s) " +
                getAirfreighterCount() + " airfreighter(s) \n" +
                "\t Airplanes: \n" + airplanes + "\n";
    }
}
