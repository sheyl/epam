package by.epam.airline.entity.technics.airplane;

/**
 * Abstract class Airplane.
 *
 * @version 1.1 08.05.2016
 * @author Dmitry Rafalovich
 */
public abstract class Airplane implements Cloneable{

    private int engineCount;
    private double length;
    private double wingspan;
    private double maxWeight;
    private double maxSpeed;
    private double maxAltitude;
    private double maxFlightRange;
    private double fuelConsumption;
    private String model;
    private String sideNumber;

    public Airplane(int engineCount, double length, double wingspan, double maxWeight,
                    double maxSpeed, double maxAltitude, double maxFlightRange,
                    double fuelConsumption, String model, String sideNumber) {
        this.engineCount = engineCount;
        this.length = length;
        this.wingspan = wingspan;
        this.maxWeight = maxWeight;
        this.maxSpeed = maxSpeed;
        this.maxAltitude = maxAltitude;
        this.maxFlightRange = maxFlightRange;
        this.fuelConsumption = fuelConsumption;
        this.model = model;
        this.sideNumber = sideNumber;
    }

    public Airplane() {

    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSideNumber() {
        return sideNumber;
    }

    public void setSideNumber(String sideNumber) {
        this.sideNumber = sideNumber;
    }

    public int getEngineCount() {
        return engineCount;
    }

    public void setEngineCount(int engineCount) {
        this.engineCount = engineCount;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWingspan() {
        return wingspan;
    }

    public void setWingspan(double wingspan) {
        this.wingspan = wingspan;
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getMaxAltitude() {
        return maxAltitude;
    }

    public void setMaxAltitude(double maxAltitude) {
        this.maxAltitude = maxAltitude;
    }

    public double getMaxFlightRange() {
        return maxFlightRange;
    }

    public void setMaxFlightRange(double maxFlightRange) {
        this.maxFlightRange = maxFlightRange;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    protected Airplane clone() throws CloneNotSupportedException {
        return (Airplane) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airplane)) return false;

        Airplane airplane = (Airplane) o;

        if (getEngineCount() != airplane.getEngineCount()) return false;
        if (Double.compare(airplane.getLength(), getLength()) != 0) return false;
        if (Double.compare(airplane.getWingspan(), getWingspan()) != 0) return false;
        if (Double.compare(airplane.getMaxWeight(), getMaxWeight()) != 0) return false;
        if (Double.compare(airplane.getMaxSpeed(), getMaxSpeed()) != 0) return false;
        if (Double.compare(airplane.getMaxAltitude(), getMaxAltitude()) != 0) return false;
        if (Double.compare(airplane.getMaxFlightRange(), getMaxFlightRange()) != 0) return false;
        if (Double.compare(airplane.getFuelConsumption(), getFuelConsumption()) != 0) return false;
        if (getModel() != null ? !getModel().equals(airplane.getModel()) : airplane.getModel() != null) return false;
        return !(getSideNumber() != null ? !getSideNumber().equals(airplane.getSideNumber()) : airplane.getSideNumber() != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getEngineCount();
        temp = Double.doubleToLongBits(getLength());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getWingspan());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxWeight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxSpeed());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxAltitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxFlightRange());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getFuelConsumption());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getModel() != null ? getModel().hashCode() : 0);
        result = 31 * result + (getSideNumber() != null ? getSideNumber().hashCode() : 0);
        return result;
    }
}
