package by.epam.airline.entity.technics.airplane;

/**
 * Abstract class CivilAirplane.
 *
 * @version 1.1 08.05.2016
 * @author Dmitry Rafalovich
 */
public abstract class CivilAirplane extends Airplane implements Cloneable{

    private double capacity;

    public CivilAirplane(int engineCount, double length, double wingspan, double maxWeight,
                         double maxSpeed, double maxAltitude, double maxFlightRange,
                         double fuelConsumption, String model, String sideNumber,
                         double capacity) {

        super(engineCount, length, wingspan, maxWeight, maxSpeed, maxAltitude,
                maxFlightRange, fuelConsumption, model, sideNumber);
        this.capacity = capacity;
    }

    public CivilAirplane() {

    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public CivilAirplane clone() throws CloneNotSupportedException {
        return (CivilAirplane) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CivilAirplane)) return false;
        if (!super.equals(o)) return false;

        CivilAirplane that = (CivilAirplane) o;

        return Double.compare(that.getCapacity(), getCapacity()) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(getCapacity());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
