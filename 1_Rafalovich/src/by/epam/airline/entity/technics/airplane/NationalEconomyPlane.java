package by.epam.airline.entity.technics.airplane;

/**
 * @version 1.1 08.05.2016
 * @author Dmitry Rafalovich
 */
public class NationalEconomyPlane extends CivilAirplane implements Cloneable{

    private String function;

    public NationalEconomyPlane(int engineCount, double length, double wingspan, double maxWeight,
                                double maxSpeed, double maxAltitude, double maxFlightRange,
                                double fuelConsumption, String model, String sideNumber,
                                double capacity, String function) {

        super(engineCount, length, wingspan, maxWeight, maxSpeed, maxAltitude,
                maxFlightRange, fuelConsumption, model, sideNumber, capacity);
        this.function = function;
    }

    public NationalEconomyPlane() {

    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    @Override
    public NationalEconomyPlane clone() throws CloneNotSupportedException {
        return (NationalEconomyPlane) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NationalEconomyPlane)) return false;
        if (!super.equals(o)) return false;

        NationalEconomyPlane that = (NationalEconomyPlane) o;

        if (!getModel().equals(that.getModel())) return false;
        if (!getSideNumber().equals(that.getSideNumber())) return false;
        return getFunction().equals(that.getFunction());

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getModel().hashCode();
        result = 31 * result + getSideNumber().hashCode();
        result = 31 * result + getFunction().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "NationalEconomyPlane{" +
                "model='" + getModel() + '\'' +
                ", sideNumber='" + getSideNumber() + '\'' +
                ", function='" + function + '\'' +
                '}';
    }
}
