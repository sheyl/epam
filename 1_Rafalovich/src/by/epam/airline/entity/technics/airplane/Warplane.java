package by.epam.airline.entity.technics.airplane;

import java.util.HashMap;

/**
 * Abstract class Warplane.
 *
 * @version 1.1 09.05.2016
 * @author Dmitry Rafalovich
 */
public abstract class Warplane extends Airplane{

    private HashMap<String, Integer> arming = new HashMap<>();

    public Warplane(int engineCount, double length, double wingspan, double maxWeight,
                    double maxSpeed, double maxAltitude, double maxFlightRange,
                    double fuelConsumption, String model, String sideNumber) {

        super(engineCount, length, wingspan, maxWeight, maxSpeed,
                maxAltitude, maxFlightRange, fuelConsumption,
                model, sideNumber);
    }

    public Warplane() {

    }

    public HashMap<String, Integer> getArming() {
        return arming;
    }

    public void arm(String weapon, int count) {
        arming.put(weapon, count);
    }

    public abstract void attack();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Warplane)) return false;
        if (!super.equals(o)) return false;

        Warplane warplane = (Warplane) o;

        return getArming().equals(warplane.getArming());

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getArming().hashCode();
        return result;
    }
}
