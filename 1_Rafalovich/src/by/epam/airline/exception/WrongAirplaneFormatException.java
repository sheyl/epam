package by.epam.airline.exception;

/**
 * @version 1.1 20.05.2016
 * @author Dmitry Rafalovich
 */
public class WrongAirplaneFormatException extends Exception {

    public WrongAirplaneFormatException() {
    }

    public WrongAirplaneFormatException(String message) {
        super(message);
    }

    public WrongAirplaneFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongAirplaneFormatException(Throwable cause) {
        super(cause);
    }
}
