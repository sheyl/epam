package by.epam.airline.io;

import by.epam.airline.exception.WrongFileNameException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirplaneArgumentsReader  {

    final static Logger LOG = LogManager.getLogger();

    public static ArrayList<String> read(String fileName) throws WrongFileNameException{

        ArrayList<String> data = new ArrayList<>();
        if (fileName != null && !fileName.trim().isEmpty()) {
            try (Scanner scanner = new Scanner(new File(fileName))) {
                while (scanner.hasNextLine()) {
                    String s = scanner.nextLine();
                    if (!s.isEmpty()){
                        data.add(s);
                    }
                }
            } catch (FileNotFoundException e) {
                throw new WrongFileNameException(fileName + " - File isn't exist. " ,e);
            }
        } else {
            throw new WrongFileNameException("FilePath is null or empty.");
        }
        return data;
    }
}
