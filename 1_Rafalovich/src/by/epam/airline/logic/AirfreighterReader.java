package by.epam.airline.logic;

import static by.epam.airline.logic.CivilAirplaneReader.FIELD_NOT_FOUND;
import by.epam.airline.entity.technics.airplane.Airfreighter;
import by.epam.airline.exception.WrongAirplaneFormatException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirfreighterReader {

    static Logger LOG = LogManager.getLogger();

    public static Airfreighter readAirfreighter(String plane) throws WrongAirplaneFormatException{

        Airfreighter airplane = new Airfreighter();

        if (plane.length() > 50) {

            Scanner scanner = new Scanner(plane);
            CivilAirplaneReader.createCivilAirplane(scanner, airplane);
            String word;

            word = scanner.findInLine("mechanizationOption:");
            if (word != null && scanner.hasNext()) {
                airplane.setMechanizationOption(scanner.next());
            } else {
                throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "mechanizationOption:"));
            }

        } else {
            throw new WrongAirplaneFormatException("String \"" + plane +
                    "\" so short. Check airplane's arguments.");
        }
        return airplane;
    }
}
