package by.epam.airline.logic;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.CivilAirplane;
import by.epam.airline.exception.WrongAirplaneFormatException;
import by.epam.airline.exception.WrongFileNameException;
import by.epam.airline.io.AirplaneArgumentsReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

/**
 * @version 1.2 15.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlineCreator {

    final static Logger LOG = LogManager.getLogger();

    public static Airline create(String companyName, String filename1, String filename2) {
        
        Airline airline = new Airline(companyName);
        ArrayList<String> airlinerArguments;
        ArrayList<String> airfreighterArguments;
        ArrayList<CivilAirplane> airplanes = new ArrayList<>();

        try {
            airlinerArguments = AirplaneArgumentsReader.read(filename1);
            for (int i = 0; i < airlinerArguments.size() ; i++) {
                int number = i + 1;
                try {
                    airplanes.add(AirlinerReader.readAirliner(airlinerArguments.get(i)));

                } catch (WrongAirplaneFormatException e) {
                    LOG.error("Airliner was not created. String № " + number + ": " + e.getMessage());
                }
            }
        } catch (WrongFileNameException e) {
            LOG.error("Airliners were not created. " + e.getMessage());
        }

        try {
            airfreighterArguments = AirplaneArgumentsReader.read(filename2);
            for (int i = 0; i < airfreighterArguments.size(); i++) {
                int number = i + 1;
                try {
                    airplanes.add(AirfreighterReader.readAirfreighter(airfreighterArguments.get(i)));
                } catch (WrongAirplaneFormatException e) {
                    LOG.error("Airfreighter was not created. String № " + number + ": " + e.getMessage());
                }
            }
        } catch (WrongFileNameException e) {
            LOG.error("Airfreighters were not created. " + e.getMessage());
        }

        airline.setAirplanes(airplanes);
        return airline;
    }
}
