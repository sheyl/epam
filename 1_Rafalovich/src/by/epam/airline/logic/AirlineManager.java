package by.epam.airline.logic;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.CivilAirplane;
import by.epam.airline.entity.enumtype.CalculationType;
import by.epam.airline.io.AirlineResultWriter;
import by.epam.airline.service.validator.Calculation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @version 1.2 16.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlineManager {

    final static Logger LOG = LogManager.getLogger();
    private static int defaultNameCount = 0;
    private final String DEFAULT_NAME = "New Manager ";
    private final String DEFAULT_PATH =  "data/default_result.txt";


    private String name;
    private String path;

    public AirlineManager(String name, String resultFileName) {

        if ((name != null) && !name.trim().isEmpty()) {
            this.name = name;
        } else {
            defaultNameCount++;
            this.name = DEFAULT_NAME + defaultNameCount;
        }

        if ((resultFileName != null) && !resultFileName.trim().isEmpty()) {
            this.path = resultFileName;
        } else {
            this.path = DEFAULT_PATH ;
        }
        LOG.info(String.format("Manager name = %s resultFileName = %s", this.name, path));
    }

    public AirlineManager() {
        defaultNameCount++;
        this.name = DEFAULT_NAME + defaultNameCount;
        this.path = DEFAULT_PATH ;
        LOG.info(String.format("Manager name = %s resultFileName = %s", name, path));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void renameCompany(Airline airline, String name) {

        if ((name != null) && !name.trim().isEmpty()) {
            airline.setName(name);
        } else {
            LOG.warn("Name can not be empty or null.");
        }

    }

    public void addAirplane(CivilAirplane airplane, Airline airline) {

        if (airplane != null && airline != null) {
            airline.addAirplane(airplane);
        } else {
            LOG.warn("It is impossible to add Airplane.");
        }

    }

    public void deleteAirplane(String sideNumber, Airline airline) {

        if (airline != null && sideNumber != null && !sideNumber.trim().isEmpty()) {
            airline.deleteAirplane(sideNumber);
        } else {
            LOG.warn("It is impossible to delete Airplane.");
        }

    }

    public void calculate(Airline airline, String type) {

        if (airline != null && type != null && !type.trim().isEmpty()) {
            String formatType = type.trim().toUpperCase();
            double amount = -1;
            CalculationType calculationType;

            try {
                calculationType = CalculationType.valueOf(formatType);
            } catch (IllegalArgumentException e) {
                LOG.error("Wrong calculation type. " + e.getMessage());
                return;
            }
            switch (calculationType) {
                case CAPACITY:
                    amount = Calculation.calculateCapacity(airline);
                    AirlineResultWriter.write("Capacity = " + amount + " ton", path);
                    break;
                case PASSENGER_CAPACITY:
                    amount =  (double) Calculation.calculatePassengerCapacity(airline);
                    AirlineResultWriter.write("PassengerCapacity = " + amount + " peoples", path);
                    break;
                default:
                    AirlineResultWriter.write(amount, path);
                    LOG.warn("Wrong calculation type.");
            }
        } else {
            LOG.warn("It is impossible to calculate airline.");
        }
    }

    public void sort(Airline airline, Comparator<CivilAirplane> comparator) {

        if (airline != null && comparator != null) {
            ArrayList<CivilAirplane> civilAirplanes = airline.getAirplanes();
            Collections.sort(civilAirplanes, comparator);
            AirlineResultWriter.write("Sorted list of planes:", path);
            AirlineResultWriter.write(civilAirplanes, path);
        } else {
            LOG.warn("It is impossible to sort airline.");
        }
    }

    public void findForFuelConsumption(Airline airline, double min, double max) {

        ArrayList<CivilAirplane> airplanes = new ArrayList<>();

        if (airline != null) {
            for (CivilAirplane x: airline.getAirplanes()) {
                if ((x.getFuelConsumption() > min) && (x.getFuelConsumption() < max)) {
                    try {
                        airplanes.add(x.clone());
                    } catch (CloneNotSupportedException e) {
                        LOG.error(e + "It is impossible to get airplanes for indForFuelConsumption method.");
                    }
                }
            }
            if (airplanes.size() > 0) {
                AirlineResultWriter.write("Founding airplanes:", path);
                AirlineResultWriter.write(airplanes, path);
            } else {
                AirlineResultWriter.write( String.format("Has not been found fuel consumption in [%f, %f]",min,max), path);
            }
        } else {
            LOG.warn("It is impossible to find For Fuel Consumption.");
        }
    }
}
