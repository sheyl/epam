package by.epam.airline.logic;

import static by.epam.airline.logic.CivilAirplaneReader.FIELD_NOT_FOUND;
import by.epam.airline.entity.technics.airplane.Airliner;
import by.epam.airline.exception.WrongAirplaneFormatException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlinerReader {

    final static Logger LOG = LogManager.getLogger();

    public static Airliner readAirliner(String plane) throws WrongAirplaneFormatException {

        Airliner airplane = new Airliner();

        if (plane.length() > 50) {

            Scanner scanner = new Scanner(plane);
            CivilAirplaneReader.createCivilAirplane(scanner, airplane);
            String word;

            word = scanner.findInLine("passengerCapacity:");
            if (word != null && scanner.hasNext()) {
                try {
                    airplane.setPassengerCapacity(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    throw new WrongAirplaneFormatException("Field passengerCapacity must be int. ", e);
                }
            } else {
                throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "passengerCapacity:"));
            }

            word = scanner.findInLine("personalCount:");
            if (word != null && scanner.hasNext()) {
                try {
                    airplane.setPersonalCount(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    throw new WrongAirplaneFormatException("Field personalCount must be int. ", e);
                }
            } else {
                throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "personalCount:"));
            }

        } else {
            LOG.warn("String \"" + plane + "\" so short." +
                    " Check airplane's arguments.");
            throw new WrongAirplaneFormatException();
        }
        return airplane;
    }
}
