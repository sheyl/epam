package by.epam.airline.logic;

import by.epam.airline.entity.technics.airplane.CivilAirplane;
import by.epam.airline.exception.WrongAirplaneFormatException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * @version 1.1 09.06.2016
 * @author Dmitry Rafalovich
 */
public class CivilAirplaneReader {

    public final static String FIELD_NOT_FOUND = "Field %s not found" +
            " in the string. It is impossible to create object!";
    final static Logger LOG = LogManager.getLogger();

    public static void createCivilAirplane(Scanner scanner, CivilAirplane airplane) throws WrongAirplaneFormatException{

        String word;

        word = scanner.findInLine("model:");
        if (word != null && scanner.hasNext()) {
            airplane.setModel(scanner.next());
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "model:"));
        }

        word = scanner.findInLine("sideNumber:");
        if (word != null && scanner.hasNext()) {
            airplane.setSideNumber(scanner.next());
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "sideNumber:"));
        }

        word = scanner.findInLine("engineCount:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setEngineCount(Integer.parseInt(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field engineCount must be int. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "engineCount:"));
        }

        word = scanner.findInLine("length:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setLength(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field length must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "length:"));
        }

        word = scanner.findInLine("wingspan:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setWingspan(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field wingspan must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "wingspan:"));
        }

        word = scanner.findInLine("maxWeight:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setMaxWeight(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field maxWeight must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "maxWeight:"));
        }

        word = scanner.findInLine("maxSpeed:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setMaxSpeed(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field maxSpeed must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "maxSpeed:"));
        }

        word = scanner.findInLine("maxAltitude:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setMaxAltitude(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field maxAltitude must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "maxAltitude:"));
        }

        word = scanner.findInLine("maxFlightRange:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setMaxFlightRange(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field maxFlightRange must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "maxFlightRange:"));
        }

        word = scanner.findInLine("fuelConsumption:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setFuelConsumption(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field fuelConsumption must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "fuelConsumption:"));
        }

        word = scanner.findInLine("capacity:");
        if (word != null && scanner.hasNext()) {
            try {
                airplane.setCapacity(Double.parseDouble(scanner.next()));
            } catch (NumberFormatException e) {
                throw new WrongAirplaneFormatException("Field capacity must be double. ", e);
            }
        } else {
            throw new WrongAirplaneFormatException(String.format(FIELD_NOT_FOUND, "capacity:"));
        }

    }
}
