package by.epam.airline.start;

import by.epam.airline.entity.Airline;
import by.epam.airline.logic.AirlineCreator;
import by.epam.airline.logic.AirlineManager;
import by.epam.airline.service.comparator.FlightRangeComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class Start {

    final static Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {

        String fileName1 = "data/airliners.txt";
        String fileName2 = "data/airfreighters.txt";
        Airline aeroflot = AirlineCreator.create("   ", fileName1, fileName2);
        AirlineManager manager = new AirlineManager("Dmitry Rafalovich","data/result.txt");
        manager.calculate(aeroflot, "pASsENgER_CAPACITY");
        manager.calculate(aeroflot, "cAPaCITY");
        manager.sort(aeroflot, new FlightRangeComparator());
        manager.findForFuelConsumption(aeroflot, 100, 500);

        // checking delete function
        /*manager.deleteAirplane("MP21AC", aeroflot);
        System.out.println(aeroflot);*/

        // checking get function
        /*ArrayList<CivilAirplane> s7 = aeroflot.getAirplanes();
        for (CivilAirplane x: s7) x.setCapacity(125);
        System.out.println(s7);
        System.out.println(aeroflot);*/

        //checking writer
        //AirlineResultWriter.write(aeroflot, "data/result.txt");
        //AirlineResultWriter.write(sumCap,"data/result.txt");
       // AirlineResultWriter.write(sumPass,"data/result.txt");
    }
}
