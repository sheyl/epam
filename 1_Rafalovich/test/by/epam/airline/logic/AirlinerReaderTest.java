package by.epam.airline.logic;

import by.epam.airline.entity.technics.airplane.Airliner;
import junit.framework.TestCase;


public class AirlinerReaderTest extends TestCase {


    public void testReadAirliner() throws Exception {

        Airliner expected = new Airliner();
        expected.setPersonalCount(6);
        expected.setCapacity(66.050);
        expected.setPassengerCapacity(400);
        expected.setMaxFlightRange(11200);
        expected.setEngineCount(2);
        expected.setFuelConsumption(1450);
        expected.setLength(73.9);
        expected.setMaxAltitude(13.1);
        expected.setMaxSpeed(905);
        expected.setMaxWeight(317.5);
        expected.setModel("Boeing-777-200");
        expected.setSideNumber("MP20AC");
        expected.setWingspan(64.8);

        String check = "model: Boeing-777-200 sideNumber: MP20AC" +
                " engineCount: 2 length: 73.9 wingspan: 64.8 maxWeight: 317.5" +
                " maxSpeed: 905 maxAltitude: 13.1 maxFlightRange: 11200" +
                " fuelConsumption: 1450 capacity: 66.050 passengerCapacity: 400" +
                " personalCount: 6";

        Airliner actual = AirlinerReader.readAirliner(check);

        assertEquals(expected, actual);
    }
}