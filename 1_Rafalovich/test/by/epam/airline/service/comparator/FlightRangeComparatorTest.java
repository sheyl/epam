package by.epam.airline.service.comparator;

import by.epam.airline.entity.technics.airplane.Airfreighter;
import by.epam.airline.entity.technics.airplane.Airliner;
import junit.framework.TestCase;

/**
 * @author Dmitry Rafalovich
 */
public class FlightRangeComparatorTest extends TestCase {



    public void testCompare() throws Exception {

        FlightRangeComparator comparator = new FlightRangeComparator();
        Airfreighter airfreighter = new Airfreighter();
        airfreighter.setMaxFlightRange(2000);
        Airliner airliner = new Airliner();
        airliner.setMaxFlightRange(1500);
        assertEquals(1, comparator.compare(airfreighter,airliner));
        assertEquals(0, comparator.compare(airfreighter,airfreighter));
        assertEquals(-1, comparator.compare(airliner,airfreighter));
    }
}