package by.epam.airline.service.validator;

import by.epam.airline.entity.Airline;
import by.epam.airline.entity.technics.airplane.Airfreighter;
import by.epam.airline.entity.technics.airplane.Airliner;
import by.epam.airline.entity.technics.airplane.CivilAirplane;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author Dmitry Rafalovich
 */
@RunWith(Parameterized.class)
public class CalculationTest extends TestCase{

    @Parameterized.Parameters
    public static Collection data() {

        Airline airline = new Airline();
        ArrayList <CivilAirplane> list = new ArrayList<>();
        Airfreighter airfreighter = new Airfreighter();
        airfreighter.setCapacity(100);
        Airliner airliner = new Airliner();
        airliner.setCapacity(20);
        airliner.setPassengerCapacity(300);

        for (int i = 0; i < 5; i++) {
            list.add(airfreighter);
            list.add(airliner);
        }

        airline.setAirplanes(list);

        return Arrays.asList(new Object[][]{
                {airline, 600, 1500}
        });
    }

    Airline checkingAirline;
    double cap;
    int passCap;

    public CalculationTest(Airline checkingAirline, double cap, int passCap) {
        this.checkingAirline = checkingAirline;
        this.cap = cap;
        this.passCap = passCap;
    }

    @Test
    public void testCalculateCapacity() throws Exception {
        assertEquals(cap, Calculation.calculateCapacity(checkingAirline));
    }

    @Test
    public void testCalculatePassengerCapacity() throws Exception {
        assertEquals(passCap, Calculation.calculatePassengerCapacity(checkingAirline));
    }

    public static void main(String[] args) {
        JUnitCore core = new JUnitCore();
        core.run(CalculationTest.class);
    }
}