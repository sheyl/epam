package by.epam.metoda.blinov_mnogopoto4nost.Barrier;


import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CyclicBarrier;

public class Auction {

    private ArrayList<Bid> bids; // список к онкурирующих предложений
    private CyclicBarrier barrier;
    public final int BIDS_NUMBER = 5; // размер барьера

    public Auction() {
        bids = new ArrayList<Bid>();
        // барьер инициализируется потоком определения победителя
        barrier = new CyclicBarrier(this.BIDS_NUMBER, () -> {
            Bid winner = Auction.this.defineWinner();
            System.out.println("Bid #" + winner.getBidId() + ", price:" + winner.getPrice() + " win!");
        });
    }

    public CyclicBarrier getBarrier() {
        return barrier;
    }

    public boolean add(Bid e) {
        return bids.add(e);
    }

    public Bid defineWinner() {
        return Collections.max(bids, (ob1, ob2) -> ob1.getPrice() - ob2.getPrice());
    }
}
