package by.epam.metoda.blinov_mnogopoto4nost.Barrier;


import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Bid extends Thread {

    private Integer bidId;
    private int price;
    private CyclicBarrier barrier;

    public Bid(Integer bidId, int price, CyclicBarrier barrier) {
        this.bidId = bidId;
        this.price = price;
        this.barrier = barrier;
    }

    public Integer getBidId() {
        return bidId;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public void run() {
        try {
            System.out.println("Client " + this.bidId + " specifies a price.");
            Thread.sleep(new Random().nextInt(3000)); // время на раздумье
            // определение уровня повышения цены
            int delta = new Random().nextInt(50);
            price += delta;
            System.out.println("Bid " + this.bidId + " : " + price);
            this.barrier.await(new Random().nextInt(4), TimeUnit.SECONDS); //	остановка	у	барьера
            System.out.println("Continue to work..."); // проверить кто выиграл
   // и оплатить в случае победы ставки
        } catch (BrokenBarrierException | InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println("Hello deadLock from " + bidId);
        }

    }
}
