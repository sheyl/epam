package by.epam.metoda.blinov_mnogopoto4nost.Executor;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Created by admin on 22.05.2016.
 */
public class BaseCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        String product = ProductList.getProduct();
        String result = null;
        if (product != null) {
            result = product + " done";
        } else {
            result  = "productList is empty";
        }
        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println(result);
        return result;
    }
}
