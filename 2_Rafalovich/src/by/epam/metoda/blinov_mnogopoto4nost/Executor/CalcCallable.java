package by.epam.metoda.blinov_mnogopoto4nost.Executor;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by admin on 22.05.2016.
 */
public class CalcCallable implements Callable<Number> {



    @Override
    public Number call() throws Exception {
        Thread.sleep(2000);
        return new Random().nextGaussian();
    }
}
