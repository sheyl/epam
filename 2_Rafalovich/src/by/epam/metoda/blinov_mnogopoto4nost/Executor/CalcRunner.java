package by.epam.metoda.blinov_mnogopoto4nost.Executor;

import java.util.concurrent.*;

/**
 * Created by admin on 22.05.2016.
 */
public class CalcRunner {

    public static void main(String[] args)  {
        ExecutorService es = Executors.newSingleThreadExecutor();
        Future<Number> future = es.submit(new CalcCallable());
        es.shutdown();
        try {
            if (future.isDone()) {
                System.out.println(future.get(500, TimeUnit.MILLISECONDS));
            } else {
                System.out.println("time out");
            }

        }catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
