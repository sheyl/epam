package by.epam.metoda.blinov_mnogopoto4nost.Executor;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by admin on 22.05.2016.
 */
public class RunExecutor {
    public static void main(String[] args) throws Exception{
        
        ArrayList<Future<String>> list = new ArrayList<Future<String>>();
        ExecutorService es = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 7; i++) {
            list.add(es.submit(new BaseCallable()));

        }
        System.out.println("do shutdown");
        es.shutdown();
        //Thread.sleep(5000);
        System.out.println("posle shutdown");
        for (Future<String> x: list) {
            System.out.println(x.get() + " result fixed");
        }
    }
}
