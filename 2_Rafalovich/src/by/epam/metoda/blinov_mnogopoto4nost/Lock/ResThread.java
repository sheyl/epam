package by.epam.metoda.blinov_mnogopoto4nost.Lock;

import java.util.Random;

/**
 * Created by admin on 22.05.2016.
 */
public class ResThread extends Thread {

    private DoubleResource resource;

    public ResThread(String name, DoubleResource resource) {
        super(name);
        this.resource = resource;
    }

    public void run() {

        for (int i = 1; i < 4; i++) {
            if (new Random().nextInt(2) > 0) {
                resource.adding(getName(), i);
            } else {
                resource.deleting();
            }
        }
    }
}
