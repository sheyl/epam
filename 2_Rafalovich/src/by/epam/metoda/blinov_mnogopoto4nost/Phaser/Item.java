package by.epam.metoda.blinov_mnogopoto4nost.Phaser;

/**
 * Created by admin on 22.05.2016.
 */
public class Item {

    private int registrationNumber;

    public Item(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }
}
