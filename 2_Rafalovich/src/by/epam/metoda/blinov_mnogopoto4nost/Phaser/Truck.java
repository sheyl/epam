package by.epam.metoda.blinov_mnogopoto4nost.Phaser;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Phaser;

/**
 * Created by admin on 22.05.2016.
 */
public class Truck implements Runnable {

    private Phaser phaser;
    private String number;
    private int capacity;
    private Storage storageFrom;
    private Storage storageTo;
    private Queue<Item> bodyStorage;

    public Truck(Phaser phaser, String number, int capacity, Storage storageFrom, Storage storageTo) {
        this.number = number;
        this.phaser = phaser;
        this.capacity = capacity;
        this.storageFrom = storageFrom;
        this.storageTo = storageTo;
        this.bodyStorage = new ArrayDeque<>(capacity);
        this.phaser.register();
    }

    @Override
    public void run() {

            loadTruck();
            phaser.arriveAndAwaitAdvance();
            goTruck();
            phaser.arriveAndAwaitAdvance();
            unloadTruck();
            phaser.arriveAndDeregister();
    }

    private void loadTruck() {
        for (int i = 0; i < capacity; i++) {
            Item g = storageFrom.getGood();
            if (g == null) { // если в хранилище больше нет товара,
                // загрузка грузовика прерывается
                return;
            }
            bodyStorage.add(g);
            System.out.println("Грузовик " + number + " загрузил товар №"
                    + g.getRegistrationNumber());

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void unloadTruck() {

        int size = bodyStorage.size();

        for (int i = 0; i < size; i++) {

            Item g = bodyStorage.poll();
            storageTo.setGood(g);
            System.out.println("Грузовик " + number + " разгрузил товар №"
                                  + g.getRegistrationNumber());
        }

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void goTruck() {

        try {
            Thread.sleep(new Random(100).nextInt(500));
            System.out.println("Грузовик " + number + " начал поездку.");
            Thread.sleep(new Random(100).nextInt(1000));
            System.out.println("Грузовик " + number + " завершил поездку.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
