package by.epam.metoda.blinov_mnogopoto4nost;

public class PriorityRunner {
  public static void main(String[ ] args) {
      ThreadGroup threadGroup = new ThreadGroup("123");
      threadGroup.setMaxPriority(10);
    PriorThread min = new PriorThread("Min");
      System.out.println(min.getThreadGroup());
    PriorThread max = new PriorThread("Max");
      System.out.println(max.getThreadGroup());
    PriorThread norm = new PriorThread("Norm");
    min.setPriority(Thread.MIN_PRIORITY); // 1
    max.setPriority(Thread.MAX_PRIORITY); // 10
    norm.setPriority(Thread.NORM_PRIORITY); // 5
   min.start();
   norm.start();
   max.start();
  }
}