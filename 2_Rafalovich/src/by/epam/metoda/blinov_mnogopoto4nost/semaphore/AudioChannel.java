package by.epam.metoda.blinov_mnogopoto4nost.semaphore;

/**
 * Created by admin on 21.05.2016.
 */
public class AudioChannel {
    private int channelId;

    public AudioChannel(int channelId) {
        this.channelId = channelId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public void using() {

        try {
            // использование канала
            Thread.sleep(new java.util.Random().nextInt(500));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
