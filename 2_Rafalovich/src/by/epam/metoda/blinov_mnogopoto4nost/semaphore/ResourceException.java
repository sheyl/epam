package by.epam.metoda.blinov_mnogopoto4nost.semaphore;

/**
 * Created by admin on 21.05.2016.
 */
public class ResourceException extends Exception{

    public ResourceException() {
    }

    public ResourceException(String message) {
        super(message);
    }

    public ResourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceException(Throwable cause) {
        super(cause);
    }
}
