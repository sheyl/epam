package by.epam.metoda.blinov_mnogopoto4nost.waitNotify;


import java.util.Scanner;

public class Payment {
    private int amount;
    private boolean close;




    public int getAmount() {
        return amount;
    }

    public boolean isClose() {
        return close;
    }

    public synchronized void doPayment() {
        try {
            System.out.println("Start payment:");
            while (amount <= 0) {
                this.wait(); //  ��������� ������ � ������������ ������
                // ����� �������� ���������� ���������� ����� ���������
            }
            // ��� ���������� �������
            close = true;
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Payment is closed : " + close);
       // notify();
    }

    public void initAmount() {
        Scanner scan = new Scanner(System.in);
        amount = scan.nextInt();
    }
}
