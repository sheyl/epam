package by.epam.metoda.blinov_mnogopoto4nost.waitNotify;


public class PaymentRunner {

    public static void main(String[] args) throws  InterruptedException{

        final Payment payment = new Payment();
        new Thread() {
            public void run() {
                payment.doPayment();
            }
        }.start();

        Thread.sleep(200);

        synchronized (payment) {
            System.out.println("init amount");
            payment.initAmount();
            payment.notify();
        }

        synchronized (payment) {
            payment.wait(500);
            System.out.println("ok");

            //System.out.println("finish");
        }
    }
}
