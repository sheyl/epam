package by.epam.metoda.checkingJoin;

import java.util.concurrent.Phaser;

/**
 * Created by admin on 25.05.2016.
 */
public class TrAct implements Runnable {

    private Phaser phaser;

    public TrAct(Phaser phaser) {
        this.phaser = phaser;
    }

    @Override
    public void run() {
        try {
                    phaser.register();
                    System.out.println("1");
                    Thread.sleep(5000);
                    System.out.println(2);
                } catch (InterruptedException e){
                        e.printStackTrace();
                }
        finally {
            phaser.arriveAndDeregister();
        }
    }
}
