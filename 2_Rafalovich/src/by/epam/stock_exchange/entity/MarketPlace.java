package by.epam.stock_exchange.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MarketPlace {

    static final Logger LOG = LogManager.getLogger();
    // акции какой компании торгуются
    private Share share;

    private ExecutorService sellPool;
    private ExecutorService buyPool;

    // запрос на сделку по цене ниже рыночной
    private boolean dealRequest;
    private boolean dealResponse;
    private int dealPrice;

    int packageToBuy;
    int packageForSale;

    long buyShareTime;
    long saleShareTime;

    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public MarketPlace(Share share) {
      this.share = share;
      sellPool = Executors.newFixedThreadPool(1);
      buyPool = Executors.newFixedThreadPool(1);
    }

    public void sell(int packageSize, Broker broker, long time) {


        try {
            lock.lock();
            packageForSale = packageSize;
            saleShareTime = time;
            LOG.info( "Брокер " + broker. getName() +" отдает лок покупающему.");
            condition.signal();
            condition.await();//2,TimeUnit.SECONDS); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // проверка, не скуплена ли уже часть акций
            if (packageSize > packageForSale) {
                int buyingCount = packageSize - packageForSale;
                broker.rechargeAccount(buyingCount * share.getValue());
                broker.writeOffShare(share.toString(), buyingCount);
                //changeValue(buyingCount);
            }

            // цикличная работа до тех пор, пока акции не проданы
            while (packageForSale > 0) {

                int ostatok = packageForSale;
                int sharePrice = share.getValue();

                if (packageToBuy == 0) {
                    condition.await();//2,TimeUnit.SECONDS);  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                }

                //проверяем на ответ
                if (dealRequest) {
                    int price = dealPrice;
                    int realPrice = share.getValue();
                    double perc = 0.95;
                    double otnoshenie = (double) (price / realPrice);
                    // производим решение
                    if (otnoshenie >= perc) {
                        int decision = Math.abs(new Random().nextInt(2));
                        if (decision > 0) {
                            LOG.info("### Продавец согласился продать по договорной цене");
                            dealResponse = true;
                            sharePrice = dealPrice;
                            condition.signal();
                            condition.await();
                        } else {
                            LOG.info("### Продавец не согласился продать по договорной цене");
                            dealResponse = false;
                            condition.signal();
                            condition.await();
                        }
                    } else {
                        LOG.info("### Продавец не согласился продать по договорной цене");
                        dealResponse = false;
                        condition.signal();
                        condition.await();
                    }
                }
                if (ostatok > packageForSale) {
                    int buyingCount = ostatok - packageForSale;
                    broker.rechargeAccount(buyingCount * sharePrice);
                    broker.writeOffShare(share.toString(), buyingCount);
                    changeValue(buyingCount);
                }
            }

        } catch (InterruptedException e) {
            LOG.error( "Процесс продажи был прерван.  " + e.getMessage());
        }
        finally {
            lock.unlock();
        }
    }

    public void buy(int packageSize, Broker broker, long time) {
        try {
            lock.lock();
            packageToBuy = packageSize;
            buyShareTime = time;
            // цикличная работа до тех пор, пока акции не выкуплены
            while( packageToBuy != 0) {
                while( packageForSale == 0) {
                LOG.info("Waiting for wishing to sell.");
                condition.await();//2,TimeUnit.SECONDS); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                }
                // так как за время нахождения в очереди стоимость
                // акций могла увеличиться - делаем проверку на
                // достаточность средств
                if (packageForSale >= packageToBuy) {

                    // достаточно средств
                    if (checkMoney(broker.getMoney())) {
                        broker.debitAccount(packageToBuy * share.getValue());
                        broker.addShare(share.toString(), packageToBuy);
                        packageForSale -= packageToBuy;
                        packageToBuy = 0;
                        LOG.info(" Полностью купил пакет акций. Транзакция завершена.");
                        condition.signal();

                    // денег не достаточно
                    // пробуем договориться по цене ниже рыночной
                    } else  {
                        int requestPrice = broker.getMoney() / packageToBuy;
                        // договорились. покупаем по договорной цене
                        if (reachAgreement( requestPrice)) {
                            broker.debitAccount(packageToBuy * requestPrice);
                            broker.addShare(share.toString(), packageToBuy);
                            packageForSale -= packageToBuy;
                            packageToBuy = 0;
                            LOG.info("### Полностью купил пакет акций по ДОГОВОРНОЙ цене. Транзакция завершена.");
                            condition.signal();

                        // не договорились. покупаем максимально возможное кол-во
                        } else {
                            int newPackage = broker.getMoney() / share.getValue();
                            broker.debitAccount(newPackage * share.getValue());
                            broker.addShare(share.toString(), newPackage);
                            packageForSale -= newPackage;
                            packageToBuy = 0;
                            LOG.info("### Не ДОГОВОРИЛСЯ. Купил на все. Транзакция завершена.");
                            condition.signal();
                        }
                        dealRequest = false;
                        dealResponse = false;
                    }
                } else {
                    broker.debitAccount(packageForSale * share.getValue());
                    broker.addShare(share.toString(), packageForSale);
                    packageToBuy -= packageForSale;
                    packageForSale = 0;
                    LOG.info("Скупил все акции у 1 брокера. жду друго.");
                    condition.signal();
                }
            }

        } catch (InterruptedException e ) {
            LOG.error("Процесс покупки был прерван. " + e.getMessage());
        }
        finally {
            lock.unlock();
        }
    }

    private boolean checkMoney(int money) {
        int price = share.getValue();
        int totalPrice = price * packageToBuy;
        return (money >= totalPrice);
    }

    private boolean reachAgreement (int price) throws InterruptedException{
        dealRequest = true;
        dealPrice = price;
        LOG.info(" Брокер просит продать акции дешевле");
        condition.signal();
        condition.await();
        return  dealResponse;
    }

    public Share getShare() {
        return share;
    }

    private void changeValue(int size) {

        final int COEFFICIENT = 3;
        int oldValue = share.getValue();
        int newValue;

        if (buyShareTime > saleShareTime) {
            newValue = (int) (oldValue * (1 + COEFFICIENT * new Random().nextDouble() * size / share.getISSUED_SHARES()));
        } else {
            newValue = (int) (oldValue * (1 - COEFFICIENT * new Random().nextDouble() * size / share.getISSUED_SHARES()));
        }

        share.setValue(newValue);
    }

    public void addToSellPool(Transaction transaction) {
        sellPool.submit(transaction);
    }

    public void addToBuyPool(Transaction transaction) {
        buyPool.submit(transaction);
    }

    public void stop() {
        buyPool.shutdownNow();
        sellPool.shutdownNow();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MarketPlace)) return false;

        MarketPlace place = (MarketPlace) o;

        if (dealRequest != place.dealRequest) return false;
        if (dealResponse != place.dealResponse) return false;
        if (dealPrice != place.dealPrice) return false;
        if (packageToBuy != place.packageToBuy) return false;
        if (packageForSale != place.packageForSale) return false;
        if (buyShareTime != place.buyShareTime) return false;
        if (saleShareTime != place.saleShareTime) return false;
        return getShare() == place.getShare();

    }

    @Override
    public int hashCode() {
        int result = getShare().hashCode();
        result = 31 * result + (dealRequest ? 1 : 0);
        result = 31 * result + (dealResponse ? 1 : 0);
        result = 31 * result + dealPrice;
        result = 31 * result + packageToBuy;
        result = 31 * result + packageForSale;
        result = 31 * result + (int) (buyShareTime ^ (buyShareTime >>> 32));
        result = 31 * result + (int) (saleShareTime ^ (saleShareTime >>> 32));
        return result;
    }
}
