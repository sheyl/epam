package by.epam.stock_exchange.start;

import by.epam.stock_exchange.entity.ExchangeManager;
import by.epam.stock_exchange.entity.Share;
import by.epam.stock_exchange.entity.StockExchange;
import by.epam.stock_exchange.io.ExchangeResultWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

public class Start {

    static final Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {

//        ArrayList<Share> shares = new ArrayList<>();
//        shares.add(Share.GOOGLE);
//        shares.add(Share.APPLE);
//        shares.add(Share.MICROSOFT);
//        shares.add(Share.SAMSUNG);
//        StockExchange exchange = StockExchange.getInstance("Exchange", shares);
        StockExchange exchange = StockExchange.getInstance();
        ExchangeManager manager = new ExchangeManager("Dima");
        manager.setExchange(exchange);
        manager.addBrokers(20);
        ExchangeResultWriter.write(manager, "data/result.txt");

        try {
            manager.start();
            Thread.sleep(12_000);
            manager.stopExchange();
            Thread.sleep(100);
        } catch (InterruptedException e) {
            LOG.error(" Error in main.");
        }
        finally {
            ExchangeResultWriter.write(manager, "data/result.txt");
        }
    }
}
