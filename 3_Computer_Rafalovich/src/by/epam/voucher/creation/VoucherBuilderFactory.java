package by.epam.voucher.creation;

import by.epam.voucher.parsing.AbstractVoucherBuilder;
import by.epam.voucher.parsing.dom.VoucherDOMBuilder;
import by.epam.voucher.parsing.sax.VoucherSAXBuilder;

public class VoucherBuilderFactory {

    public AbstractVoucherBuilder createVoucherBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
             case DOM:
                 return new VoucherDOMBuilder();
            case STAX:
                return new VoucherSAXBuilder();
            case SAX:
                return new VoucherSAXBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());}
    }

    private enum TypeParser {
        SAX, STAX, DOM
    }


}
