package by.epam.voucher.entity;

public enum  AdditionalOption {

    TV("tv"),
    AIR_CONDITIONING("air-conditioning"),
    PHONE("phone"),
    MINI_BAR("mini-bar"),
    FRIDGE("fridge"),
    FIREPLACE("fireplace"),
    JACUZZI("jacuzzi"),
    WI_FI("wi-fi"),
    SAFE("safe");

    private String value;

    AdditionalOption(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static AdditionalOption getByValue(String s) {
        AdditionalOption option = null;
        for (AdditionalOption x: AdditionalOption.values()) {
            if (s.equals(x.getValue())) {
                option = x;
            }
        }
        return option;
    }
}
