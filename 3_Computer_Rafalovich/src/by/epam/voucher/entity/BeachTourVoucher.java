package by.epam.voucher.entity;

public class BeachTourVoucher extends TouristVoucher {

    private String sea;
    private String beach;
    private int fromSeaDistance;

    public BeachTourVoucher() {
        super();
    }

    public String getSea() {
        return sea;
    }

    public void setSea(String sea) {
        this.sea = sea;
    }

    public String getBeach() {
        return beach;
    }

    public void setBeach(String beach) {
        this.beach = beach;
    }

    public int getFromSeaDistance() {
        return fromSeaDistance;
    }

    public void setFromSeaDistance(int fromSeaDistance) {
        this.fromSeaDistance = fromSeaDistance;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", sea=" + sea +
                ", beach=" + beach +
                ", distance=" + fromSeaDistance +
                '}' + "\n";
    }
}
