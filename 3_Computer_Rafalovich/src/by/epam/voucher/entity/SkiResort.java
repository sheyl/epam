package by.epam.voucher.entity;

/**
 * Created by admin on 30.05.2016.
 */
public class SkiResort implements Cloneable {
    private String name;
    private String mountainSystem;
    private int routLength;
    private int maxDownLength;
    private int elevatorCount;

    public SkiResort() {
    }

    @Override
    protected SkiResort clone() throws CloneNotSupportedException {
        return (SkiResort) super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMountainSystem() {
        return mountainSystem;
    }

    public void setMountainSystem(String mountainSystem) {
        this.mountainSystem = mountainSystem;
    }

    public int getRoutLength() {
        return routLength;
    }

    public void setRoutLength(int routLength) {
        this.routLength = routLength;
    }

    public int getMaxDownLength() {
        return maxDownLength;
    }

    public void setMaxDownLength(int maxDownLength) {
        this.maxDownLength = maxDownLength;
    }

    public int getElevatorCount() {
        return elevatorCount;
    }

    public void setElevatorCount(int elevatorCount) {
        this.elevatorCount = elevatorCount;
    }

    @Override
    public String toString() {
        return "SkiResort{" +
                "name='" + name + '\'' +
                ", mountainSystem='" + mountainSystem + '\'' +
                ", routLength='" + routLength + '\'' +
                ", maxDownLength='" + maxDownLength + '\'' +
                ", elevatorCount=" + elevatorCount +
                "}";
    }
}
