package by.epam.voucher.entity;

/**
 * Created by admin on 30.05.2016.
 */
public class SkiTourVoucher extends TouristVoucher {

    private SkiResort skiResort;

    public SkiTourVoucher() {
        super();
        skiResort = new SkiResort();
    }

    public SkiResort getSkiResort() {
        SkiResort skiResort = null;
        try {
            skiResort = skiResort.clone();
        } catch (CloneNotSupportedException e) {

        }
        return skiResort;
    }

    public void setSkiResort(SkiResort skiResort) {
        this.skiResort = skiResort;
    }

    @Override
    public String toString() {
        return super.toString() + " " +
                skiResort.toString() +
                '}' + "\n";
    }
}
