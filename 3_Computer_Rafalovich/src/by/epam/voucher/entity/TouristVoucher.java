package by.epam.voucher.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by admin on 30.05.2016.
 */
public abstract class TouristVoucher {

    public static final String DEFAULT_NAME = "Unnamed_Hotel";
    private String id;
    private String hotelName;
    private String type;
    private String country;
    private int nightCount;
    private String transport;
    private int hotelClass;
    private String food;
    private int roomType;
    private List<AdditionalOption> facilities;
    private int cost;

    public TouristVoucher() {
        hotelName = DEFAULT_NAME;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNightCount() {
        return nightCount;
    }

    public void setNightCount(int nightCount) {
        this.nightCount = nightCount;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public int getHotelClass() {
        return hotelClass;
    }

    public void setHotelClass(int hotelClass) {
        this.hotelClass = hotelClass;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public void createFacilities() {
        facilities = new ArrayList<>();
    }

    public List<AdditionalOption> getFacilities() {
        return Collections.unmodifiableList(facilities);
    }

    public void setFacilities(List<AdditionalOption> facilities) {
        this.facilities = facilities;
    }

    public void addFacilities(AdditionalOption option) {
        facilities.add(option);
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "id='" + id + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", nightCount=" + nightCount +
                ", transport='" + transport + '\'' +
                ", hotelClass=" + hotelClass +
                ", food='" + food + '\'' +
                ", roomType=" + roomType +
                ", facilities=" + facilities +
                ", cost=" + cost;
    }
}
