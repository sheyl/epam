package by.epam.voucher.entity;

/**
 * Created by admin on 31.05.2016.
 */
public enum VoucherTag {
    TOURIST_VOUCHERS("tourist-vouchers"),
    TOURIST_VOUCHER("tourist-voucher"),
    BEACH_TOUR_VOUCHER("beach-tour-voucher"),
    SKI_TOUR_VOUCHER("ski-tour-voucher"),
    TYPE,
    COUNTRY,
    NIGHT_COUNT("night-count"),
    TRANSPORT,
    HOTEL_CLASS("hotel-class"),
    FOOD,
    ROOM_TYPE("room-type"),
    COST,
    FACILITIES,
    ADDITIONAL_OPTION("additional-option"),
    ADDITIONAL_OPTIONS("additional-options"),
    SKI_RESORT("ski-resort"),
    NAME,
    MOUNTAIN_SYSTEM("mountain-system"),
    ROUT_LENGTH("rout-length"),
    MAX_DOWN_LENGTH("max-down-length"),
    ELEVATOR_COUNT("elevator-count"),
    SEA,
    BEACH,
    DISTANCE;


    private String value;
    private static String nameSpace = "ns:";

    VoucherTag(String value) {
        this.value = value;
    }

    VoucherTag() {
        value = this.name().toLowerCase();
    }

    public String getFullValue() {
        return nameSpace + value;
    }

    public String getValue() {
        return value;
    }

    public static VoucherTag getByValue(String s) {
        VoucherTag tag = null;
        for (VoucherTag x: VoucherTag.values()) {
            if (s.equals(x.getValue())) {
                tag = x;
                break;
            }
        }
        return tag;
    }


}
