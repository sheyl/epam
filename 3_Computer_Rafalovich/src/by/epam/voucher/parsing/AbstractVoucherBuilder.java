package by.epam.voucher.parsing;

import by.epam.voucher.entity.TouristVoucher;
import java.util.HashSet;
import java.util.Set;


public abstract class AbstractVoucherBuilder {

    protected Set<TouristVoucher> vouchers;

    public AbstractVoucherBuilder() {
        vouchers = new HashSet<>();
    }

    public Set<TouristVoucher> getVouchers() {
        return vouchers;

    }

    public abstract void buildSetVouchers(String filename);
}
