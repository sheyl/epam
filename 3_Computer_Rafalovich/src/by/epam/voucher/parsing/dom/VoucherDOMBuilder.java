package by.epam.voucher.parsing.dom;

import static by.epam.voucher.entity.VoucherTag.*;
import by.epam.voucher.entity.*;
import by.epam.voucher.parsing.AbstractVoucherBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class VoucherDOMBuilder extends AbstractVoucherBuilder{

    final static Logger LOG = LogManager.getLogger();

    private DocumentBuilder docBuilder;

    public VoucherDOMBuilder() {

        // creation dom-analyzer
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error(e.getMessage());
        }
    }

    public void buildSetVouchers(String fileName) {

        Document doc;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList skiTourList = root.getElementsByTagName(SKI_TOUR_VOUCHER.getFullValue());
            for (int i = 0; i < skiTourList.getLength(); i++) {
                Element skiTourElement = (Element) skiTourList.item(i);
                TouristVoucher voucher = buildSkiTourVoucher(skiTourElement);
                vouchers.add(voucher);
            }
            NodeList beachTourList = root.getElementsByTagName(BEACH_TOUR_VOUCHER.getFullValue());
            for (int i = 0; i < beachTourList.getLength(); i++) {
                Element beachTourElement = (Element) beachTourList.item(i);
                TouristVoucher voucher = buildBeachTourVoucher(beachTourElement);
                vouchers.add(voucher);
            }
        } catch (SAXException | IOException e) {
            LOG.error(e.getMessage());
        }
    }

    private SkiTourVoucher buildSkiTourVoucher(Element element) {

        SkiTourVoucher voucher = new SkiTourVoucher();
        buildTouristVoucher(voucher, element);
        NodeList skiResortList = element.getElementsByTagName(SKI_RESORT.getFullValue());
        Element skiResortElement = (Element) skiResortList.item(0);
        SkiResort resort = buildSkiResort(skiResortElement);
        voucher.setSkiResort(resort);
        return voucher;

    }

    private BeachTourVoucher buildBeachTourVoucher(Element element) {

        BeachTourVoucher voucher = new BeachTourVoucher();
        buildTouristVoucher(voucher, element);
        voucher.setSea(getElementTextContent(element,SEA.getFullValue()));
        voucher. setBeach(getElementTextContent(element,BEACH.getFullValue()));
        int distance = Integer.parseInt(getElementTextContent(element,DISTANCE.getFullValue()));
        voucher.setFromSeaDistance(distance);
        return voucher;
    }

    private void buildTouristVoucher(TouristVoucher voucher, Element voucherElement) {

        voucher.setId(voucherElement.getAttribute("id"));
        if (!voucherElement.getAttribute("hotel-name").isEmpty()) {
            voucher.setHotelName(voucherElement.getAttribute("hotel-name"));
        }

        voucher.setType(getElementTextContent(voucherElement,TYPE.getFullValue()));
        voucher.setCountry(getElementTextContent(voucherElement,COUNTRY.getFullValue()));
        int count = Integer.parseInt(getElementTextContent(voucherElement,NIGHT_COUNT.getFullValue()));
        voucher.setNightCount(count);
        voucher.setTransport(getElementTextContent(voucherElement,TRANSPORT.getFullValue()));
        int clas = Integer.parseInt(getElementTextContent(voucherElement,HOTEL_CLASS.getFullValue()));
        voucher.setHotelClass(clas);
        voucher.setFood(getElementTextContent(voucherElement, FOOD.getFullValue()));
        int rooms = Integer.parseInt(getElementTextContent(voucherElement,ROOM_TYPE.getFullValue()));
        voucher.setRoomType(rooms);
        int cost = Integer.parseInt(getElementTextContent(voucherElement,COST.getFullValue()));
        voucher.setCost(cost);

        NodeList facilitiesList = voucherElement.getElementsByTagName(FACILITIES.getFullValue());
        // if element in <facilities>
        if (facilitiesList.getLength() == 1) {
            voucher.createFacilities();
            Element facil = (Element) facilitiesList.item(0);
            NodeList nodeList = facil.getElementsByTagName(ADDITIONAL_OPTION.getFullValue());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String value = node.getTextContent().trim();
                AdditionalOption option = AdditionalOption.getByValue(value);
                voucher.addFacilities(option);
            }
        }
    }

    private SkiResort buildSkiResort(Element skiResortElement) {

        SkiResort resort = new SkiResort();
        resort.setName(getElementTextContent(skiResortElement,NAME.getFullValue()));
        resort.setMountainSystem(getElementTextContent(skiResortElement, MOUNTAIN_SYSTEM.getFullValue()));
        int routLength = Integer.parseInt(getElementTextContent(skiResortElement, ROUT_LENGTH.getFullValue()));
        resort.setRoutLength(routLength);
        int maxLength = Integer.parseInt(getElementTextContent(skiResortElement, MAX_DOWN_LENGTH.getFullValue()));
        resort.setMaxDownLength(maxLength);
        int elevatorCount = Integer.parseInt(getElementTextContent(skiResortElement, ELEVATOR_COUNT.getFullValue()));
        resort.setElevatorCount(elevatorCount);
        return resort;
    }

    private static String getElementTextContent(Element element, String elementName) {

        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent().trim();
    }
}
