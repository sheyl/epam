package by.epam.voucher.parsing.sax;

import static by.epam.voucher.entity.VoucherTag.*;
import by.epam.voucher.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class VoucherHandler extends DefaultHandler {
    private Set<TouristVoucher> vouchers;
    private TouristVoucher currentVoucher;
    private BeachTourVoucher currentBeach;
    private SkiTourVoucher currentSki;
    private VoucherTag currentTag;
    private Set<VoucherTag> withText;
    private String sea;
    private String beach;
    private int distance;

    private SkiResort skiResort;

    public VoucherHandler() {
        vouchers = new HashSet<>();
        withText = EnumSet.range(VoucherTag.TYPE,VoucherTag.DISTANCE);
    }

    public Set<TouristVoucher> getVouchers() {
        return vouchers;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String tmp = new String(ch, start, length).trim();
        if (currentTag != null){
            switch (currentTag) {
                case TYPE:
                    currentVoucher.setType(tmp);
                    break;
                case COUNTRY:
                    currentVoucher.setCountry(tmp);
                    break;
                case NIGHT_COUNT:
                    currentVoucher.setNightCount(Integer.parseInt(tmp));
                    break;
                case TRANSPORT:
                    currentVoucher.setTransport(tmp);
                    break;
                case HOTEL_CLASS:
                    currentVoucher.setHotelClass(Integer.parseInt(tmp));
                    break;
                case FOOD:
                    currentVoucher.setFood(tmp);
                    break;
                case ROOM_TYPE:
                    currentVoucher.setRoomType(Integer.parseInt(tmp));
                    break;
                case COST:
                    currentVoucher.setCost(Integer.parseInt(tmp));
                    break;
                case FACILITIES:
                    currentVoucher.createFacilities();
                    break;
                case ADDITIONAL_OPTION:
                    currentVoucher.addFacilities(AdditionalOption.getByValue(tmp));
                    break;
                case SKI_RESORT:
                    skiResort = new SkiResort();
                    break;
                case NAME:
                    skiResort.setName(tmp);
                    break;
                case MOUNTAIN_SYSTEM:
                    skiResort.setMountainSystem(tmp);
                    break;
                case ROUT_LENGTH:
                    skiResort.setRoutLength(Integer.parseInt(tmp));
                    break;
                case MAX_DOWN_LENGTH:
                    skiResort.setMaxDownLength(Integer.parseInt(tmp));
                    break;
                case ELEVATOR_COUNT:
                    skiResort.setElevatorCount(Integer.parseInt(tmp));
                    break;
                case SEA:
                    sea = tmp;
                    break;
                case BEACH:
                    beach = tmp;
                    break;
                case DISTANCE:
                    distance = Integer.parseInt(tmp);
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                    currentTag.getDeclaringClass(), currentTag.name());
            }

        }
        currentTag = null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (SKI_TOUR_VOUCHER.getValue().equals(localName)) {
            currentSki = (SkiTourVoucher) currentVoucher;
            currentSki.setSkiResort(skiResort);
            vouchers.add(currentSki);
        } else if (BEACH_TOUR_VOUCHER.getValue().equals(localName)) {
            currentBeach = (BeachTourVoucher) currentVoucher;
            currentBeach.setSea(sea);
            currentBeach.setFromSeaDistance(distance);
            currentBeach.setBeach(beach);
            vouchers.add(currentBeach);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (SKI_TOUR_VOUCHER.getValue().equals(localName)) {
            currentVoucher = new SkiTourVoucher();
            currentVoucher.setId(attributes.getValue("id"));
            if (attributes.getLength() == 2) {
                currentVoucher.setHotelName(attributes.getValue("hotel-name"));
            }

        } else if (BEACH_TOUR_VOUCHER.getValue().equals(localName)) {
            currentVoucher = new BeachTourVoucher();
            currentVoucher.setId(attributes.getValue(0));
            if (attributes.getLength() == 2) {
                currentVoucher.setHotelName(attributes.getValue(1));
            }
        }
        else {
            for (VoucherTag x: withText) {
                if (x.getValue().equals(localName)) {
                   currentTag = x;
                }
            }
        }
    }
}
