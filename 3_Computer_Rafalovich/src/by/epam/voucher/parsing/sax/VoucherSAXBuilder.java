package by.epam.voucher.parsing.sax;

import by.epam.voucher.parsing.AbstractVoucherBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;

/**
 * Created by admin on 31.05.2016.
 */
public class VoucherSAXBuilder extends AbstractVoucherBuilder {

    final static Logger LOG = LogManager.getLogger();
    private VoucherHandler voucherHandler;
    private XMLReader reader;

    public VoucherSAXBuilder() {
        voucherHandler = new VoucherHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(voucherHandler);
        } catch (SAXException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void buildSetVouchers(String fileName){
        try {
            // ������ ����� xml
            reader.parse(fileName);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } catch (SAXException e) {
            LOG.error(e.getMessage(), e);
        }
        vouchers = voucherHandler.getVouchers();
    }
}
