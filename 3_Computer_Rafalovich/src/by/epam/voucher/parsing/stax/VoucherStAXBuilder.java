package by.epam.voucher.parsing.stax;

import static by.epam.voucher.entity.VoucherTag.*;
import by.epam.voucher.entity.*;
import by.epam.voucher.parsing.AbstractVoucherBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


public class VoucherStAXBuilder extends AbstractVoucherBuilder {

    final static Logger LOG = LogManager.getLogger();

    private XMLInputFactory inputFactory;

    public VoucherStAXBuilder() {

        inputFactory = XMLInputFactory.newInstance();
    }

    public void buildSetVouchers(String fileName){

        XMLStreamReader reader;
        String name;
        try (FileInputStream inputStream = new FileInputStream(fileName)){
            reader = inputFactory.createXMLStreamReader(inputStream);
            // ��������������� �������
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    //  ���� ��������� ��� ������� �������
                    if (BEACH_TOUR_VOUCHER.getValue().equals(name)) {
                        BeachTourVoucher voucher = buildBeachTourVoucher(reader);
                        vouchers.add(voucher);
                    } else if (SKI_TOUR_VOUCHER.getValue().equals(name)) {
                        SkiTourVoucher voucher = buildSkiTourVoucher(reader);
                        vouchers.add(voucher);
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOG.error("stax parsing error! " + e.getMessage());
        } catch (IOException e) {
            LOG.error("File " + fileName + " not found! " +e.getMessage());
        }
    }

    private void buildTouristVoucher(TouristVoucher voucher, XMLStreamReader reader) throws XMLStreamException {

        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            // determine the type of vertex
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    VoucherTag tag = getByValue(name);
                    // determine tag
                    switch (tag) {
                        case TYPE:
                            voucher.setType(getXMLText(reader));
                            break;
                        case COUNTRY:
                            voucher.setCountry(getXMLText(reader));
                            break;
                        case NIGHT_COUNT:
                            voucher.setNightCount(Integer.parseInt(getXMLText(reader)));
                            break;
                        case TRANSPORT:
                            voucher.setTransport(getXMLText(reader));
                            break;
                        case HOTEL_CLASS:
                            voucher.setHotelClass(Integer.parseInt(getXMLText(reader)));
                            break;
                        case FOOD:
                            voucher.setFood(getXMLText(reader));
                            break;
                        case ROOM_TYPE:
                            voucher.setRoomType(Integer.parseInt(getXMLText(reader)));
                            break;
                        case COST:
                            voucher.setCost(Integer.parseInt(getXMLText(reader)));
                            break;
                        case FACILITIES:
                            voucher.createFacilities();
                            voucher.setFacilities(addOptions(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (COST.getValue().equals(name)) {
                        return;
                    }
                    break;
            }
        }
    }

    private SkiTourVoucher buildSkiTourVoucher(XMLStreamReader reader) throws XMLStreamException {

        SkiTourVoucher voucher = new SkiTourVoucher();
        voucher.setId(reader.getAttributeValue(0));
        if (reader.getAttributeCount() == 2) {
            voucher.setHotelName(reader.getAttributeValue(1));
        }
        buildTouristVoucher(voucher, reader);
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            // determine the type of vertex
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    VoucherTag tag = getByValue(name);
                    // determine tag
                    switch (tag) {
                        case SKI_RESORT:
                           voucher.setSkiResort(buildSkiResort(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (SKI_TOUR_VOUCHER.getValue().equals(name)) {
                        return voucher;
                    }
                    break;
            }

        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private BeachTourVoucher buildBeachTourVoucher(XMLStreamReader reader) throws XMLStreamException {

        BeachTourVoucher voucher = new BeachTourVoucher();
        voucher.setId(reader.getAttributeValue(0));
        if (reader.getAttributeCount() == 2) {
            voucher.setHotelName(reader.getAttributeValue(1));
        }
        buildTouristVoucher(voucher, reader);
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            // determine the type of vertex
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    VoucherTag tag = getByValue(name);
                    // determine tag
                    switch (tag) {
                       case SEA:
                           voucher.setSea(getXMLText(reader));
                            break;
                        case BEACH:
                            voucher.setBeach(getXMLText(reader));
                            break;
                        case DISTANCE:
                            voucher.setFromSeaDistance(Integer.parseInt(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (BEACH_TOUR_VOUCHER.getValue().equals(name)) {
                        return voucher;
                    }
                    break;
            }

        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private SkiResort buildSkiResort(XMLStreamReader reader) throws XMLStreamException {

        SkiResort resort = new SkiResort();
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            // determine the type of vertex
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    VoucherTag tag = getByValue(name);
                    // determine tag
                    switch (tag) {
                        case NAME:
                            resort.setName(getXMLText(reader));
                            break;
                        case MOUNTAIN_SYSTEM:
                            resort.setMountainSystem(getXMLText(reader));
                            break;
                        case ROUT_LENGTH:
                            resort.setRoutLength(Integer.parseInt(getXMLText(reader)));
                            break;
                        case MAX_DOWN_LENGTH:
                            resort.setMaxDownLength(Integer.parseInt(getXMLText(reader)));
                            break;
                        case ELEVATOR_COUNT:
                            resort.setElevatorCount(Integer.parseInt(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (SKI_RESORT.getValue().equals(name)) {
                        return resort;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private List<AdditionalOption> addOptions(XMLStreamReader reader) throws XMLStreamException {

        List<AdditionalOption> options = new ArrayList<>();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    String value = getXMLText(reader);
                    AdditionalOption option =  AdditionalOption.getByValue(value);
                    options.add(option);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (FACILITIES.getValue().equals(name)) {
                        return options;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {

        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText().trim();
        }
        return text;
    }
}
