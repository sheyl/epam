package by.epam.voucher.start;

import by.epam.voucher.creation.VoucherBuilderFactory;
import by.epam.voucher.parsing.AbstractVoucherBuilder;

/**
 * Created by admin on 31.05.2016.
 */
public class Start {
    public static void main(String[] args) {

       /* VoucherSAXBuilder saxBuilder = new VoucherSAXBuilder();
        saxBuilder.buildSetVouchers("data/vouchers.xml");
        System.out.println(saxBuilder.getVouchers());

        VoucherDOMBuilder domBuilder = new VoucherDOMBuilder();
        domBuilder.buildSetVouchers("data/vouchers.xml");
        System.out.println(domBuilder.getVouchers());

        VoucherStAXBuilder stAXBuilder = new VoucherStAXBuilder();
        stAXBuilder.buildSetVouchers("data/vouchers.xml");
        System.out.println(stAXBuilder.getVouchers());*/

        VoucherBuilderFactory factory = new VoucherBuilderFactory();
        AbstractVoucherBuilder builder = factory.createVoucherBuilder("sax");
        builder.buildSetVouchers("data/vouchers.xml");
        System.out.println(builder.getVouchers());
    }
}
