package by.epam.voucher.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class ValidatorSAXXSD {

    final static Logger LOG = LogManager.getLogger();

    public static void main(String[ ] args) {

        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        String fileName = "data/vouchers.xml";
        String schemaName = "data/vouchers_schema.xsd";
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(schemaName);
        try {
            // creation schemes
            Schema schema = factory.newSchema(schemaLocation);
            // creating a validator on the basis of the scheme
            Validator validator = schema.newValidator();
            // document check
            Source source = new StreamSource(fileName);
            validator.validate(source);
            System.out.println(fileName + " is valid.");
        } catch (SAXException e) {
            LOG.error("validation "+ fileName + " is not valid because "
                    + e.getMessage());
        } catch (IOException e) {
            LOG.error(fileName + " is not valid because "
                    + e.getMessage());
        }
    }
}
