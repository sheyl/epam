package by.epam.airline.entity.enumtype;

/**
 *
 * @version 1.1 08.05.2016
 * @author Dmitry Rafalovich
 */
public enum CivilAirplaneType {
    AIRLINER,
    AIRFREIGHTER,
    NATIONAL_ECONOMY_PLANE
}
