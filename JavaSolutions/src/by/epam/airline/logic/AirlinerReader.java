package by.epam.airline.logic;

import by.epam.airline.entity.technics.airplane.Airliner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * @version 1.1 10.05.2016
 * @author Dmitry Rafalovich
 */
public class AirlinerReader {

    public final static Logger LOG = LogManager.getLogger();

    public final static String FIELD_DOES_NOT_FIND = "Field %s doesn't find" +
            " in the string. It is impossible to create object!";

    /**
     *@return new Airliner or null
     */
    public static Airliner readAirliner(String plane) {

        Airliner airliner = new Airliner();

        if (plane.length() > 50) {
            Scanner scanner = new Scanner(plane);

            scanner.findInLine("model:");
            if (scanner.hasNext()) {
                airliner.setModel(scanner.next());
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "model:"));
                return null;
            }

            scanner.findInLine("sideNumber:");
            if (scanner.hasNext()) {
                airliner.setSideNumber(scanner.next());
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "sideNumber:"));
                return null;
            }

            scanner.findInLine("engineCount:");
            if (scanner.hasNext()) {
                try {
                    airliner.setEngineCount(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field engineCount must be int.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "engineCount:"));
                return null;
            }

            scanner.findInLine("passengerCapacity:");
            if (scanner.hasNext()) {
                try {
                    airliner.setPassengerCapacity(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field passengerCapacity must be int.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "passengerCapacity:"));
                return null;
            }

            scanner.findInLine("personalCount:");
            if (scanner.hasNext()) {
                try {
                    airliner.setPersonalCount(Integer.parseInt(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field personalCount must be int.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "personalCount:"));
                return null;
            }

            scanner.findInLine("length:");
            if (scanner.hasNext()) {
                try {
                    airliner.setLength(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field length must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "length:"));
                return null;
            }

            scanner.findInLine("wingspan:");
            if (scanner.hasNext()) {
                try {
                    airliner.setWingspan(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field wingspan must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "wingspan:"));
                return null;
            }

            scanner.findInLine("maxWeight:");
            if (scanner.hasNext()) {
                try {
                    airliner.setMaxWeight(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxWeight must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxWeight:"));
                return null;
            }

            scanner.findInLine("maxSpeed:");
            if (scanner.hasNext()) {
                try {
                    airliner.setMaxSpeed(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxSpeed must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxSpeed:"));
                return null;
            }

            scanner.findInLine("maxAltitude:");
            if (scanner.hasNext()) {
                try {
                    airliner.setMaxAltitude(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxAltitude must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxAltitude:"));
                return null;
            }

            scanner.findInLine("maxFlightRange:");
            if (scanner.hasNext()) {
                try {
                    airliner.setMaxFlightRange(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field maxFlightRange must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "maxFlightRange:"));
                return null;
            }

            scanner.findInLine("fuelConsumption:");
            if (scanner.hasNext()) {
                try {
                    airliner.setFuelConsumption(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field fuelConsumption must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "fuelConsumption:"));
                return null;
            }

            scanner.findInLine("capacity:");
            if (scanner.hasNext()) {
                try {
                    airliner.setCapacity(Double.parseDouble(scanner.next()));
                } catch (NumberFormatException e) {
                    LOG.error(e + " Field capacity must be double.");
                    return null;
                }
            } else {
                LOG.warn(String.format(FIELD_DOES_NOT_FIND, "capacity:"));
                return null;
            }

        } else {
            LOG.info("String \"" + plane + "\" so short." +
                    " Check airplane's arguments.");
            return null;
        }
        return airliner;
    }
}
