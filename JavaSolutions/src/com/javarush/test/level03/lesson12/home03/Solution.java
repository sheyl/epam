package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int max = 0 ;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s1 = reader.readLine();
        int a = Integer.parseInt(s1);
        String s2 = reader.readLine();
        int b = Integer.parseInt(s2);
        String s3 = reader.readLine();
        int c = Integer.parseInt(s3);

        if (a >= b && a >= c)
            max = a;
        else   if (b >= a && b >= c)
            max = b;
        else   if (c >= a && c >= b)
            max = c;
        System.out.println(max);
        if (max == a) {
            if (b >= c) {
                System.out.println(b);
                System.out.println(c);
            } else {
                System.out.println(c);
                System.out.println(b);
            }
        }
        if (max ==b) {
            if (a >= c) {
                System.out.println(a);
                System.out.println(c);
            } else {
                System.out.println(c);
                System.out.println(a);
            }
        }
        if (max == c) {
            if (b >= a) {
                System.out.println(b);
                System.out.println(a);
            } else {
                System.out.println(a);
                System.out.println(b);
            }
        }

        for (int i = 0; i <10 ; i++) {
            System.out.println();
            for (int j = 0; j <=i ; j++) {
                System.out.print("8");
            }

        }
    }

}

