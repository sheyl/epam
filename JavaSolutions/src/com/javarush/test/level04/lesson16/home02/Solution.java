package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s1 = reader.readLine();
        int a = Integer.parseInt(s1);
        String s2 = reader.readLine();
        int b = Integer.parseInt(s2);
        String s3 = reader.readLine();
        int c = Integer.parseInt(s3);
        int max = a ;
        int min = a;
        if (b>max) max =b;
        if (c>max) max = c;
        if (b<min) min =b;
        if (c<min) min = c;
        if ((a != min) && (a !=max))
            System.out.println(a);
        else if ((b != min) && (b !=max))
            System.out.println(b);
        else //if ((c != min) && (c !=max))
            System.out.println(c);
    }
}
