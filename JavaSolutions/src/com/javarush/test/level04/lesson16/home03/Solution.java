package com.javarush.test.level04.lesson16.home03;

import java.io.*;

/* Посчитать сумму чисел
Вводить с клавиатуры числа и считать их сумму. Если пользователь ввел -1, вывести на экран сумму и завершить программу.  -1 должно учитываться в сумме.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean sExit = false;
        int sum = 0;
        while(!sExit) {
            String s1 = reader.readLine();
            int a = Integer.parseInt(s1);
            sum +=a;
            if (a == -1) sExit = true;
        }
        System.out.println(sum);
        //напишите тут ваш код
    }
}
