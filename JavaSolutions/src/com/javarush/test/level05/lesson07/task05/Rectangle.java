package com.javarush.test.level05.lesson07.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше методов initialize(…)
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    private int left;
    private int top;
    private int width = 0;
    private int height = 0;

    public void initialize( int left, int top, int width, int height) {
      this.height = height;
        this.left = left;
        this.top = top;
        this.width = width;
    }

    public void initialize( int left, int top, int width) {
        this.height = width;
        this.left = left;
        this.top = top;
        this.width = width;
    }

    public void initialize( int left, int top) {

        this.left = left;
        this.top = top;

    }

    public void initialize ( Rectangle r1) {
        this.height = r1.height;
        this.left = r1.left;
        this.top = r1.top;
        this.width = r1.width;
    }
    //напишите тут ваш код

}
