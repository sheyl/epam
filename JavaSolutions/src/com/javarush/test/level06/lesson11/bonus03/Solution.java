package com.javarush.test.level06.lesson11.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Задача по алгоритмам
Задача: Написать программу, которая вводит с клавиатуры 5 чисел и выводит их в возрастающем порядке.
Пример ввода:
3
2
15
6
17
Пример вывода:
2
3
6
15
17
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader  = new BufferedReader(new InputStreamReader(System.in));
        int[] arr = new int[5];
        int buf;
        for (int i = 0; i < 5; i++) {
            arr[i] = Integer.parseInt(reader.readLine());

        }
        for (int i = 0; i < 4; i++) {
            for (int j = i + 1; j <=4 ; j++) {
                if (arr[j] < arr[i]) {
                    buf = arr[i];
                    arr[i] = arr[j];
                    arr[j] = buf;
                }

            }

        }
        for (int x : arr) System.out.println(x);


    }
}
