package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human ded1 = new Human("anton", true, 77);
        Human ded2 = new Human("vasya", true, 78);
        Human baba1 = new Human("masha", false, 77);
        Human baba2 = new Human("lusya", false, 73);
        Human papa = new Human("dima", true, 40, ded1, baba1);
        Human mama = new Human("christina", false, 43, ded2, baba2);
        Human doch1 = new Human("ira", false, 20, papa, mama);
        Human doch2 = new Human("katya", false, 15, papa, mama);
        Human doch3 = new Human("lena", false, 13, papa, mama);
        System.out.println(ded1);
        System.out.println(ded2);
        System.out.println(baba1);
        System.out.println(baba2);
        System.out.println(papa);
        System.out.println(mama);
        System.out.println(doch1);
        System.out.println(doch2);
        System.out.println(doch3);




    }

    public static class Human
    {

        String name;
        boolean sex;
        int age;
        Human father; //= new Human(;
        Human mother; // = new Human();

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }


        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
