package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        HashSet<String> s = new HashSet<String>();
        s.add("Лф");
        s.add("Лq");
        s.add("Лw");
        s.add("Лe");
        s.add("Лr");
        s.add("Лt");
        s.add("Лн");
        s.add("Лг");
        s.add("Ля");
        s.add("Лч");
        s.add("Лс");
        s.add("Ла");
        s.add("Лю");
        s.add("Лм");
        s.add("Ли");
        s.add("Лт");
        s.add("Ль");
        s.add("Лб");
        s.add("Лэ");
        s.add("Лыфв");

        return s;

    }
}
