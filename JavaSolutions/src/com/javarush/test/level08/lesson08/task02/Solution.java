package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        HashSet<Integer> s = new HashSet<Integer>();
        s.add(10);
        s.add(20);
        s.add(100);
        s.add(101);
        s.add(9);
        s.add(4);
        s.add(10444);
        s.add(13330);
        s.add(1110);
        s.add(1);
        s.add(19);
        s.add(18);
        s.add(17);
        s.add(16);
        s.add(15);
        s.add(14);
        s.add(13);
        s.add(12);
        s.add(11);
        s.add(6);
        return s;

    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set) {

        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            Integer element = it.next();
            if (element > 10) {
                it.remove();
            }
        }
        return  set;
    }


}
