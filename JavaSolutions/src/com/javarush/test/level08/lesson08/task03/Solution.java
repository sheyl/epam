package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Ivanov","qw");
        map.put("Petrov","et");
        map.put("Sidorov","hgf");
        map.put("Lob","fgh");
        map.put("Nos","dhs");
        map.put("Tur","sfh");
        map.put("Qwert","hb");
        map.put("asd","ht");
        map.put("wqer","tu");
        map.put("poy","uo");
        return map;



    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int n =0;
       for (Map.Entry<String,String> x : map.entrySet()) {
           String value = x.getValue();
           if (value.equals(name)) n++;

       }
        return n;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        int n =0;
        for (Map.Entry<String,String> x : map.entrySet()) {
            String key= x.getKey();
            if (key.equals(familiya)) n++;

        }
        return n;

    }
}
