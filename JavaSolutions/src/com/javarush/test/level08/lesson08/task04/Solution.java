package com.javarush.test.level08.lesson08.task04;

import java.util.*;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static void main(String[] args) {
        Solution s = new Solution();
        HashMap map = s.createMap();
        s.removeAllSummerPeople(map);
    }
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();



        map.put("Stallone", new Date("JUNE 1 1980"));
        map.put("Petrov", new Date("JUlY 2 1981"));
        map.put("Sidorov", new Date("OCTOBER 1 1980"));
        map.put("Ivanov", new Date("SEPTEMBER 1 1980"));
        map.put("Rezov", new Date("MARCH 1 1980"));
        map.put("Stas", new Date("APRIL 1 1980"));
        map.put("Stal", new Date("MAY 1 1980"));
        map.put("Stalloner", new Date("MAY 1 1980"));
        map.put("Stallon", new Date("JUNE 1 1980"));
        map.put("Stakan", new Date("JUNE 1 1980"));


        //напишите тут ваш код
        return map;

    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        HashMap<String, Date> mapcopy = new HashMap<String, Date>(map);
        for (Map.Entry<String,Date> x: mapcopy.entrySet() ) {

            Date date = x.getValue();
            String key = x.getKey();

            int month = date.getMonth();
            //System.out.println(month);
            if (month == 5 | month == 6 | month ==7){
                System.out.println(key);
                map.remove(key);
            }
        }

        /*for (Map.Entry<String,Date> x: map.entrySet() ) {
            System.out.println(x.getKey() + " " + x.getValue());
        }*/

    }
}
