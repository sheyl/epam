package com.javarush.test.level08.lesson08.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put("Пирожков", "Тихон");
        map.put("Иванов", "Иван");
        map.put("Кайлевич", "Александр");
        map.put("Рафалович", "Дмитрий");
        map.put("Дедкова", "Юля");
        map.put("Шагов", "Дмитрий");
        map.put("Ревенько", "Мария");
        map.put("Баркевич", "Александр");
        map.put("Петров", "Александр");
        map.put("Чеботаре", "Борис");
        return  map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        //HashMap<String, String> copy = new HashMap<String, String>(map);
        ArrayList<String> l = new ArrayList<String>();
        for (Map.Entry<String, String> pair: map.entrySet()) {
            String s = pair.getValue();
           l.add(s);
           // removeItemFromMapByValue(map,s);
        }
        for (int i = 0; i <l.size()-1 ; i++) {
            for (int j = i +1; j < l.size() ; j++) {
                if (l.get(i).equals(l.get(j)) ) {
                    removeItemFromMapByValue(map,l.get(i));
                }

            }

        }
        /*for (String value : copy.values()) постранство ключей
        if (Collections.frequency(copy.values(), value) > 1) совпадение в copy.values() значения value
            removeItemFromMapByValue(map, value);*/

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
