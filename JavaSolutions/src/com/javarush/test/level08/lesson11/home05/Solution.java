package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Вывести результат на экран.

Пример ввода:
  мама     мыла раму.

Пример вывода:
  Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        String s1[] = s.split(" "); // создаем массив по пробелу
        //for (String x : s1) System.out.println(x);
        for (int i = 0; i < s1.length  ;) {
            if (s1[i].equals("")) {
               i++;
            }
            else {
                char ch = s1[i].toUpperCase().charAt(0);
                String s2 = s1[i].substring(1);
                s1[i] = ch + s2;
                i++;
            }

        }
        s ="";
        for (int i = 0; i <s1.length ; i++) {
            s = s + (" " + s1[i]);

        }
        s = s.substring(1);
        System.out.println(s);


        //напишите тут ваш код
    }


}
