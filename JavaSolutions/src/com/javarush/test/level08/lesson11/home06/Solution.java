package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) {
        Human ch1 = new Human("Q",true,13);
        Human ch2 = new Human("V",true,13);
        Human ch3 = new Human("S",true,13);

        Human ma = new Human("Sveta",false,43);
        ma.children.add(ch1);
        ma.children.add(ch2);
        ma.children.add(ch3);

        Human pa = new Human("Igor",true,43);
        pa.children.add(ch1);
        pa.children.add(ch2);
        pa.children.add(ch3);


        Human Grm1 = new Human("Ba",false, 90);
        Grm1.children.add(pa);

        Human Grf1 = new Human("Ded",true, 90);
        Grf1.children.add(pa);

         Human Grm2 = new Human("Ba",false, 90);
        Grm2.children.add(ma);

        Human Grf2 = new Human("Ded",true, 90);
        Grf2.children.add(ma);

        System.out.println(ch1);
        System.out.println(ch2);
        System.out.println(ch3);
        System.out.println(ma);
        System.out.println(pa);
        System.out.println(Grf1);
        System.out.println(Grf2);
        System.out.println(Grm1);
        System.out.println(Grm2);

}

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = new ArrayList<Human>();
        }
        //напишите тут ваш код

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
