package com.javarush.test.level08.lesson11.home09;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args) throws ParseException
    {
        String date ="JANUARY 2 2000";
        System.out.println(date + " = " + isDateOdd(date));

    }

    public static boolean isDateOdd(String date) throws ParseException  {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d yyyy", Locale.ENGLISH);
        Date thisDate = simpleDateFormat.parse(date);


       // System.out.println(thisDate);
        Date yearStartTime = thisDate;
        long t1 = thisDate.getTime();
        //System.out.println(t1);


        yearStartTime.setDate(1);      // первое число
        yearStartTime.setMonth(0);
        //System.out.println(yearStartTime);
        long t2 =yearStartTime.getTime();
        //System.out.println(t2);

        long t = t1 - t2;
        //System.out.println(t);
        long z =24*60*60*1000;
        //System.out.println(z);
        int dayCount = (int) (t/z);
        //System.out.println(dayCount);
        return (dayCount % 2 == 0);


    }
}
