package com.javarush.test.level09.lesson11.home09;

import java.util.*;

/* Десять котов
Создать класс кот – Cat, с полем «имя» (String).
Создать словарь Map(<String, Cat>) и добавить туда 10 котов в виде «Имя»-«Кот».
Получить из Map множество(Set) всех имен и вывести его на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap()
    {
        Map<String, Cat> map = new HashMap<String, Cat>();
        map.put("Bars", new Cat("Bars"));
        map.put("Mars", new Cat("Mars"));
        map.put("Nars", new Cat("Nars"));
        map.put("Dars", new Cat("Dars"));
        map.put("Wars", new Cat("Wars"));
        map.put("Fars", new Cat("Fars"));
        map.put("Lars", new Cat("Lars"));
        map.put("Zars", new Cat("Zars"));
        map.put("Cars", new Cat("Cars"));
        map.put("Pars", new Cat("Pars"));

        return  map;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map)
    {
        Set<Cat> s = new HashSet<Cat>();
        s.addAll(map.values());
        return s;

    }

    public static void printCatSet(Set<Cat> set)
    {
        for (Cat cat:set)
        {
            System.out.println(cat);
        }
    }

    public static class Cat
    {
        private String name;

        public Cat(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return "Cat "+this.name;
        }
    }


}
