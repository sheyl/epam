package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {   String name;
        int age;
        boolean sex;
        int hight;
        int weight;
        boolean education;

        public Human(boolean sex) {
            this.sex = sex;
        }

        public Human(boolean sex, String name) {
            this.sex = sex;
            this.name = name;
        }

        public Human(String name) {
            this.name = name;
        }

        public Human(String name, boolean sex) {
            this.name = name;
            this.sex = sex;
        }

        public Human(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Human(String name, int age, int hight) {
            this.name = name;
            this.age = age;
            this.hight = hight;
        }

        public Human(String name, int age, int hight, int weight) {
            this.name = name;
            this.age = age;
            this.hight = hight;
            this.weight = weight;
        }

        public Human(String name, int age, boolean sex, int hight, int weight) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.hight = hight;
            this.weight = weight;
        }

        public Human(int age, boolean sex, int hight) {
            this.age = age;
            this.sex = sex;
            this.hight = hight;
        }

        public Human(int age, boolean sex, int hight, int weight) {
            this.age = age;
            this.sex = sex;
            this.hight = hight;
            this.weight = weight;
        }

        public Human(String name, int age, boolean sex, int hight, int weight, boolean education) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.hight = hight;
            this.weight = weight;
            this.education = education;
        }
        //напишите тут ваши переменные и конструкторы
    }
}
