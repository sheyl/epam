package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }
        public static ArrayList<String>[] createList()
    {
        ArrayList<String> s1 = new ArrayList<String>();
        s1.add("a");
        s1.add("b");
        s1.add("c");
         ArrayList<String> s2 = new ArrayList<String>();
        s2.add("a");
        s2.add("a");
        s2.add("b");
        s2.add("c");
         ArrayList<String> s3 = new ArrayList<String>();
        s3.add("a");
        s3.add("b");
        s3.add("b");
        s3.add("C");
        s3.add("C");
         ArrayList<String> s4 = new ArrayList<String>();
        s4.add("a");
        s4.add("B");
        s4.add("c");
        s4.add("c");
        s4.add("c");
        s4.add("c");


     ArrayList<String>[] list= new ArrayList[] {s1,s2,s3,s4};


        return list;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}