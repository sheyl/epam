package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public void print (String s) {

    }
    public void print (int s) {

    }
    public void print (boolean s) {

    }
    public void print (double s) {

    }
    public void print (float s) {

    }
}
