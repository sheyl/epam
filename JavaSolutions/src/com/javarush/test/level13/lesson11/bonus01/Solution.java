package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        String data2 ="";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String adres = reader.readLine();
        InputStream inputStream = new FileInputStream(adres);
        while (inputStream.available() >0) {
            int data = inputStream.read();
            data2 +=(char) data;


        }
        BufferedReader reader2 = new BufferedReader(new StringReader(data2) );
        String line = null;
        while ( ( line = reader2.readLine() ) != null && checkInt(line)) {
            if (Integer.parseInt(line)%2 == 0) {
                list.add(Integer.parseInt(line));
            }
        }
        reader2.close();


        reader.close();
        inputStream.close();
        Integer [] array = pusur(list);
        for (Integer x: array) System.out.println(x);

    }
    public static boolean checkInt (String s) {
        try {
            int n = Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }
            return true;
    }

    public static Integer[] pusur (ArrayList<Integer> list) {
        Integer [] arr = new  Integer[list.size()];
        list.toArray(arr);

        int buf;
        for (int i = 0; i <arr.length-1 ; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[j] < arr[i]) {
                    buf = arr [i];
                    arr [i] = arr [j];
                    arr [j] = buf;
            }

        }

        }
        return  arr;
    }
}
