package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести абсолютно все введенные строки в файл, каждую строчку с новой стороки.
*/

import java.io.*;

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();
        String adres = reader.readLine();
        while (true) {
            String data = reader.readLine();

            list.add(data);
            if (data.equals("exit")) break;
        }
        OutputStream outputStream = new FileOutputStream(adres);
        for (String x: list) {
            for (int i = 0; i <x.length() ; i++) {
                int data = (int) x.charAt(i);
                outputStream.write(data);

            }
            if (x.equals(list.get(list.size()-1))) {

            }
            else {
                outputStream.write(13);
                outputStream.write(10);

            }


        }
        reader.close();
        outputStream.close();

    }
}
