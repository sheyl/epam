package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num1 = Integer.parseInt(reader.readLine());
        int num2 = Integer.parseInt(reader.readLine());
        ArrayList<Integer> list1 = findDenominator(num1);
        ArrayList<Integer> list2 = findDenominator(num2);
        System.out.println(nod(list1,list2));



    }
    static ArrayList<Integer> findDenominator(int num) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i <= num ; i++) {
            if ((num % i)== 0 ) list.add(i);

        }

        return list;
    }
    static int nod (ArrayList<Integer> list1, ArrayList<Integer> list2) {
        int nod;
        first: {

            for (int i = list1.size() - 1; i > 0; i--) {
                for (int j = list2.size() - 1; j > 0; j--) {
                    if (list1.get(i) == list2.get(j)) {
                        nod = list1.get(i);
                        break first;
                    }
                }
            }
            nod = 1;
        }
        return nod;
    }

}
