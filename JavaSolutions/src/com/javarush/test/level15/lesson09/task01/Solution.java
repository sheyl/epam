package com.javarush.test.level15.lesson09.task01;

import java.util.HashMap;
import java.util.Map;

/* Статики 1
В статическом блоке инициализировать labels 5 различными парами.
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();
    static {
        labels.put(2.12, "A");
        labels.put(2.13, "s");
        labels.put(2.14, "d");
        labels.put(2.15, "f");
        labels.put(2.16, "g");


    }

    public static void main(String[] args) {
        System.out.println(labels);
    }
}
