package com.javarush.test.level15.lesson12.home01;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


/* Разные методы для разных типов
1. Считать с консоли данные, пока не введено слово "exit".
2. Для каждого значения вызвать метод print. Если значение:
2.1. содержит точку '.', то вызвать метод print для Double;
2.2. больше нуля, но меньше 128, то вызвать метод print для short;
2.3. больше либо равно 128, то вызвать метод print для Integer;
2.4. иначе, вызвать метод print для String.
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList <String> list = new ArrayList<String>();
        while (true) {
            String data = reader.readLine();
            if (data.equals("exit")) break;
            list.add(data);

        }
        for (String x: list) {

            if (x.contains(".")) {
                try {
                    Double num = Double.valueOf(x);
                    print(num);

                }
                catch (Exception e) {
                    print(x);

                }
                continue;
            }




            try {
                Short num = Short.valueOf(x);
                if ((num > 0) && num < 128) {
                    print(num);
                    continue;
                }

            }
            catch (Exception e) {

            }
            try {
                Integer num = Integer.parseInt(x);
                if (num >= 128) {
                    print(num);

                } else print(x);
                continue;

            }
            catch (Exception e) {
                print(x);

            }










        } //for
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
