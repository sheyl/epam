package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

import java.util.ArrayList;

public class Solution {
    public Solution (String s) {

    }
    public Solution (Exception e) {

    }
    public Solution (ArrayList list) {

    }
    private Solution (Character ch) {

    }
    private Solution (Double d) {

    }
    private Solution (Float f) {

    }
    protected Solution (Integer n) {

    }
    protected Solution (Byte b) {

    }
    protected Solution (Long l) {

    }
    Solution() {

    }
    Solution(int [] a) {

    }
    Solution(String [] a) {

    }
}

