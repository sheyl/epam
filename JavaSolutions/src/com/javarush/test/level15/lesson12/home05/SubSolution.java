package com.javarush.test.level15.lesson12.home05;


import java.util.ArrayList;

public class SubSolution extends Solution {
    public SubSolution(String s) {
        super(s);
    }

    public SubSolution(Exception e) {
        super(e);
    }

    public SubSolution(ArrayList list) {
        super(list);
    }

    protected SubSolution(Integer n) {
        super(n);
    }

    protected SubSolution(Byte b) {
        super(b);
    }

    protected SubSolution(Long l) {
        super(l);
    }

     SubSolution() {
    }

    SubSolution(int[] a) {
        super(a);
    }

    SubSolution(String[] a) {
        super(a);
    }
    private SubSolution (Character ch) {

    }
    private SubSolution (Double d) {

    }
    private SubSolution (Float f) {

    }
}
