package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String obj = null;
        ArrayList <String> list = new ArrayList<String>();
        String s = reader.readLine();
        int num = s.indexOf("?");
        s = s.substring(num + 1);
        String [] arr = s.split("&");

        for (String x: arr) {

                if (x.indexOf("=") != (-1)) {
                    String par = x.substring(0, x.indexOf("="));
                    list.add(par);
                    if (par.equals("obj")) obj = x.substring(4, x.length());
                } else list.add(x);

        }
        for (String x: list) {
             System.out.print(x + " ");
        }



        if (obj != null) {
            System.out.println();

                try {
                    alert(Double.parseDouble(obj));
                } catch (Exception e) {
                    alert(obj);
                }

        }

    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }

}
