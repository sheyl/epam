package com.javarush.test.level17.lesson04.task05;

/* МВФ
Singleton паттерн - синхронизация в методе
IMF - это Международный Валютный Фонд
Внутри метода getFund создайте синхронизированный блок
Внутри синхронизированного блока инициализируйте переменную imf так, чтобы метод getFund всегда возвращал один и тот же объект
*/

public class Solution {
    public static class IMF {

        private static IMF imf;

        public static void main(String[] args) {
            Thread t1 = new Thread1();
            Thread t2 = new Thread1();
            t1.start();
            t2.start();

        }

        public static IMF getFund() {
            synchronized (IMF.class) {
                imf = new IMF();
            }
            //add your code here - добавь код тут
            return imf;
        }

        private IMF() {
        }
    }
    public static class Thread1 extends Thread {
        @Override
        public void run() {
            try {
                System.out.println(IMF.getFund());
                sleep(2000);
            } catch (InterruptedException e) {

            }
        }
    }
}
