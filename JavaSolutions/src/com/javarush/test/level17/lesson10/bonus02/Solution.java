package com.javarush.test.level17.lesson10.bonus02;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD 2
CrUD Batch - multiple Creation, Updates, Deletion
!!!РЕКОМЕНДУЕТСЯ выполнить level17.lesson10.bonus01 перед этой задачей!!!

Программа запускается с одним из следующих наборов параметров:
-c name1 sex1 bd1 name2 sex2 bd2 ...
-u id1 name1 sex1 bd1 id2 name2 sex2 bd2 ...
-d id1 id2 id3 id4 ...
-i id1 id2 id3 id4 ...
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет всех людей с заданными параметрами в конец allPeople, выводит id (index) на экран в соответствующем порядке
-u  - обновляет соответствующие данные людей с заданными id
-d  - производит логическое удаление всех людей с заданными id
-i  - выводит на экран информацию о всех людях с заданными id: name sex bd

id соответствует индексу в списке
Формат вывода даты рождения 15-Apr-1990
Все люди должны храниться в allPeople
Порядок вывода данных соответствует вводу данных
Обеспечить корректную работу с данными для множества нитей (чтоб не было затирания данных)
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws java.text.ParseException{
        try {
            String parametr = args[0];
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            switch (parametr) {
                case "-c":
                    for (int i = 1; i <args.length ; i+=3) {
                        creation(args[i],args[i+1],args[i+2]);
                    }

                    break;
                case "-u":
                    for (int i = 1; i <args.length ; i+=4) {
                        update(args[i],args[i+1],args[i+2],args[i+3]);
                    }

                    break;
                case "-d":
                    for (int i = 1; i <args.length ; i++) {
                        delete(args[i]);

                    }

                    break;
                case "-i":
                    for (int i = 1; i <args.length ; i++) {
                        getInfo(args[i]);

                    }
                    break;
            }

        }
        catch ( ArrayIndexOutOfBoundsException e) {

        }
    }

    public static synchronized void creation(String name, String sex, String birthday) throws java.text.ParseException{
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        Date date = format1.parse(birthday);
        switch (sex) {
            case "м":
                allPeople.add(Person.createMale(name, date));

                break;
            case "ж":
                allPeople.add(Person.createFemale(name, date));
                break;
        }
        System.out.println(allPeople.size()-1);

    }

    public static synchronized void update(String id, String name, String sex, String birthday) throws java.text.ParseException {
                    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = format1.parse(birthday);
                    allPeople.get(Integer.parseInt(id)).setBirthDay(date1);
                    allPeople.get(Integer.parseInt(id)).setName(name);
                    switch (sex) {
                        case "м":
                            allPeople.get(Integer.parseInt(id)).setSex(Sex.MALE);
                            break;
                        case "ж":
                            allPeople.get(Integer.parseInt(id)).setSex(Sex.FEMALE);
                            break;
                    }


    }
    public static synchronized void delete(String id) {
        int id2 = Integer.parseInt(id);
        allPeople.get(id2).setBirthDay(null);
    }

    public static synchronized void getInfo(String id2) {
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        int id = Integer.parseInt(id2);
        System.out.print(allPeople.get(id).getName() + " ");
        if (allPeople.get(id).getSex() == Sex.FEMALE) System.out.println("ж");
        else System.out.print("м" + " ");
        System.out.println(format2.format(allPeople.get(id).getBirthDay()));
    }
}
