package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/* Самые частые байты
Ввести с консоли имя файла
Найти байт или байты с максимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<Byte,Integer> map = new HashMap<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        reader.close();
        FileInputStream file = new FileInputStream(name);
        while (file.available() > 0) {
            byte data = (byte) file.read();
            if (map.containsKey(data)){
                int count = (map.get(data) + 1);
                map.put(data, count);
            }
            else map.put(data,1);
        }
        file.close();
        int max = 1;
        for (Integer k : map.values()) {
            if (k > max) max = k;
        }
        for (Byte x: map.keySet()) {
            if (map.get(x) == max) System.out.print(x + " ");
        }
    }
}
