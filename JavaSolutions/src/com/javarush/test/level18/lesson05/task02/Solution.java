package com.javarush.test.level18.lesson05.task02;

/* Подсчет запятых
С консоли считать имя файла
Посчитать в файле количество символов ',', количество вывести на консоль
Закрыть потоки. Не использовать try-with-resources

Подсказка: нужно сравнивать с ascii-кодом символа ','
*/


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;
        int count = 0;
        byte zap = 44;
        try {
            String fileName = reader.readLine();
            fis = new FileInputStream(fileName);


            while (fis.available() > 0) {
                byte data = (byte) fis.read();
                if (data == zap) count++;
            }
            System.out.println(count);

        }
        catch (IOException e) {

        }
        finally {
            try {
                reader.close();
                if (fis != null) fis.close();
            }
            catch (IOException e) {

            }
        }


    }
}
