package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            String filename1 = reader.readLine();
            String filename2 = reader.readLine();
            String filename3 = reader.readLine();
            fis = new FileInputStream(filename1);
            int count = fis.available();

                fos = new FileOutputStream(filename2);
                while (fis.available() > (count/2)) {
                    fos.write(fis.read());
                }
                fos = new FileOutputStream(filename3);
                while (fis.available()>0) {
                     fos.write(fis.read());
                }

        }
        catch (IOException e) {

        }
        finally {
            try {
               reader.close();
                if (fis != null) fis.close();
                if (fos != null) fos.close();

            }
            catch (IOException e) {

            }

        }


    }
}
