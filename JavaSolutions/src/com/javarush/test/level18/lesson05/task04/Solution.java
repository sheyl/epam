package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;
        FileOutputStream fos = null;
        ArrayList<Byte> list = new ArrayList<>();
        try {
            String file1 = bufferedReader.readLine();
            String file2 = bufferedReader.readLine();
            fis = new FileInputStream(file1);
            fos = new FileOutputStream(file2);
            int size = fis.available();
            byte arr[] = new byte[size];
            byte buff[] = new byte[size];
            fis.read(arr);
            for (int i = 0; i <arr.length ; i++) {
                buff[i] = arr[arr.length - i - 1];
            }
            fos.write(buff);

        }
        catch  (IOException e) {

        }
        finally {
            try {
                bufferedReader.close();
                if (fis != null) fis.close();
                if (fos != null) fos.close();
            }
            catch (IOException e) {

            }

        }

    }
}
