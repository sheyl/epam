package com.javarush.test.level18.lesson05.task05;

/* DownloadException
1 Считывать с консоли имена файлов.
2 Если файл меньше 1000 байт, то:
2.1 Закрыть потоки
2.2 выбросить исключение DownloadException
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException {
        BufferedReader reader= new BufferedReader( new InputStreamReader(System.in));
        FileInputStream fis = null;
        try {
            boolean b = true;
            while (b) {
               String fileName = reader.readLine();
                fis = new FileInputStream(fileName);
                if (fis.available() < 1000) {
                    reader.close();
                    fis.close();
                    throw new DownloadException();

                }

            }


        }
        catch (IOException e) {
            System.out.println("Run");
        }
        finally {
            try {
                reader.close();
                if (fis != null) fis.close();
            }
            catch (IOException e) {

            }
        }
    }

    public static class DownloadException extends Exception{

    }
}
