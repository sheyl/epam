package com.javarush.test.level18.lesson10.bonus01;

/* Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        String key = args[0];
        String file1 = args[1];
        String file2 = args[2];

       /* String key = "-e";
        String file1 = "d:/111.txt";
        String file2 = "d:/222.txt";*/

        /*String key = "-d";
        String file1 = "d:/222.txt";
        String file2 = "d:/333.txt";*/

        switch (key) {
            case "-e":
                shifr(file1,file2);
                break;
            case "-d":
                deshifr(file1,file2);
                break;

        }
    }
    public static void shifr(String file1,String file2 ) {
        try  {
            final String KEY = "Дима";
            byte[] key = KEY.getBytes();
            FileInputStream fis = new FileInputStream(file1);
            byte[] text = new byte[fis.available()];
            for (int i = 0; i < text.length ; i++) {
                text[i] = (byte) fis.read();
            }

            fis.close();

            byte[] text2 =new byte[text.length];
            for (int i = 0; i < text.length; i++) {
                text2[i] = (byte) (text[i]^key [i%key.length]);

            }
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(text2);
            fos.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("404");
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }
    public static void deshifr(String file1,String file2 ) {
        try  {
            final String KEY = "Дима";
            byte[] key = KEY.getBytes();
            FileInputStream fis = new FileInputStream(file1);
            byte[] text = new byte[fis.available()];
            for (int i = 0; i < text.length ; i++) {
                text[i] = (byte) fis.read();
            }

            fis.close();

            byte[] text2 =new byte[text.length];
            for (int i = 0; i < text.length; i++) {
                text2[i] = (byte) (text[i]^key [i%key.length]);

            }
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(text2);
            fos.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("404");
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }
}
