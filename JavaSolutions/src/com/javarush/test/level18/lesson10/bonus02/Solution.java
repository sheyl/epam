package com.javarush.test.level18.lesson10.bonus02;

/* Прайсы
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается со следующим набором параметров:
-c productName price quantity
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-c  - добавляет товар с заданными параметрами в конец файла, генерирует id самостоятельно, инкрементируя максимальный id, найденный в файле

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {

        //
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();


        if (args[0].equals("-c")) {
            StringBuilder prodName = new StringBuilder();
            for (int i = 1; i < args.length - 2 ; i++) {
                prodName.append(args[i]);
                prodName.append(" ");

            }

            String productName = prodName.toString();
            if (productName.length() > 30) productName = productName.substring(0,30);
            else productName = String.format("%-30s",productName);

            String price = args[args.length -2];
            if (price.length() > 8) price = price.substring(0,8);
            else price = String.format("%-8s",price);

            String  quantity = args[args.length -1];
            if (quantity.length() > 4) quantity = quantity.substring(0,4);
            else quantity = String.format("%-4s",quantity);

            int id = findMaxId(fileName);
            String curId = "" + id;
            curId = String.format("%-8s",curId);

            //record information
            FileOutputStream fos = new FileOutputStream(fileName,true);
            if (id != 0) {
                fos.write(13);
                fos.write(10);
            }
            fos.write(curId.getBytes());
            fos.write(productName.getBytes());
            fos.write(price.getBytes());
            fos.write(quantity.getBytes());


            fos.close();
        }

    }
    public static int findMaxId( String fileName) throws IOException{
            String line;
            int max = -1;

            BufferedReader reader1 = new BufferedReader(new FileReader(fileName));

            while ((line = reader1.readLine()) != null) {
                if (line.length() == 50) {
                    int x = Integer.parseInt(line.substring(0, 8).trim());
                    if (x > max) max = x;
                }
            }
            int id = max + 1;
            reader1.close();
            return id;
    }
}
