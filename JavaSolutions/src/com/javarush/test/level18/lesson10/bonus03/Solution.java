package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String fileName = reader.readLine();
            reader.close();

            switch (args[0]) {
                case "-u":
                    String id2 = args[1];
                    if (id2.length() > 8) id2 = id2.substring(0,8);
                    else id2 = String.format("%-8s",id2);

                    StringBuilder prod2Name = new StringBuilder();
                    for (int i = 2; i < args.length - 2 ; i++) {
                        prod2Name.append(args[i]);
                        prod2Name.append(" ");

                    }
                    String product2Name = prod2Name.toString();

                    if (product2Name.length() > 30) product2Name = product2Name.substring(0,30);
                    else product2Name = String.format("%-30s",product2Name);

                    String price2 = args[args.length -2];
                    if (price2.length() > 8) price2 = price2.substring(0,8);
                    else price2 = String.format("%-8s",price2);

                    String  quantity2 = args[args.length -1];
                    if (quantity2.length() > 4) quantity2 = quantity2.substring(0,4);
                    else quantity2 = String.format("%-4s",quantity2);
                    update(fileName,id2,product2Name,price2,quantity2);
                        break;


                case "-c":
                    StringBuilder prodName = new StringBuilder();
                for (int i = 1; i < args.length - 2 ; i++) {
                    prodName.append(args[i]);
                    prodName.append(" ");

                }

                String productName = prodName.toString();
                if (productName.length() > 30) productName = productName.substring(0,30);
                else productName = String.format("%-30s",productName);

                String price = args[args.length -2];
                if (price.length() > 8) price = price.substring(0,8);
                else price = String.format("%-8s",price);

                String  quantity = args[args.length -1];
                if (quantity.length() > 4) quantity = quantity.substring(0,4);
                else quantity = String.format("%-4s",quantity);

                int id = findMaxId(fileName);
                String curId = "" + id;
                curId = String.format("%-8s",curId);

                //record information
                FileOutputStream fos = new FileOutputStream(fileName,true);
                if (id != 0) {
                    fos.write(13);
                    fos.write(10);
                }
                fos.write(curId.getBytes());
                fos.write(productName.getBytes());
                fos.write(price.getBytes());
                fos.write(quantity.getBytes());


                fos.close();
                    break;

                case "-d":
                    // перевел id в формат 8 символов
                    String id3 = args[1];
                    if (id3.length() > 8) id3 = id3.substring(0,8);
                    else id3 = String.format("%-8s",id3);
                    delete(id3,fileName);
                    break;

            }



    }
    public static int findMaxId( String fileName) throws IOException{
            String line;
            int max = -1;

            BufferedReader reader1 = new BufferedReader(new FileReader(fileName));

            while ((line = reader1.readLine()) != null) {
                if (line.length() == 50) {
                    int x = Integer.parseInt(line.substring(0, 8).trim());
                    if (x > max) max = x;
                }
            }
            int id = max + 1;
            reader1.close();
            return id;
    }
    public static void delete(String id,String fileName) throws IOException{
        String temp =  fileName + "temp.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;

        // vz9al s neta
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp)));
        while ((line = reader.readLine()) != null) {
            if (line.length() >= 8) {
                if (!line.substring(0,8).equals(id)) {
                    writer.write(line);
                    writer.newLine();

                }
            }
            else {
                writer.newLine();

            }
        }

        writer.close();
        reader.close();

        File file1 = new File(fileName);
        file1.delete();
        File file = new File(temp); // создаем объект на файл test.txt
        if(file.exists()){ // если файл существует, то переименовываем его
            file.renameTo(new File(fileName));
        }
        else{
            System.out.println("File not found!");
        }

    }
    public static void update(String fileName, String id, String productName, String price, String quantity) throws IOException{
        String temp =  fileName + "temp.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;

        // vz9al s neta
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp)));
        while ((line = reader.readLine()) != null) {
            if (line.length() >= 8) {
                if (!line.substring(0,8).equals(id)) {
                    writer.write(line);


                }
                else  writer.write(id+productName+price+quantity);
                writer.newLine();
            }
            else {
                writer.newLine();

            }
        }

        writer.close();
        reader.close();

        File file1 = new File(fileName);
        file1.delete();
        File file = new File(temp); // создаем объект на файл test.txt
        if(file.exists()){ // если файл существует, то переименовываем его
            file.renameTo(new File(fileName));
        }
        else{
            System.out.println("File not found!");
        }
    }
}
