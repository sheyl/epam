package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки. Не использовать try-with-resources
*/


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) {
        String fileName = args[0];
        FileInputStream fis = null;
        String alf = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int count = 0;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            fis = new FileInputStream(fileName);
            while (fis.available() >0) {
                char ch = (char) fis.read();
                stringBuilder.append(ch);
                if (alf.contains(stringBuilder.toString()))  count++;
                stringBuilder.deleteCharAt(0);

            }
            System.out.println(count);
        }
        catch(FileNotFoundException e) {

        }
        catch (IOException e) {

        }
        finally {
            try {
               if (fis != null) fis.close();

            }
            catch (IOException e) {

            }
        }
    }
}
