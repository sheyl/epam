package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Solution {
    public static void main(String[] args) {
        String fileName = args[0];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileName);
            int allSize = fis.available();
            double count = 0;
            while (fis.available() > 0) {
                int ch =  fis.read();
                if (ch == 32) count++;

            }

            double proc = count*100/allSize;

            double newDouble = new BigDecimal(proc).setScale(2, RoundingMode.UP).doubleValue();
            System.out.println(newDouble);
        }
        catch (IOException e) {

        }
        finally {
            try {
               if (fis != null) fis.close();

            }
            catch (IOException e) {

            }
        }
    }
}
