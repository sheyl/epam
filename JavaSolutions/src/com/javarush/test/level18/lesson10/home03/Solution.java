package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать в первый файл содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;
        FileInputStream fis2 = null;

        FileOutputStream fos = null;


        try {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();
            String fileName3 = reader.readLine();
            fis = new FileInputStream(fileName2);
            fos = new FileOutputStream(fileName1,true);
            while (fis.available()>0) {
                fos.write(fis.read());
            }
            fis2 = new FileInputStream(fileName3);
            while (fis2.available()>0) {
                fos.write(fis2.read());
            }
        }
        catch (IOException e) {

        }
        finally {
            try {
               reader.close();
               if (fis != null) fis.close();
               if (fis2 != null) fis2.close();
               if (fos != null) fos.close();


            }
            catch (IOException e) {

            }
        }

    }
}
