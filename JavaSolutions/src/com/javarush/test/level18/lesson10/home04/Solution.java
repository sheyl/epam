package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis1 = null;
        FileInputStream fis2 = null;
        FileOutputStream fos = null;
        try {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

            fis1 = new FileInputStream(fileName1);
            byte buff[] = new byte[fis1.available()];
            fis1.read(buff);
            fis1.close();
            fos =new FileOutputStream(fileName1);
            fis2 = new FileInputStream(fileName2);


            while(fis2.available() > 0) {
                fos.write(fis2.read());
            }
            fos.write(buff);
        }
        catch (IOException e) {

        }
        finally {
            try {
               reader.close();

               if (fis2 != null) fis2.close();

               if (fos != null) fos.close();


            }
            catch (IOException e) {

            }
        }

    }
}
