package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать через пробел во второй файл
Закрыть потоки. Не использовать try-with-resources
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
-3.49 - -3
-3.50 - -3
-3.51 - -4
*/

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Solution {
    public static void main(String[] args) {
 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;

        FileOutputStream fos = null;
        try {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

             fis = new FileInputStream(fileName1);
             int size = fis.available();
             byte buff[] = new byte[size];
             fis.read(buff);
             StringBuilder stringBuilder = new StringBuilder(size);

             for (int i = 0; i < buff.length ; i++) {
                stringBuilder.append((char) buff[i]);

            }
            String arr[] = stringBuilder.toString().split(" ");

            StringBuilder build = new StringBuilder();
            double num2; // округленный даубл до целого
            for (int i = 0; i <arr.length ; i++) {
                double num = Double.parseDouble(arr[i]);

                if (num >=0) num2 = new BigDecimal(num).setScale(0, RoundingMode.HALF_UP).doubleValue();
                else num2 = new BigDecimal(num).setScale(0, RoundingMode.HALF_DOWN).doubleValue();
                build.append((int) num2);
                build.append(" ");
            }
            String str = build.toString().trim();
            byte buff2[] = str.getBytes();
            fos = new FileOutputStream(fileName2);
            fos.write(buff2);

        }
        catch (IOException e) {

        }
        finally {
            try {
               reader.close();

               if (fis != null) fis.close();

               if (fos != null) fos.close();


            }
            catch (IOException e) {

            }
        }
    }
}
