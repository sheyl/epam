package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки. Не использовать try-with-resources

Пример вывода:
, 19
- 7
f 361
*/


import java.io.FileInputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException{
        Map<Byte,Integer> map = new HashMap<>();
        FileInputStream fis = new FileInputStream(args[0]);
        while (fis.available() > 0) {
            Byte b = (byte) fis.read();
            if (map.containsKey(b)) {
                int val = map.get(b);
                val++;

                map.put(b,val);
            }
            else map.put(b,1);
        }
        fis.close();


        Map<Byte,Integer> map2 = new TreeMap<>(map);
        for (Map.Entry<Byte,Integer> entry : map2.entrySet()) {
            System.out.println( (char) ( (byte) entry.getKey() ) + " " + entry.getValue());
        }





    }
}
