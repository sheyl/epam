package com.javarush.test.level18.lesson10.home08;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException{
        BufferedReader reader  = new BufferedReader(new InputStreamReader(System.in));
        String fileName;
        while (!"exit".equals(fileName=reader.readLine())) {
            new ReadThread(fileName).start();

        }
        reader.close();

    }

    public static class ReadThread extends Thread {

        String fileName;

        public ReadThread(String fileName) {
            this.fileName = fileName;

        }

        @Override
        public void run() {
            try {

                Map<Character,Integer> map = new HashMap<>();
                FileInputStream fis = new FileInputStream(fileName);

                while (fis.available() > 0) {
                    char b = (char) fis.read();
                    if (map.containsKey(b)) {
                        int val = map.get(b);
                        val++;

                        map.put(b,val);
                    }
                    else map.put(b,1);
                }
                fis.close();

                int max = 1;
                for (Integer x: map.values()) {
                    if (x > max) max = x;
                }

                for (Character x: map.keySet()) {
                    if (map.get(x) == max) {
                        resultMap.put(fileName, (int) ((char) x));
                        break;
                    }
                }

                }

                catch (IOException e) {

                }

        }

    }
}
