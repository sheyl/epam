package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = null;
        FileOutputStream fos = null;
        ArrayList<String> fileList = new ArrayList<>();
        Map<Integer,String> map = new TreeMap<>();
        String fileName;
        while (!"end".equals(fileName = reader.readLine())) {
            fileList.add(fileName);
        }
        try {
            String file =fileList.get(0).split(".part")[0];

            fos = new FileOutputStream(file, true);
            for (String x: fileList) {
                int part = Integer.parseInt(x.split(".part")[1]);
                map.put(part,x);
            }
            for (Map.Entry<Integer,String> entry : map.entrySet()) {
                fis = new FileInputStream(entry.getValue());
                byte buff[] = new byte[ fis.available()];
                fis.read(buff);
                fos.write(buff);
                fis.close();

            }
        }
        catch(FileNotFoundException e) {

        }

        finally {
            try {
                reader.close();
                if (fis != null) fis.close();
                if (fos != null) fos.close();

            } catch (IOException e) {

            }
        }

    }
}
