package com.javarush.test.level19.lesson03.task03;

/* Адаптация нескольких интерфейсов
Адаптировать IncomeData к Customer и Contact.
Классом-адаптером является IncomeDataAdapter.
Инициализируйте countries перед началом выполнения программы. Соответствие кода страны и названия:
UA Ukraine
RU Russia
CA Canada
Дополнить телефонный номер нулями до 10 цифр при необходимости (смотри примеры)
Обратите внимание на формат вывода фамилии и имени человека
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();
    static {
        countries.put("UA","Ukraine");
        countries.put("RU","Russia");
        countries.put("CA","Canada");
    }



    public static class IncomeDataAdapter implements Contact,Customer{
        private IncomeData incomeData;

        public IncomeDataAdapter(IncomeData incomeData) {
            this.incomeData = incomeData;
        }

        @Override
        public String getName() {
            String name = incomeData.getContactLastName() + ", " + incomeData.getContactFirstName();
            return name;
        }

        @Override
        public String getPhoneNumber() {
            int number = incomeData.getPhoneNumber() ;
            int code = incomeData.getCountryPhoneCode();
            String sNumber = String.format("%010d",number);
            String s1=sNumber.substring(0, 3);
            String s2=sNumber.substring(3, 6);
            String s3 = sNumber.substring(6, 8);
            String s4=sNumber.substring(8);
           return String.format("+%d(%s)%s-%s-%s", code,s1,s2,s3,s4);

        }

        @Override
        public String getCompanyName() {
          return   incomeData.getCompany();
        }

        @Override
        public String getCountryName() {
            String country = incomeData.getCountryCode();
            return countries.get(country);
        }
    }

    public static interface IncomeData {
        String getCountryCode();        //example UA

        String getCompany();            //example JavaRush Ltd.

        String getContactFirstName();   //example Ivan

        String getContactLastName();    //example Ivanov

        int getCountryPhoneCode();      //example 38

        int getPhoneNumber();           //example 501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.

        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan

        String getPhoneNumber();        //example +38(050)123-45-67
    }
}