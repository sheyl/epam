package com.javarush.test.level19.lesson03.task04;


import java.io.File;
import java.io.IOException;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* И еще один адаптер
Адаптировать Scanner к PersonScanner.
Классом-адаптером является PersonScannerAdapter.
Данные в файле хранятся в следующем виде:
Иванов Иван Иванович 31 12 1950

В файле хранится большое количество людей, данные одного человека находятся в одной строке. Метод read() должен читать данные одного человека.
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        File file = new File("D:/tmp.txt");
        Scanner scanner = new Scanner(file);
        PersonScannerAdapter adapter = new PersonScannerAdapter(scanner);

        System.out.println(adapter.read());
        adapter.close();

    }
    public static class PersonScannerAdapter implements PersonScanner{
        private Scanner scanner;

        public PersonScannerAdapter(Scanner scanner) {
            this.scanner = scanner;
        }

        @Override
        public Person read() throws IOException{
            Person person = null;
            String line = null;


            if (scanner.hasNextLine()) line = scanner.nextLine();

            if (line != null) {
                String arr[] = line.split(" ");
                String sDate = String.format("%s %s %s",arr[3],arr[4],arr[5]);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
                Date date = null ;
                try {
                  date = dateFormat.parse(sDate);
                }
                catch (java.text.ParseException e) {

                }
                person = new Person(arr[1],arr[2],arr[0],date);
            }

        return person;

        }

        @Override
        public void close() throws IOException {
            scanner.close();
        }
    }
}
