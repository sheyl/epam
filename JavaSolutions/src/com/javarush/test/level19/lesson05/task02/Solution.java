package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        FileReader reader1 = new FileReader(file);
        Scanner scanner = new Scanner(reader1);

        scanner.useDelimiter("[-.,:=_;()?!\" \t\n\r]+");
        String world = "world";
        int count = 0;
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.equals(world)) {
                count++;
            }
        }
        reader.close();
        scanner.close();
        reader1.close();
        System.out.println(count);
    }
}
