package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        reader.close();
        FileReader file = new FileReader(file1);
        FileWriter fileWriter = new FileWriter(file2);
        BufferedReader reader1 = new BufferedReader(file);
        String line = null;
        while ((line = reader1.readLine())!= null) {
            fileWriter.write(line.replace(".", "!"));
        }

        reader1.close();
        file.close();
        fileWriter.close();
    }
}
