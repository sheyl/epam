package com.javarush.test.level19.lesson08.task04;

/* Решаем пример
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить на консоль решенный пример
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Возможные операции: + - *
Шаблон входных данных и вывода: a [знак] b = c
Отрицательных и дробных чисел, унарных операторов - нет.

Пример вывода:
3 + 6 = 9
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream stream = System.out;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        System.setOut(printStream);
        testString.printSomething();
        StringBuilder stringBuilder = new StringBuilder();

        String str = byteArrayOutputStream.toString();
        stringBuilder.append(str.trim());
        stringBuilder.append(" ");
        String example[] = str.split(" ");
        int member1 = Integer.parseInt( example[0]);
        int member2 = Integer.parseInt( example[2]);
        int result = 0;
        switch (example[1]) {
            case "+":
                result = member1 + member2;
                break;
            case "-":
                result = member1 - member2;
                break;
            case "*":
                result = member1*member2;
                break;

        }

        System.setOut(stream);
        stringBuilder.append(result);
        System.out.println(stringBuilder.toString());
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

