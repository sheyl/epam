package com.javarush.test.level19.lesson10.bonus01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
оригинальный   редактированный    общий
file1:         file2:             результат:(lines)

строка1        строка1            SAME строка1
строка2                           REMOVED строка2
строка3        строка3            SAME строка3
строка4                           REMOVED строка4
строка5        строка5            SAME строка5
строка0                           ADDED строка0
строка1        строка1            SAME строка1
строка2                           REMOVED строка2
строка3        строка3            SAME строка3
строка5                           ADDED строка5
строка4        строка4            SAME строка4
строка5                           REMOVED строка5
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();


    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        reader.close();
        FileReader fileReader1 = new FileReader(file1);
        FileReader fileReader2 = new FileReader(file2);
        Scanner scanner1 = new Scanner(fileReader1);
        Scanner scanner2 = new Scanner(fileReader2);

        while (scanner1.hasNextLine()&scanner2.hasNextLine()) {
            String first = scanner1.nextLine();
            String second = scanner2.nextLine();
            basicCycle(scanner1,scanner2,first,second);

        }

        while (scanner1.hasNextLine()) {
            String first = scanner1.nextLine();
            lines.add(new LineItem(Type.REMOVED, first));

        }
        while (scanner2.hasNextLine()) {
            String second = scanner2.nextLine();
            lines.add(new LineItem(Type.ADDED,second));

        }
        fileReader1.close();
        fileReader2.close();
        scanner1.close();
        scanner2.close();
        System.out.println(lines);
    }



    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
        @Override
        public String toString() {
            return type + " " + line;
        }


    }
    public static void basicCycle(Scanner scanner1, Scanner scanner2, String first, String second) {
        if (first.equals(second)) lines.add(new LineItem(Type.SAME,second));

            // ! equals
            else {
                //в первом файле больше нет строк
                if (!scanner1.hasNextLine() & scanner2.hasNextLine()) {
                    scanner2.nextLine();
                    lines.add(new LineItem(Type.ADDED,second));
                    lines.add(new LineItem(Type.SAME,first));
                }
                //во втором файле больше нет строк
                if (scanner1.hasNextLine()& !scanner2.hasNextLine()) {
                    scanner1.nextLine();
                    lines.add(new LineItem(Type.REMOVED,first));
                    lines.add(new LineItem(Type.SAME,second));

                }

                //есть продолжения
                if (scanner1.hasNextLine()&scanner2.hasNextLine()) {
                   find(scanner1,scanner2,first,second);

                }
            }

    }

    public static void find(Scanner scanner1, Scanner scanner2, String first, String second) {

         String f2 = scanner1.nextLine(); //берем еще 1 строку из сканера1
                    // совпали сторки
                    if (f2.equals(second)) {
                        lines.add(new LineItem(Type.REMOVED, first));
                        lines.add(new LineItem(Type.SAME,second));
                    }
                    // не совпали
                    else {
                        lines.add(new LineItem(Type.ADDED,second));
                        lines.add(new LineItem(Type.SAME,first));
                        scanner2.nextLine();
                        if (scanner2.hasNextLine()) {
                            String s3 = scanner2.nextLine();
                            basicCycle(scanner1, scanner2, f2, s3);
                        }
                        else lines.add(new LineItem(Type.REMOVED, f2));
                    }

    }
}
