package com.javarush.test.level19.lesson10.bonus03;

/* Знакомство с тегами
Считайте с консоли имя файла, который имеет HTML-формат
Пример:
Info about Leela <span xml:lang="en" lang="en"><b><span>Turanga Leela
</span></b></span><span>Super</span><span>girl</span>
Первым параметром в метод main приходит тег. Например, "span"
Вывести на консоль все теги, которые соответствуют заданному тегу
Каждый тег на новой строке, порядок должен соответствовать порядку следования в файле
Количество пробелов, \n, \r не влияют на результат
Файл не содержит тег CDATA, для всех открывающих тегов имеется отдельный закрывающий тег, одиночных тегов нету
Тег может содержать вложенные теги
Пример вывода:
<span xml:lang="en" lang="en"><b><span>Turanga Leela</span></b></span>
<span>Turanga Leela</span>
<span>Super</span>
<span>girl</span>

Шаблон тега:
<tag>text1</tag>
<tag text2>text1</tag>
<tag
text2>text1</tag>

text1, text2 могут быть пустыми
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException{


        String teg = args[0];
        ArrayList<Integer> start = new ArrayList<>();
        ArrayList<Integer> end = new ArrayList<>();
        Map<Integer,Integer> strings = new TreeMap<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        reader.close();
        FileReader fileReader = new FileReader(file);
        Scanner scanner = new Scanner(fileReader);
        StringBuilder str = new StringBuilder();
        while (scanner.hasNextLine()) {
            str.append(scanner.nextLine());
        }
        String text = str.toString().trim();
        Pattern pattern1 = Pattern.compile("<"+teg);
        Pattern pattern2 = Pattern.compile("</"+teg+">");

        Matcher m1 = pattern1.matcher(text);
        while (m1.find()){
            start.add(m1.start());
        }
        Matcher m2 = pattern2.matcher(text);
        while (m2.find()){
            end.add(m2.end());
        }
        int maxRazn = end.get(end.size()-1) - start.get(0);
        int razn = maxRazn;
        int startNum =0;
        for (int i = 0; i < end.size() ; i++) {
            razn = maxRazn;
            for (int j = 0; j < start.size() ; j++) {
                if ((end.get(i)-start.get(j))>0 & (end.get(i)-start.get(j))<razn){
                    razn = (end.get(i)-start.get(j));
                    startNum = j;
                }

            }
            strings.put(start.get(startNum),end.get(i));
            start.remove(startNum);
        }

        for (Map.Entry<Integer,Integer> x : strings.entrySet() ) {
            System.out.println(text.substring(x.getKey(),x.getValue()));
        }


    }
}
