package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        String file = args[0];
        TreeMap<String,Double> map = new TreeMap<>();
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = null;
        while ((line = reader.readLine())!= null) {
            if (line.length()>2) {
                String str[] = line.split(" ");
            if (map.containsKey(str[0])) {
                double val = map.get(str[0]) + Double.parseDouble(str[1]);
                map.put(str[0],val);
            }
            else map.put(str[0],Double.parseDouble(str[1]));
            }
        }
        double max = 0;
        String name = null;
        for (Map.Entry e : map.entrySet()) {
            if (max < (double) e.getValue()) {
                max = (double) e.getValue();
                name = (String) e.getKey();
            }
        }
        System.out.println(name);

        fileReader.close();
        reader.close();

    }
}
