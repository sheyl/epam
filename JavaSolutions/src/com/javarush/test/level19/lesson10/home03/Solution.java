package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException, ParseException{
        SimpleDateFormat format = new SimpleDateFormat("d M yyyy");

        String file = args[0];
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = null;
        StringBuilder name = new StringBuilder();
        int year = 0;
        int month = 0;
        int day = 0;
        while ((line = reader.readLine()) != null) {
            if (line.length()>6) {
                String str[] = line.trim().split(" ");
                year =Integer.parseInt( str[str.length-1]);
                month =Integer.parseInt( str[str.length-2]);
                day =Integer.parseInt( str[str.length-3]);
                for (int i = 0; i < str.length-3 ; i++) {
                    name.append(str[i]);
                    name.append(" ");

                }

                Date birthday = format.parse(day + " " + month + " " + year);
                PEOPLE.add(new Person(name.toString().trim(),birthday));
                name.delete(0,name.length());

            }

        }

        reader.close();
        fileReader.close();

    }

}
