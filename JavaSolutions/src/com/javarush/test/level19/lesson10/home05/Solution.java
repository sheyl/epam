package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит строки со слов, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

import java.util.Scanner;
import java.util.regex.*;

public class Solution {



    public static void main(String[] args) throws IOException{

        String file1 = args[0];
        String file2 = args[1];
        FileReader fileReader =new FileReader(file1);
        FileWriter fileWriter = new FileWriter(file2);
        Scanner scanner = new Scanner(fileReader);
        Matcher matcher;
        Pattern p =  Pattern.compile("\\D*[0-9].*");
        while (scanner.hasNext()) {
            String str = scanner.next();
            matcher = p.matcher(str);
            if (matcher.matches()) fileWriter.write(str + " ");

        }

        scanner.close();
        fileReader.close();
        fileWriter.close();

    }

}
