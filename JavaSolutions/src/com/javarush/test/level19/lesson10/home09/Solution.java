package com.javarush.test.level19.lesson10.home09;

/* Контекстная реклама
В методе main подмените объект System.out написанной вами реадер-оберткой
Ваша реадер-обертка должна выводить на консоль контекстную рекламу после каждого второго println-а
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Рекламный текст: "JavaRush - курсы Java онлайн"

Пример вывода:
first
second
JavaRush - курсы Java онлайн
third
fourth
JavaRush - курсы Java онлайн
fifth
*/

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();


    public static void main(String[] args) throws IOException{
        String reklama  = "JavaRush - курсы Java онлайн";
        PrintStream printStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
         //создаем адаптер к классу PrintStream
         PrintStream stream = new PrintStream(outputStream);
         //Устанавливаем его как текущий System.out
         System.setOut(stream);
        testString.printSomething();
        String text  = outputStream.toString();
        String arr[] = text.split("\\n");
        System.setOut(printStream);
        for (int i = 0; i < arr.length ; i++) {
            System.out.println(arr[i]);
            if (i>0&i%2==1) System.out.println(reklama);
        }
        outputStream.close();
        stream.close();


    }

    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }
}
