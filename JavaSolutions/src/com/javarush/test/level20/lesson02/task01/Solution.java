package com.javarush.test.level20.lesson02.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* Читаем и пишем в файл: Human
Реализуйте логику записи в файл и чтения из файла для класса Human
Поле name в классе Human не может быть пустым
В файле your_file_name.tmp может быть несколько объектов Human
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) throws  Exception{
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        //try {

           // File your_file_name = File.createTempFile("your_file_name", null);
            File your_file_name = new File("d:/1.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            Human ivanov = new Human("Ivanov", new Asset("home"), new Asset("car"));
            ivanov.save(outputStream);
            outputStream.flush();

            Human somePerson = new Human();
            somePerson.load(inputStream);

            if (ivanov.name.equals(somePerson.name)) System.out.println("equals");
            else System.out.println("no");
            System.out.println( ivanov.assets.get(0).getName());
            System.out.println( ivanov.assets.get(0).getPrice());
            System.out.println( ivanov.assets.get(1).getName());
            System.out.println( ivanov.assets.get(1).getPrice());

            System.out.println( somePerson.assets.get(0).getName());
            System.out.println( somePerson.assets.get(0).getPrice());
            System.out.println( somePerson.assets.get(1).getName());
            System.out.println( somePerson.assets.get(1).getPrice());



        //if (ivanov.name.equals(somePerson.name)) System.out.println("equals");

            //check here that ivanov equals to somePerson - проверьте тут, что ivanov и somePerson равны
            inputStream.close();

        /*} catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }*/
    }


    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.println(name);
            String isHasAsset = assets != null ? "yes":"no";
            printWriter.println(isHasAsset);
            if (assets!=null) {
                printWriter.println(assets.size());
            for (int i = 0; i < assets.size() ; i++) {
                printWriter.println(assets.get(i).getName());
                String isHasPrice = assets.get(i).getPrice() != 0 ? "yes":"no";
                printWriter.println(isHasPrice);

                if (assets.get(i).getPrice() != 0 ) {
                    printWriter.println(assets.get(i).getPrice());
                }
            }
            }
            printWriter.flush();


        }

        public void load(InputStream inputStream) throws Exception {

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            name = reader.readLine();
            String isHasAsset = reader.readLine();
            if (isHasAsset.equals("yes")) {
                Asset asset;
                int size = Integer.parseInt(reader.readLine());
                for (int i = 0; i < size ; i++) {
                    asset = new Asset(reader.readLine());
                    String isHasPrice = reader.readLine();
                    if (isHasPrice.equals("yes")) {
                        asset.setPrice(Double.parseDouble(reader.readLine()));
                    }

                    assets.add(assets.size(),asset);
                }
            }
        }
    }
}
