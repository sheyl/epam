package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) throws  Exception{
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу

            File your_file_name = new File("d:/1.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            OutputStream outputStream2 = new FileOutputStream("d:/2.txt");

            InputStream inputStream = new FileInputStream(your_file_name);

            JavaRush javaRush = new JavaRush();
            User u1 = new User();
            u1.setBirthDate(new Date());
            u1.setCountry(User.Country.RUSSIA);
            u1.setFirstName("Valera");
            u1.setLastName("Pupkin");
            u1.setMale(true);
            javaRush.users.add(u1);
            javaRush.users.add(new User());
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            javaRush.save(outputStream);
            outputStream.flush();

          JavaRush loadedObject = new JavaRush();
          loadedObject.load(inputStream);
        loadedObject.save(outputStream2);
        outputStream2.flush();
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

         /*catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }*/
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter printWriter = new PrintWriter(outputStream);
            String hasList = users.size() != 0 ? "yes" : "no";
            printWriter.println(hasList);
            if (users.size() != 0 ) {
               printWriter.println(users.size());
                for (int i = 0; i < users.size(); i++) {
                    User user = users.get(i);
                    printWriter.println(user.getFirstName());

                    printWriter.println(user.getLastName());
                    String hasDate = user.getBirthDate() != null ? "yes" : "no";
                    printWriter.println(hasDate);
                    if (user.getBirthDate() != null ) {
                        printWriter.println(user.getBirthDate().getTime());
                    }

                    printWriter.println(user.isMale());
                    String hasCountry = user.getCountry() != null ? "yes" : "no";
                    printWriter.println(hasCountry);
                    if (user.getCountry() != null) {
                        printWriter.println(user.getCountry().getDisplayedName());
                    }



                }
            }
            printWriter.flush();

        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            if (reader.readLine().equals("yes")) {
                int listSize = Integer.parseInt(reader.readLine());
                for (int i = 0; i < listSize ; i++) {
                    User user  =new User();
                    user.setFirstName(reader.readLine());
                    user.setLastName(reader.readLine());
                    if (reader.readLine().equals("yes")) {
                        long time = Long.parseLong(reader.readLine());
                        Date d = new Date();
                        d.setTime(time);
                        user.setBirthDate(d);
                    }
                    if (reader.readLine().equals("true")) {
                        user.setMale(true);
                    }
                    if (reader.readLine().equals("yes")) {
                        String country = reader.readLine();
                        switch (country) {
                            case "Russia" :
                                 user.setCountry(User.Country.RUSSIA);
                                break;
                            case "Ukraine" :
                                 user.setCountry(User.Country.UKRAINE);
                                break;
                            default:
                                 user.setCountry(User.Country.OTHER);
                        }

                    }
                    users.add(user);
                }

            }

        }
    }
}
