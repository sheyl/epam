package com.javarush.test.level20.lesson02.task03;

import java.io.*;
import java.util.*;

/* Знакомство с properties
В методе fillInPropertiesMap считайте имя файла с консоли и заполните карту properties данными из файла.
Про .properties почитать тут - http://ru.wikipedia.org/wiki/.properties
Реализуйте логику записи в файл и чтения из файла для карты properties.
*/
public class Solution implements Serializable{
    public static void main(String[] args) throws Exception{
        Locale.setDefault(Locale.ENGLISH);
        Solution solution = new Solution();
        properties.put("123","1245");

        solution.fillInPropertiesMap();
        FileOutputStream fos = new FileOutputStream("d:/123.txt");
        solution.save(fos);
        properties.put("12345","1245");
        fos.close();
        //properties.clear();
        FileInputStream fis = new FileInputStream("d:/123.txt");
        solution.load(fis);
        for (String key: properties.keySet()) System.out.println(key + " " + properties.get(key));
    }
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String path = reader.readLine();
           // bla bla bla String path = "com.javarush.test.level26.lesson15.big01.resources.common_en";
            ResourceBundle bundle = ResourceBundle.getBundle(path);
            Enumeration<String> keys = bundle.getKeys();
            while(keys.hasMoreElements()) {
                String key = keys.nextElement();
                properties.put(key,bundle.getString(key));
            }
        }
        catch (IOException e) {

        }
    }

    public void save(OutputStream outputStream) throws Exception {
        if (outputStream != null ) {
            if (outputStream instanceof FileOutputStream) {
                ObjectOutputStream out = null;
                try{
                    out = new ObjectOutputStream(outputStream);
                    out.writeObject(properties);
                }
                finally {
                    if (out != null) out.close();
                }
            }
        }
        else System.out.println("outputstream  = null");
    }

    public void load(InputStream inputStream) throws Exception {
        if (inputStream != null ) {
            if (inputStream instanceof FileInputStream) {
                ObjectInputStream in = null;
                try{
                    in = new ObjectInputStream (inputStream);
                    properties = (Map<String,String>) in.readObject();
                }
                finally {
                    if (in != null) in.close();
                }
            }
        }
        else System.out.println("inputstream  = null");
    }
}
