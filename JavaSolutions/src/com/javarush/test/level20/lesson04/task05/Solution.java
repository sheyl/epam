package com.javarush.test.level20.lesson04.task05;

import java.io.*;

/* Как сериализовать что-то свое?
Сделайте так, чтобы сериализация класса Object была возможной
*/
public class Solution {
    public static void main(java.lang.String[] args) throws Exception{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream ous = new ObjectOutputStream(baos);
        Object ob1 = new Object();
        ob1.string1 = new String();
        ob1.string2 = new String();
        ous.writeObject(ob1);
        ous.flush();
        baos.flush();
        ous.close();
        baos.close();
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object ob2 = (Object) ois.readObject();
        ois.close();
        bais.close();
        ob2.string1.print();
        ob2.string2.print();
    }
    public static class Object implements Serializable{
        public String string1;
        public String string2;
    }

    public static int countStrings;

    public static class String implements Serializable{
        private final int number;

        public String() {
           number = ++countStrings;
        }

        public void print() {
            System.out.println("string #" + number);
        }
    }
}
