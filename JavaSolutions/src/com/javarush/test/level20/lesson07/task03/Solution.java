package com.javarush.test.level20.lesson07.task03;

import java.io.*;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* Externalizable Person
Класс Person должен сериализоваться с помощью интерфейса Externalizable.
Подумайте, какие поля не нужно сериализовать.
Исправьте ошибку сериализации.
Сигнатуры методов менять нельзя.
*/
public class Solution {
public static void main(String[] args) throws Exception{
        Person person = new Person("f","l",30);
        person.father = new Person("father","123",70);
        person.mother = new Person("mother","123",70);
        List<Person> list = new ArrayList<>();
        list.add(person);
       person.children = list;
      //  System.out.println(person.children.size());
      // person.children.add(new Person("father000","123",70));
        Person person2;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream ous = new ObjectOutputStream(baos);
        ous.writeObject(person);
        ous.flush();
        ous.close();
        baos.flush();
        baos.close();


        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);

        person2 = (Person) ois.readObject();
        System.out.println(person2.age);
        System.out.println(person2.firstName);
        System.out.println(person2.lastName);
        System.out.println(person2.father.lastName);
        System.out.println(person2.father.age);
        System.out.println(person2.children.get(0).lastName);
    }
    public static class Person implements Externalizable {
        private String firstName;
        private String lastName;
        private int age;
        private Person mother;
        private Person father;
        private List<Person> children;

        public Person(String firstName, String lastName, int age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            children = null;
        }
        public Person() {

        }

        public void setMother(Person mother) {
            this.mother = mother;
        }

        public void setFather(Person father) {
            this.father = father;
        }

        public void setChildren(List<Person> children) {
            this.children = children;
        }

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {
            out.writeObject(mother);
            out.writeObject(father);
            out.writeObject(firstName);
            out.writeObject(lastName);
            out.writeInt(age);
            out.writeObject(children);
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

            mother = (Person)in.readObject();
            father = (Person)in.readObject();
            firstName = (String) in.readObject();
            lastName = (String) in.readObject();

            age = in.readInt();
            children = (List)in.readObject();
        }
    }
}
