package com.javarush.test.level20.lesson10.bonus01;



import java.util.ArrayList;



/* Алгоритмы-числа
Число S состоит из M чисел, например, S=370 и M(количество цифр)=3
Реализовать логику метода getNumbers, который должен среди натуральных чисел меньше N (long)
находить все числа, удовлетворяющие следующему критерию:
число S равно сумме его цифр, возведенных в M степень
getNumbers должен возвращать все такие числа в порядке возрастания

Пример искомого числа:
370 = 3*3*3 + 7*7*7 + 0*0*0
8208 = 8*8*8*8 + 2*2*2*2 + 0*0*0*0 + 8*8*8*8

На выполнение дается 10 секунд и 50 МБ памяти.
*/
public class Solution {

    public static void main(String[] args) {
        Long t0 = System.currentTimeMillis();
        //long memoryStart = Runtime.getRuntime().freeMemory();
        int mass[] = getNumbers(100000000);

        for (int x : mass) System.out.print(x + " ");
        //long memoryEnd = Runtime.getRuntime().freeMemory();

        Long t1 = System.currentTimeMillis();
        System.out.println();
        System.out.println("Memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024d) + " Mb.");
        System.out.println("Time: " + (t1 - t0) / 1000d + " sec.");
    }

    public static int[] getNumbers(int N) {
        // cоздаем лист щначений
        ArrayList<Integer> list = new ArrayList<>();
        //определяем длину максимального числа
        String s = Integer.toString(N);
        int size = s.length();
        //заполняем матрицу степеней от 1 до длины числа для чисел от 1 до 9
        int matrix[][] = new int[10][size + 1];
        for (int i = 0; i <= 9; i++) {  //  i - цифра от 0 до 9
            for (int j = 1; j <= size; j++) { // j - степень числа от 1 до максимального
                int res = 1;
                for (int k = 1; k <= j; k++) {
                    res *= i;
                }
                matrix[i][j] = res;
            }
        }


        int[] result;
        // для все чисел цикл
        for (int p = 1; p < N; p++) {
            int n = p;
            // если одинарное число - сразу в лист
            if (p < 10) list.add(n);

            int c = 0;   //количество цифр - - степень
            for (int i = n; i >= 10; i /= 10) {
                int k = i % 10;
                int m = (i / 10) % 10;
                c++;
                if (k >= m || k == 0) { //просеевание из известного источника
                    if (i < 100) {//если полностью удовлетворяет схеме числа
                        c++;
                        ///////////находим сумму
                        int sum = 0;
                        int j = p;
                        while (j > 0) {
                            sum += matrix[j % 10][c];
                            j /= 10;
                        }
                        //определяем кол-во цифр в сумме
                        int size2 = Integer.toString(sum).length();
                        // если сумма меньше максимального числа (введенного) и совпадает разрядность
                        if (sum < N && c == size2) {
                            //определим сумму чисел суммы)))
                            int sum2 = 0;
                            int j2 = sum;
                            while (j2 > 0) {
                                sum2 += matrix[j2 % 10][c];
                                j2 /= 10;
                            }
                            // если число Армстронга
                            if (sum == sum2) {
                                if (!list.contains(sum))
                                    list.add(sum);
                            }
                        }
                    }
                } else break;
            }
        }

        result = new int[list.size()];
        for (int j = 0; j < list.size(); j++) {
            result[j] = list.get(j);
        }
        // пузырьковая сортировка массива
        int buff;
        for (int i = 0; i < result.length - 1; i++) {
            for (int j = i + 1; j < result.length; j++) {
                if (result[j] < result[i]) {
                    buff = result[i];
                    result[i] = result[j];
                    result[j] = buff;
                }
            }
        }
        return result;
    }

}
