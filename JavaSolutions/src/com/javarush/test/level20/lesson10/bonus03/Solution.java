package com.javarush.test.level20.lesson10.bonus03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Кроссворд
1. Дан двумерный массив, который содержит буквы английского алфавита в нижнем регистре.
2. Метод detectAllWords должен найти все слова из words в массиве crossword.
3. Элемент(startX, startY) должен соответствовать первой букве слова, элемент(endX, endY) - последней.
text - это само слово, располагается между начальным и конечным элементами
4. Все слова есть в массиве.
5. Слова могут быть расположены горизонтально, вертикально и по диагонали как в нормальном, так и в обратном порядке.
6. Метод main не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        List<Word> list = detectAllWords(crossword, "roel", "nlo", "rek","darr", "erpo", "home", "same", "vorg");
        for (Word x: list) System.out.println(x);

        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        int [][] cross = crossword;
        int maxI = cross.length;
        int maxJ = cross[0].length;
        // итоговый лист
        List<Word> list = new ArrayList<>();
        //лист из входных слов
        List<String> word = new ArrayList<>();
        for (String x:words) {
            word.add(x.toLowerCase());

        }

        //поиск слов
        for (String x:word) {
            // длина слова для поиска
            int size = x.length();
            //первая буква слова
            int firstLetter = x.charAt(0);

            //карта с возможной позицией первой буквы в исходном массиве
            Map<Integer,Integer> letterPosition = new HashMap<>();
            for (int i = 0; i < maxI ; i++) {
                for (int j = 0; j < maxJ ; j++) {
                    // если в массиве есть эта буква кладем в карту ее значение
                    if ( cross[i][j] == firstLetter) {
                        letterPosition.put(i,j);
                    }
                }
            }
            for (Map.Entry<Integer,Integer> entry: letterPosition.entrySet()) {

                int i = entry.getKey();
                int j = entry.getValue();

                //проверяем возможность нахождения слова по горизонтали вправо от буквы
                if ((j+(size - 1)) <= (maxJ-1) ) {

                    StringBuffer sWord = new StringBuffer();
                    for (int k = j; k < j+size ; k++) {
                        sWord.append((char) cross[i][k]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j+(size - 1)),i);
                        list.add(w);
                    }
                }
                //влево
                if ((j-(size - 1)) >=0 ) {

                    StringBuffer sWord = new StringBuffer();
                    for (int k = j; k > j-size ; k--) {
                        sWord.append((char) cross[i][k]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j-(size - 1)),i);
                        list.add(w);
                    }
                }

                //вверх
                if ((i-(size - 1)) >=0 ) {

                    StringBuffer sWord = new StringBuffer();
                    for (int k = i; k > i-size ; k--) {
                        sWord.append((char) cross[k][j]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint(j,(i-(size - 1)));
                        list.add(w);
                    }
                }
                //вниз
                if ((i+(size - 1)) <= (maxI-1) ){

                    StringBuffer sWord = new StringBuffer();
                    for (int k = i; k < i+size ; k++) {
                        sWord.append((char) cross[k][j]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint(j,(i+(size - 1)));
                        list.add(w);
                    }
                }
                //вверх впрво
                if ((i-(size - 1)) >=0 && (j+(size - 1)) <= (maxJ-1) ) {

                    StringBuffer sWord = new StringBuffer();
                    int m = i+1;
                    for (int k = j; k < j+size ; k++) {
                        m--;
                        sWord.append((char) cross[m][k]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j+(size - 1)),(i-(size - 1)));
                        list.add(w);
                    }
                }
                //вниз вправо
                if ((i+(size - 1)) <= (maxI-1) && (j+(size - 1)) <= (maxJ-1)){

                    StringBuffer sWord = new StringBuffer();
                    int m = i-1;
                    for (int k = j; k < j+size ; k++) {
                        m++;
                        sWord.append((char) cross[m][k]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j+(size - 1)),(i+(size - 1)));
                        list.add(w);
                    }
                }
                //вниз влево
                if ((i+(size - 1)) <= (maxI-1) && (j-(size - 1)) >=0){

                    StringBuffer sWord = new StringBuffer();
                    int m = i-1;
                    for (int k = j; k > j-size ; k--) {
                        m++;
                        sWord.append((char) cross[m][k]);
                    }
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j-(size - 1)),(i+(size - 1)));
                        list.add(w);
                    }
                }
                //вверх влево
                if ((i-(size - 1)) >=0 && (j-(size - 1)) >=0) {

                    StringBuffer sWord = new StringBuffer();
                     int m = i+1;
                    for (int k = j; k > j-size ; k--) {
                        m--;
                        sWord.append((char) cross[m][k]);
                    }
                    System.out.println(sWord);
                    //проверяем на совпадение со словом
                    if (sWord.toString().equals(x)) {
                        Word w = new Word(x);
                        w.setStartPoint(j,i);
                        w.setEndPoint((j-(size - 1)),(i-(size - 1)));
                        list.add(w);
                    }
                }



            }


        }



        return list;
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}
