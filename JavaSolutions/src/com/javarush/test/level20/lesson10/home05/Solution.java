package com.javarush.test.level20.lesson10.home05;

import java.io.*;
import java.util.logging.Logger;

/* Сериализуйте Person
Сериализуйте класс Person стандартным способом. При необходимости поставьте полям модификатор transient.
*/
public class Solution {

    public static void main(String[] args) throws Exception{
        Person person = new Person("dim","raf","bel",Sex.MALE);
        System.out.println(person.logger);
        System.out.println(person.outputStream);
        System.out.println(person.fullName);
        System.out.println(person.greetingString);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream ous = new ObjectOutputStream(baos);
        ous.writeObject(person);
        baos.flush();
        ous.flush();
        baos.close();
        ous.close();
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
        Person b2 = (Person) ois.readObject();
        System.out.println(b2.sex);
        System.out.println(b2.logger);
        System.out.println(b2.outputStream);
        System.out.println(b2.fullName);
        System.out.println(b2.greetingString);
    }

    public static class Person implements Serializable{
        String firstName;
        String lastName;
        transient String fullName;
        transient final String greetingString;
        String country;
        Sex sex;
        transient PrintStream outputStream;
        transient Logger logger;

        Person(String firstName, String lastName, String country, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.greetingString = "Hello, ";
            this.country = country;
            this.sex = sex;
            this.outputStream = System.out;
            this.logger = Logger.getLogger(String.valueOf(Person.class));
        }

    }

    enum Sex {
        MALE,
        FEMALE
    }
}
