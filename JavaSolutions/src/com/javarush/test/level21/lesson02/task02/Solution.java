package com.javarush.test.level21.lesson02.task02;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* Сравниваем модификаторы
Реализовать логику метода isAllModifiersContainSpecificModifier, который проверяет,
содержит ли переданный параметр allModifiers значение конкретного модификатора specificModifier
*/
public class Solution {
    public static void main(String[] args) {
        int modifiersOfThisClass = Solution.class.getModifiers();
        System.out.println(isAllModifiersContainSpecificModifier(modifiersOfThisClass, Modifier.PUBLIC));   //true
        System.out.println(isAllModifiersContainSpecificModifier(modifiersOfThisClass, Modifier.STATIC));   //false

        int modifiersOfMethod = getMainMethod().getModifiers();
        System.out.println(isAllModifiersContainSpecificModifier(modifiersOfMethod, Modifier.STATIC));      //true
    }

    public static boolean isAllModifiersContainSpecificModifier(int allModifiers, int specificModifier) {
        if (specificModifier == Modifier.STATIC) {
            if (Modifier.isStatic(allModifiers)) return true;
            else return false;
        }

        else if (specificModifier == Modifier.ABSTRACT) {
            if (Modifier.isAbstract(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.FINAL) {
            if (Modifier.isFinal(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.INTERFACE) {
            if (Modifier.isInterface(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.NATIVE) {
            if (Modifier.isNative(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.PRIVATE) {
            if (Modifier.isPrivate(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.PROTECTED) {
            if (Modifier.isProtected(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.PUBLIC) {
            if (Modifier.isPublic(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.STATIC) {
            if (Modifier.isStatic(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.STRICT) {
            if (Modifier.isStrict(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.SYNCHRONIZED) {
            if (Modifier.isSynchronized(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.VOLATILE) {
            if (Modifier.isVolatile(allModifiers)) return true;
            else return false;
        }
        else if (specificModifier == Modifier.TRANSIENT) {
            if (Modifier.isTransient(allModifiers)) return true;
            else return false;
        }
        else return false;
    }

    private static Method getMainMethod() {
        Method[] methods = Solution.class.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equalsIgnoreCase("main")) return method;
        }

        return null;
    }
}
