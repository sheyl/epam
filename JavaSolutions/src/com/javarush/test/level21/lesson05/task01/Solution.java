package com.javarush.test.level21.lesson05.task01;

import java.util.HashSet;
import java.util.Set;

/* Equals and HashCode
В классе Solution исправить пару методов equals/hashCode в соответствии с правилами реализации этих методов.
Метод main не участвует в тестировании.
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj== this)
            return true;
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Solution n = (Solution) obj;
        return (first == n.first ||(first != null && first.equals(n.first))) && (last == n.last || (last != null && last.equals(n.last)));
    }

    @Override public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((first == null) ? 0 : first.hashCode());
        result = prime * result + ((last == null) ? 0 : last.hashCode());
        return result; }






    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        s.add(new Solution("Donald", "Duck"));
        s.add(new Solution("Donald", null));
        s.add(new Solution(null, "Duck"));
        s.add(new Solution(null, null));


        System.out.println(s.contains(new Solution("Donald", "Duck")));
        System.out.println(s.contains(new Solution("Donald", null)));
        System.out.println(s.contains(new Solution(null, "Duck")));
        System.out.println(s.contains(new Solution(null, null)));

    }
}
