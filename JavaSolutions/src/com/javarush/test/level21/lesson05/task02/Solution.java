package com.javarush.test.level21.lesson05.task02;

import java.util.HashSet;
import java.util.Set;

/* Исправить ошибку
Сравнение объектов Solution не работает должным образом. Найти ошибку и исправить.
Метод main не участвует в тестировании.
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((first == null) ? 0 : first.hashCode());
        result = prime * result + ((last == null) ? 0 : last.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (obj== this)
            return true;
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Solution n = (Solution) obj;
        return (first == n.first ||(first != null && first.equals(n.first))) && (last == n.last || (last != null && last.equals(n.last)));
    }

    public static void main(String[] args) {
        Solution s2 = null;
        Solution s3 = new Solution(null,null);
        System.out.println(s2 instanceof Solution);
        System.out.println(s3 instanceof Solution);
        Set<Solution> s = new HashSet<>();
        s.add(new Solution("Mickey", "Mouse"));
        System.out.println(s.contains(new Solution("Mickey", "Mouse")));
    }
}
