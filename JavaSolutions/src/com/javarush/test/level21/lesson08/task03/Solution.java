package com.javarush.test.level21.lesson08.task03;

/* Запретить клонирование
Разрешите клонировать класс А
Запретите клонировать класс B
Разрешите клонировать класс C
Метод main не участвует в тестировании.
*/
public class Solution {
    public static void main(String[] args) throws CloneNotSupportedException{
        A a1 = new A(1,3);
        B b1 = new B(7,3,"qwerty");
        B bClone = b1.clone();
        C c1 = new C(1,2,"123");
        C clone = c1.clone();
    }

    public static class A implements Cloneable {
        @Override
        public A clone() throws CloneNotSupportedException {
            return (A) super.clone();
        }

        private int i;
        private int j;

        public A(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }

    public static class B extends A {
        @Override
        public B clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }

        private String name;

        public B(int i, int j, String name) {
            super(i, j);
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class C extends B {
        @Override
        public C clone() throws CloneNotSupportedException {
            C clone = new C(super.getI(),super.getJ(),super.getName());
            return clone;
        }

        public C(int i, int j, String name) {
            super(i, j, name);
        }
    }
}
