package com.javarush.test.level21.lesson16.big01;


import java.util.ArrayList;

public class Hippodrome {
    public static Hippodrome game;
    ArrayList<Horse> horses = new ArrayList<>();


    public ArrayList<Horse> getHorses() {
        return horses;
    }
    public void move() {
        for (Horse x: horses) {
            x.move();
        }

    }
    public void run() {
        for (int i = 0; i < 100; i++) {
            try{
                move();
                print();
                Thread.sleep(200);
            }
            catch (InterruptedException e) {

            }

        }
    }
    public void print() {
        for (Horse x: horses) {
            x.print();
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }
    public Horse getWinner() {
        double maxDistance = horses.get(0).getDistance();
        Horse winner = horses.get(0);
        for (int i = 1; i < horses.size() ; i++) {

            if (horses.get(i).getDistance() > maxDistance) {
                maxDistance = horses.get(i).getDistance();
                winner = horses.get(i);
            }
        }
        return winner;
    }
    public void printWinner() {
        Horse winner =  this.getWinner();
        System.out.println("Winner is " + winner.getName()+ "!");
    }

    public static void main(String[] args) {

        game = new Hippodrome();

        Horse horse1 = new Horse("horse1",3,0);
        Horse horse2 = new Horse("horse2",3,0);
        Horse horse3 = new Horse("horse3",3,0);

        game.getHorses().add(horse1);
        game.getHorses().add(horse2);
        game.getHorses().add(horse3);
        game.run();
        game.printWinner();

    }
}
