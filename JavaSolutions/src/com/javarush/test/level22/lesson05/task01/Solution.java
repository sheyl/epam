package com.javarush.test.level22.lesson05.task01;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
Метод main не участвует в тестировании.
*/
public class Solution {
    public static void main(String[] args) throws TooShortStringException{
        System.out.println( getPartOfString("JavaRush - лучший сервис обучения Java.")); // true
       // System.out.println( getPartOfString("    ")); // empty - fale
        System.out.println( getPartOfString("1   4 ")); // empty - fale
        System.out.println( getPartOfString("1   4 5")); // empty - fale
        System.out.println( getPartOfString("1   4 5 3")); // empty - fale
       // System.out.println( getPartOfString(null)); // null - fale
       // System.out.println( getPartOfString("   qwr")); // 3 spaces - false
        //System.out.println( getPartOfString("    5")); // 4 sp + not sp - true
       // System.out.println( getPartOfString("1 2 3 4 ")); // 4 sp (3 + 1 last sp) - false
       // System.out.println( getPartOfString("1 2 3 4 5 ")); // true
        //System.out.println( getPartOfString("1 2 3 4  5")); // false

    }
    public static String getPartOfString(String string) throws TooShortStringException{
        if (string == null) throw new TooShortStringException();
        if (string.isEmpty()) throw new TooShortStringException();
        ArrayList<Integer> start = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            start.add(matcher.start());        }

            if (start.size() < 4) throw new TooShortStringException();
            if (start.size() == 4) {
               return string.substring(start.get(0) + 1,string.length());
            }
            else {
                return string.substring(start.get(0) + 1,start.get(4));
            }
    }



    public static class TooShortStringException extends Exception{

    }
}
