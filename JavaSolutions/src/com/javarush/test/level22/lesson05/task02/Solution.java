package com.javarush.test.level22.lesson05.task02;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Между табуляциями
Метод getPartOfString должен возвращать подстроку между первой и второй табуляцией.
На некорректные данные бросить исключение TooShortStringException.
Класс TooShortStringException не менять.
*/
public class Solution {
    public static String getPartOfString(String string) throws TooShortStringException{
        if (string == null) throw new TooShortStringException();
        if (string.isEmpty()) throw new TooShortStringException();
        ArrayList<Integer> start = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\t");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            start.add(matcher.start());
        }
        if (start.size() < 2) throw new TooShortStringException();
        else return string.substring(start.get(0) + 1,start.get(1));
    }

    public static class TooShortStringException extends Exception {
    }

    public static void main(String[] args) throws TooShortStringException {
        System.out.println(getPartOfString("tab0\ttab\ttab1\t"));       //tab
        System.out.println(getPartOfString("\t\t"));                    //
        System.out.println(getPartOfString("123\t123"));                //Exception
        System.out.println(getPartOfString(null));                      //Exception
    }
}
