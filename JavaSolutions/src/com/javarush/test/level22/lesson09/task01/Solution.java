package com.javarush.test.level22.lesson09.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример содержимого файла
рот тор торт о
о тот тот тот
Вывод:
рот тор
о о
тот тот
*/
public class Solution {

    public static List<Pair> result = new LinkedList<>();


    public static void main(String[] args) throws IOException{

        ArrayList<String> allWords = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName  = reader.readLine();
        FileInputStream fis = new FileInputStream(fileName);
        Scanner scanner = new Scanner(fis);


        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            if (s  != null && !s.isEmpty()) {
                String[] strings = s.trim().split(" ");
                for (int i = 0; i < strings.length ; i++) {
                    allWords.add(strings[i]);
                }


            }

        }
        reader.close();
        fis.close();
        scanner.close();

        for (int i = 0; i < allWords.size()-1 ; i++) {
            for (int j =  i+1 ; j < allWords.size(); j++) {
                StringBuilder perevertush = new StringBuilder(allWords.get(i));
                perevertush.reverse();
                if (allWords.get(j).equals(perevertush.toString())) {
                    Pair pair = new Pair();
                    pair.first = allWords.get(i);
                    pair.second = perevertush.toString();
                    result.add(pair);
                    allWords.remove(j);
                }
            }
        }
        for (Pair x: result) System.out.println(x);

    }

    public static class Pair {
        String first;
        String second;

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
