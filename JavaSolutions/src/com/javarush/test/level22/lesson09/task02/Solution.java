package com.javarush.test.level22.lesson09.task02;

import java.util.HashMap;
import java.util.Map;

/* Формируем Where
Сформируйте часть запроса WHERE используя StringBuilder.
Если значение null, то параметр не должен попадать в запрос.
Пример:
{"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null}
Результат:
"name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'"
*/
public class Solution {


    public static StringBuilder getCondition(Map<String, String> params) {

        StringBuilder result = new StringBuilder();
         String S = " and ";
        if (params != null && !params.isEmpty()){
           for (Map.Entry<String,String> x: params.entrySet()){
                if (result.toString() != null && !result.toString().isEmpty()&& x.getValue()!= null) {
                    result.append(S);
                }
                if (x.getValue()!= null) {

                    String s = String.format("%s = '%s'",x.getKey(),x.getValue());
                    result.append(s);
                }

            }

        }
        return result;
    }
}
