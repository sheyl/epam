package com.javarush.test.level22.lesson09.task03;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
public class Solution {

    public static void main(String[] args) throws IOException{

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        Scanner scanner = new Scanner(new FileInputStream(fileName));
        String text = null;
        if (scanner.hasNextLine()){
           text  = scanner.nextLine();

        }
        reader.close();
        scanner.close();



        //...
        StringBuilder result = getLine(text.trim().split(" "));
        System.out.println(result.toString());
    }

    public static StringBuilder getLine(String... words) {

        if (words == null ||words.length == 0) return new StringBuilder();
        else if (words.length == 1) return new StringBuilder(words[0]);
        else {
            ArrayList<String> allWords = new ArrayList<>();
            for (int i = 0; i < words.length; i++) {
                allWords.add(words[i]);
            }
            for (int i = 0; i < allWords.size() - 1; i++) {
                if (allWords.get(i).toLowerCase().charAt(allWords.get(i).length()-1) == allWords.get(i+1).toLowerCase().charAt(0)) continue;
                else {
                    i = -1;
                    Collections.shuffle(allWords);
                }
            }
            StringBuilder result = new StringBuilder();
            for (String x: allWords) {
                result.append(x);
                result.append(" ");
            }
            return result;
        }
    }
}
