package com.javarush.test.level22.lesson13.task02;

import java.io.*;
import java.nio.charset.Charset;

/* Смена кодировки
В метод main первым параметром приходит имя файла, тело которого в кодировке Windows-1251.
В метод main вторым параметром приходит имя файла, в который необходимо записать содержимое первого файла в кодировке UTF-8.
*/
public class Solution {
    static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing

    public static void main(String[] args) throws IOException {

        String fileName1 = args[0];
        String fileName2 = args[1];

        FileInputStream fis = new FileInputStream(fileName1);
        FileOutputStream fos = new FileOutputStream(fileName2);
        int size = fis.available();
        byte [] buff = new byte[size];
        fis.read(buff);


        Charset uTF8 =  Charset.forName("UTF-8");
        Charset windows1251 =  Charset.forName("Windows-1251");
        String s1 = new String(buff,uTF8);


        buff = s1.getBytes(windows1251);
        fos.write(buff);
        fis.close();
        fos.flush();
        fos.close();
    }
}
