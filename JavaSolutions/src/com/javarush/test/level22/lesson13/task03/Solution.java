package com.javarush.test.level22.lesson13.task03;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Проверка номера телефона
Метод checkTelNumber должен проверять, является ли аргумент telNumber валидным номером телефона.
Критерии валидности:
1) если номер начинается с '+', то он содержит 12 цифр
2) если номер начинается с цифры или открывающей скобки, то он содержит 10 цифр
3) может содержать 0-2 знаков '-', которые не могут идти подряд
4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
5) скобки внутри содержат четко 3 цифры
6) номер не содержит букв
7) номер заканчивается на цифру

Примеры:
+380501234567 - true
+38(050)1234567 - true
+38050123-45-67 - true
050123-4567 - true

+38)050(1234567 - false
+38(050)1-23-45-6-7 - false
050ххх4567 - false
050123456 - false
(0)501234567 - false
*/
public class Solution {
    public static void main(String[] args) {
        /*System.out.println(checkTelNumber("38xx501A34567"));
        System.out.println(checkTelNumber("3805012345"));
        System.out.println(checkTelNumber("+380501234567"));
        System.out.println(checkTelNumber("++380501234567"));
        System.out.println(checkTelNumber("+38(0501234567"));
        System.out.println(checkTelNumber("+38)050(1234567"));
        System.out.println(checkTelNumber("+38(050)1234567"));
        System.out.println(checkTelNumber("+38(05)1234567"));
        System.out.println(checkTelNumber("(3)80501234567"));
        System.out.println(checkTelNumber("(380)501234567"));
        System.out.println(checkTelNumber("380-50123-45"));
        System.out.println(checkTelNumber("(380)501-234567"));
        System.out.println(checkTelNumber("(38-0)501234567"));
        System.out.println(checkTelNumber("380-(501)234567"));
        System.out.println(checkTelNumber("380(50-1)234567"));
        System.out.println(checkTelNumber("380(501)(23)4567"));
        System.out.println(checkTelNumber("+38050123(456)7"));
        System.out.println(checkTelNumber("+38050123(456)76"));
        System.out.println(checkTelNumber("+380501234(567)"));
        System.out.println(checkTelNumber("3-805-0123-45"));
        System.out.println(checkTelNumber("-3805-012345"));
        System.out.println(checkTelNumber("3805-012345-"));
        System.out.println(checkTelNumber("+380501234567"));
        System.out.println(checkTelNumber("+38(050)1234567"));
        System.out.println(checkTelNumber("+38(05)01234567"));
        System.out.println(checkTelNumber("+38(0501)234567"));
        System.out.println(checkTelNumber("+38050123-45-67"));
        System.out.println(checkTelNumber("050123-4567"));
        System.out.println(checkTelNumber("+38)050(1234567"));
        System.out.println(checkTelNumber("+38(050)1-23-45-6-7"));
        System.out.println(checkTelNumber("050ххх4567"));
        System.out.println(checkTelNumber("050123456"));
        System.out.println(checkTelNumber("+38-(050)1234567"));
        System.out.println(checkTelNumber("+38((050)1234567"));
        System.out.println(checkTelNumber("+5(0--5)1234567"));
        System.out.println(checkTelNumber("7-4-4123689-5"));
        System.out.println(checkTelNumber("1-23456789-0"));
        System.out.println(checkTelNumber("+38051202(345)7"));
        System.out.println(checkTelNumber("+38051202(345)-7"));
        System.out.println(checkTelNumber("+-313450531202"));
        System.out.println(checkTelNumber("+313450--531202"));*/
    }

    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null || telNumber.isEmpty()) return false;
        Pattern pattern = Pattern.compile("\\d");
        Matcher matcher = pattern.matcher(telNumber);
        int count = 0;
        while (matcher.find()){
            count++;
        }

        if (count == 12) {
            pattern = Pattern.compile("^[+]{1}\\d*(\\(\\d{3}\\))*\\d*(-?\\d+)?-?\\d+$");
            Matcher matcher1 = pattern.matcher(telNumber);
           return matcher1.matches();

        }
        else if (count == 10) {
            pattern = Pattern.compile("^\\d*((\\d+)|(\\(\\d{3}\\))){1}\\d*(-?\\d+)?-?\\d+$");
            Matcher matcher1 = pattern.matcher(telNumber);
            return matcher1.matches();
        }
        else return false;
    }
}
