package com.javarush.test.level23.lesson04.task01;

/* Inner
Реализовать метод getTwoSolutions, который должен возвращать массив из 2-х экземпляров класса Solution.
Для каждого экземпляра класса Solution инициализировать поле innerClasses двумя значениями.
Инициализация всех данных должна происходить только в методе getTwoSolutions.
*/
public class Solution {
    public InnerClass[] innerClasses = new InnerClass[2];

    public class InnerClass {
    }

    public static Solution[] getTwoSolutions() {
        Solution sol1 = new Solution();
        Solution.InnerClass innerClass1 = sol1.new InnerClass();
        Solution.InnerClass innerClass2 = sol1.new InnerClass();
        sol1.innerClasses[0] = innerClass1;
        sol1.innerClasses[1] = innerClass2;
        Solution sol2 = new Solution();
        Solution.InnerClass innerClass3 = sol2.new InnerClass();
        Solution.InnerClass innerClass4 = sol2.new InnerClass();
        sol2.innerClasses[0] = innerClass3;
        sol2.innerClasses[1] = innerClass4;
        Solution [] array = new Solution[] {sol1,sol2};

        return array;
    }
}
