package com.javarush.test.level24.lesson14.big01;

/**
 * Created by admin on 16.02.2016.
 */
public class Canvas {
    private int width;
    private int height;
    private char [][] matrix;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        matrix = new char[height+2][width+2];

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char[][] getMatrix() {
        return matrix;
    }
    public void setPoint(double x, double y, char c) {
        int x1 = (int) Math.round(x);
        int y1 = (int) Math.round(y);
        if (x1<0 || y1<0 || x1>=matrix[0].length || y1>=matrix.length) {

        }
        else  matrix [y1][x1] = c;

    }
    public void drawMatrix(double x, double y, int[][] matrix, char c){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
               // if (matrix[i][j] == 1) setPoint(i+ x, j+ y,c);
                if (matrix[i][j] != 0) setPoint(j+ x, i+ y,c);
            }
        }
    }
    public void clear() {

        this.matrix = new char[height + 2][width + 2];

        /*for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                matrix[i][j] = 32; // space
            }
        }*/
    }
    public void print() {
        for (int i = 0; i < getHeight()+2; i++) {
            for (int j = 0; j < getWidth()+2; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }
}
