package com.javarush.test.level25.lesson02.task02;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/* Машину на СТО не повезем!
Инициализируйте поле wheels используя данные из loadWheelNamesFromDB.
Обработайте некорректные данные.
Подсказка: если что-то не то с колесами, то это не машина!
Сигнатуры не менять.
*/
public class Solution {
    public static enum Wheel {
        FRONT_LEFT,
        FRONT_RIGHT,
        BACK_LEFT,
        BACK_RIGHT
    }

    public static class Car {
        protected List<Wheel> wheels;

        public Car() {
            try {
                // check 4 wheels
                if (loadWheelNamesFromDB().length !=4) throw new IllegalArgumentException();
                wheels = new LinkedList<>();
                // take array of Wheel
                Wheel[] list = Wheel.values();
                Wheel carWheel = null;
                for (int i = 0; i < loadWheelNamesFromDB().length; i++) {
                    for (int j = 0; j < list.length; j++) {
                        if (list[j].toString().equals(loadWheelNamesFromDB()[i])){
                            carWheel = list[i];
                            break;
                        }

                    }
                    if (carWheel == null) throw new IllegalArgumentException();
                    if (wheels.contains(carWheel)) throw new IllegalArgumentException();
                    wheels.add(carWheel);
                }


            } catch (IllegalArgumentException e) {
                System.out.println("Not a Car!");
            }


        }

        protected String[] loadWheelNamesFromDB() {
            //this method returns mock data
            return new String[]{"FRONT_LEFT", "FRONT_RIGHT", "BACK_LEFT", "BACK_RIGHT"};
        }
    }


}
