package com.javarush.test.level25.lesson05.home01;




public class LoggingStateThread extends Thread {
    private Thread target;
    private State state;
    public LoggingStateThread(Thread target) {
        this.target = target;
        state = this.target.getState();
        setDaemon(true);
        System.out.println(state);
    }

    @Override
    public void run() {
            while (state != State.TERMINATED){

                if (state != target.getState()) {
                    state = target.getState();
                    System.out.println(state);
                }
            }

    }
}
