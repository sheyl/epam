package com.javarush.test.level25.lesson09.task03;

import java.util.ArrayList;

/* Живем своим умом
В классе Solution реализуйте интерфейс UncaughtExceptionHandler, который должен:
1. прервать нить, которая бросила исключение.
2. вывести в консоль стек исключений начиная с самого вложенного.
Пример исключения: new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI")))
Пример вывода:
java.lang.IllegalAccessException: GHI
java.lang.RuntimeException: DEF
java.lang.Exception: ABC
*/
public class Solution implements Thread.UncaughtExceptionHandler {
    public static void main(String[] args){

        Throwable e =  new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI")));
        new Solution().uncaughtException(Thread.currentThread(), e);
    }
    @Override
    public void uncaughtException(Thread t, Throwable e) {

        t.interrupt();
        ArrayList<String> list = new ArrayList<>();
        list.add(0,e.getClass().getName() + ": " + e.getMessage());
        Throwable e1 = e;
        while   (e1.getCause() != null) {

            list.add(0,e1.getCause().getClass().getName() + ": " + e1.getCause().getMessage());
            e1 = e1.getCause();
        }

        for (String x: list) System.out.println(x);
    }
}
