package com.javarush.test.level26.lesson02.task01;


import java.util.Arrays;

/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/
public class Solution {
    public static void main(String[] args) {
        Integer [] mass = new Integer[] {-2,-1,0,1,2};
        Integer [] mass2 = new Integer[] {-2,-1,0,1,2,3};
        for (Integer x: sort(mass)) System.out.print(x + " ");
        System.out.println();
        for (Integer x: sort(mass2)) System.out.print(x + " ");

    }
    public static Integer[] sort(Integer[] array) {
        //implement logic here
        Arrays.sort(array);
        int size = array.length;
        double meridiana;
        if (size % 2 == 0) meridiana = (array[size/2 -1] + array[size/2] )/2;
        else meridiana = array[size/2];
        int buff;
        for (int i = 0; i < size -1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (Math.abs(array[j] - meridiana) < Math.abs(array[i] - meridiana)) {
                    buff = array[i];
                    array[i] = array[j];
                    array[j]= buff;
                }
                if (Math.abs(array[j] - meridiana) == Math.abs(array[i] - meridiana)) {
                    if (array[j] < array[i]) {
                        buff = array[i];
                        array[i] = array[j];
                        array[j]= buff;
                    }
                }
            }
        }
        return array;
    }
}
