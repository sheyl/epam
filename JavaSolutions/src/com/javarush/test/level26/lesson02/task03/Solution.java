package com.javarush.test.level26.lesson02.task03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* Убежденному убеждать других не трудно.
В таблице есть колонки, по которым можно сортировать.
Пользователь имеет возможность настроить под себя список колонок, которые будут сортироваться.
Напишите public static компаратор CustomizedComparator, который будет:
1. в конструкторе принимать список компараторов
2. сортировать данные в порядке, соответствующем последовательности компараторов.
Все переданные компараторы сортируют дженерик тип Т
В конструктор передается как минимум один компаратор
*/
public class Solution {
    public static void main(String[] args) {
        Humen humen1 = new Humen("1",true,5,160);
        Humen humen2 = new Humen("2",true,5,164);
        Humen humen3 = new Humen("3",false,5,164);
        Humen humen4 = new Humen("4",false,12,164);
        Humen humen5 = new Humen("5",false,13,155);
        Humen humen6 = new Humen("6",true,13,161);
        Humen humen7 = new Humen("7",true,13,169);
        Humen humen8 = new Humen("8",true,15,169);
        ArrayList<Humen> list = new ArrayList<>();
        list.add(humen1);
        list.add(humen2);
        list.add(humen3);
        list.add(humen4);
        list.add(humen5);
        list.add(humen6);
        list.add(humen7);
        list.add(humen8);

        Comparator<Humen> sexComp = new Comparator<Humen>() {
            @Override
            public int compare(Humen o1, Humen o2) {
                if (o1.sex == o2.sex) return 0;
                else if (o1.sex & !o2.sex ) return 1;
                else return -1;
            }
        };

        Comparator<Humen> ageComp = new Comparator<Humen>() {
            @Override
            public int compare(Humen o1, Humen o2) {
                return o1.age - o2.age;
            }
        };

         Comparator<Humen> heightComp = new Comparator<Humen>() {
            @Override
            public int compare(Humen o1, Humen o2) {
                return o1.height - o2.height;
            }
        };

        CustomizedComparator<Humen> comparator = new CustomizedComparator<>(sexComp,ageComp,heightComp);

        Collections.sort(list,comparator);

        for (Humen x : list) System.out.println(x.name + " " + x.sex + " " + x.age + " "  + x.height) ;


    }

    public static class Humen {
        String name;
        boolean sex;
        int age;
        int height;

        public Humen(String name, boolean sex, int age, int height) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.height = height;
        }
    }
    public static class CustomizedComparator<T> implements Comparator<T> {
        ArrayList<Comparator<T>> list = new ArrayList<>();
        public CustomizedComparator(Comparator<T>... comparators) {
            for (Comparator<T> x: comparators) list.add(x);
        }

        @Override
        public int compare(T o1, T o2) {
            int result = 0;
            for (Comparator<T> x: list) {
                result = x.compare(o1,o2);
                if (result != 0) break;
            }
            return result;
        }
    }
}
