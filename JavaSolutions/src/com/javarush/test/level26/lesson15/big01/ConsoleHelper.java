package com.javarush.test.level26.lesson15.big01;


import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ConsoleHelper
{
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "common_en");
    static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    public static void  writeMessage(String message){
        System.out.println(message);
    }
    public static String readString() throws InterruptOperationException
    {
        String s = "";
        try {
            s = bufferedReader.readLine();
            if (s.equalsIgnoreCase("EXIT")) throw new InterruptOperationException();
        }
        catch (IOException e) {}
        return s;
    }
    public static String askCurrencyCode() throws InterruptOperationException{
        writeMessage(res.getString("choose.currency.code"));
        String currencyCode;

            currencyCode =  readString().trim();
            while (currencyCode.length() != 3) {
                writeMessage(res.getString("invalid.data"));
                currencyCode = readString().trim();
        }

        return currencyCode.toUpperCase();
    }
    public static String[] getValidTwoDigits(String currencyCode) throws InterruptOperationException{
        writeMessage(String.format(res.getString("choose.denomination.and.count.format"), currencyCode));
        String code = currencyCode;
        String [] result;
        try {
            result = readString().split(" ");
            Integer nominal = Integer.parseInt(result[0]);
            Integer count = Integer.parseInt(result[1]);
            if (nominal <= 0 || count <= 0 || result.length != 2) {
                throw new IOException();
            }
            return result;

        }
        catch (IOException e) {
            writeMessage(res.getString("invalid.data"));
            result = getValidTwoDigits(code);


        }
        return result;


    }
    public static Operation askOperation() throws InterruptOperationException{
        Operation result;
        try {
            writeMessage(res.getString("choose.operation"));
            writeMessage(String.format("Select the operation: 1 - %s, 2 - %s, 3 - %s, 4 - %s",res.getString("operation.INFO"),
                    res.getString("operation.DEPOSIT"),res.getString("operation.WITHDRAW"),res.getString("operation.EXIT")) );
            String op = readString();
            if (checkWithRegExp(op)) result =  Operation.getAllowableOperationByOrdinal(Integer.parseInt(op));
            else throw new IllegalArgumentException();

        } catch (IllegalArgumentException e) {
            writeMessage(res.getString("invalid.data"));
            result = askOperation();
        }
        return result;
    }
    public static void printExitMessage() {
        writeMessage(res.getString("the.end"));
    }

     public static boolean checkWithRegExp(String Name)
    {
        Pattern p = Pattern.compile("^[1-4]$");
        Matcher m = p.matcher(Name);
        return m.matches();
    }
}
