package com.javarush.test.level26.lesson15.big01;




import com.javarush.test.level26.lesson15.big01.exception.NotEnoughMoneyException;

import java.util.*;

public class CurrencyManipulator {
    private String currencyCode;
    public Map<Integer, Integer> denominations = new HashMap<>();


    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;

    }

    public boolean hasMoney() {
        //��� ����� �������� � ���� return denominations.size() != 0;
        return getTotalAmount() > 0;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination)) {
            denominations.put(denomination,denominations.get(denomination)+count);
        }
        else denominations.put(denomination,count);
    }

    public int getTotalAmount() {
        int summa = 0;
        for (Integer key : denominations.keySet()) {
            summa += key * denominations.get(key);
        }
        return summa;
    }
    public boolean isAmountAvailable(int expectedAmount) {
        return expectedAmount <= getTotalAmount();
    }
    public Map<Integer, Integer> withdrawAmount(int expectedAmount) throws NotEnoughMoneyException{

        ArrayList<Integer> list = new ArrayList<>();
        for (Integer key : denominations.keySet()) {
            if (denominations.get(key) != 0 && denominations.get(key) <= expectedAmount) {
                list.add(key);
            }
        }

        Collections.sort(list);
        Map<Integer,Integer> result = null;
        int koli4estvoCupur = 0;
        try {

            for (int i = list.size() - 1; i >= 0; i--) {
                int summa = expectedAmount;
                int countCup = 0;
                TreeMap<Integer, Integer> map = new TreeMap<>(Collections.reverseOrder());

                for (int j = i; j >= 0; j--) {

                    int nominal = list.get(j);
                    int countOfNominals = denominations.get(nominal);
                    int count = summa / nominal;
                    if (count > 0 && count <= countOfNominals) {
                        countCup +=count;
                        summa = summa % nominal;
                        map.put(nominal, count);
                    } else if(count > 0 && count > countOfNominals){
                        countCup += countOfNominals;
                        summa -= nominal * countOfNominals;
                        map.put(nominal, countOfNominals);
                    }

                    if (summa == 0) break;
                }
                if (summa == 0 && (result == null || koli4estvoCupur > countCup)) {
                    koli4estvoCupur = countCup;
                    result = map;
                }
            }
            if (result == null) {
                throw new NotEnoughMoneyException();
            }
            for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
                denominations.put(entry.getKey(), denominations.get(entry.getKey()) - entry.getValue());
            }

        }catch (ConcurrentModificationException e) {

        }
        return result;

    }
}
