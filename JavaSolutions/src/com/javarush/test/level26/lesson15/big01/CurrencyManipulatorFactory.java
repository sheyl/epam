package com.javarush.test.level26.lesson15.big01;



import java.util.Collection;
import java.util.HashMap;

public class CurrencyManipulatorFactory {
    private static CurrencyManipulatorFactory ourInstance = new CurrencyManipulatorFactory();
    private static HashMap<String,CurrencyManipulator> map = new HashMap<>();


    private CurrencyManipulatorFactory() {

    }
    public static CurrencyManipulatorFactory getInstance() {
        return ourInstance;
    }


    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode) {
        for (String i : map.keySet()){
            if (i.equals(currencyCode)){
                return map.get(i);
            }
        }
        CurrencyManipulator manipulator = new CurrencyManipulator(currencyCode);
        map.put(currencyCode,manipulator);
        return manipulator;
    }
    public static Collection<CurrencyManipulator> getAllCurrencyManipulators() {
        return map.values();
    }
}
