package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.CashMachine;
import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.util.ResourceBundle;

class DepositCommand implements Command{
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "deposit_en");
    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));
        //asking currency code (usd,eur,rub)
        String code = ConsoleHelper.askCurrencyCode();
        //entering nominal & count
        String [] validDigits =ConsoleHelper.getValidTwoDigits(code);
        // adding money i the manipulator
        CurrencyManipulator manipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(code);
        manipulator.addAmount(Integer.parseInt(validDigits[0]), Integer.parseInt(validDigits[1]));
        ConsoleHelper.writeMessage(String.format(res.getString("success.format"),manipulator.getTotalAmount(),code) );

    }
}
