package com.javarush.test.level26.lesson15.big01.command;


import com.javarush.test.level26.lesson15.big01.CashMachine;
import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;


import java.util.Collection;
import java.util.ResourceBundle;

class InfoCommand implements Command{
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "info_en");
    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));
        int summ = 0;
        Collection<CurrencyManipulator> list =  CurrencyManipulatorFactory.getAllCurrencyManipulators();
        for (CurrencyManipulator x: list) {

            if (x.hasMoney()) {
                summ++;
                ConsoleHelper.writeMessage(x.getCurrencyCode() + " - " + x.getTotalAmount());
            }
        }
        if (summ == 0) ConsoleHelper.writeMessage(res.getString("no.money"));


    }
}
