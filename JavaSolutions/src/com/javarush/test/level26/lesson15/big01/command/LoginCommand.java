package com.javarush.test.level26.lesson15.big01.command;
import com.javarush.test.level26.lesson15.big01.CashMachine;
import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class LoginCommand implements Command{


    private ResourceBundle validCreditCards = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "verifiedCards");
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "login_ru");
    Enumeration<String> keys = validCreditCards.getKeys();
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));
        Pattern pattern1 = Pattern.compile("^\\d{12}$");
        Pattern pattern2 = Pattern.compile("^\\d{4}$");

        while (true) {
            ConsoleHelper.writeMessage(res.getString("specify.data"));
            String login = ConsoleHelper.readString();
            String passw = ConsoleHelper.readString();
            Matcher m1 = pattern1.matcher(login);
            Matcher m2 = pattern2.matcher(passw);
            if (m1.matches() && m2.matches()) {

                if (validCreditCards.containsKey(login) && validCreditCards.getString(login).equals(passw)){
                    ConsoleHelper.writeMessage(String.format(res.getString("success.format"),login));
                    break;
                }
                else ConsoleHelper.writeMessage("Incorrect login or password.");
            }
            else ConsoleHelper.writeMessage(res.getString("try.again.with.details"));

        }
    }
}
