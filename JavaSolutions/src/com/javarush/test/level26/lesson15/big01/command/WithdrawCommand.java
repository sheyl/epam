package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.CashMachine;
import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;
import com.javarush.test.level26.lesson15.big01.exception.NotEnoughMoneyException;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class WithdrawCommand implements Command{
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "withdraw_en");
    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));

        String code = ConsoleHelper.askCurrencyCode();
        CurrencyManipulator manipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(code);

        Pattern p = Pattern.compile("^[1-9]\\d*$");


        while (true) {
            ConsoleHelper.writeMessage(res.getString("specify.amount"));
            String amount = ConsoleHelper.readString();
            Matcher m = p.matcher(amount);
            if (m.matches()) {
                int summa = Integer.parseInt(amount);
                if (manipulator.isAmountAvailable(summa)) {
                    try {
                        Map<Integer, Integer> map = manipulator.withdrawAmount(summa);
                        for (Integer key : map.keySet()) {
                            System.out.println("\t" + key + " - " + map.get(key));
                        }
                        ConsoleHelper.writeMessage(String.format(res.getString("success.format"),summa,code) );
                        break;
                    } catch (NotEnoughMoneyException e) {
                        ConsoleHelper.writeMessage(res.getString("exact.amount.not.available"));
                    }
                }
                else ConsoleHelper.writeMessage(res.getString("not.enough.money"));
            }
            else ConsoleHelper.writeMessage(res.getString("specify.not.empty.amount"));
        }
    }
}
