package com.javarush.test.no_net.CompareClasses;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by admin on 24.04.2016.
 */
public class ClassRoom {
    ArrayList<Humen> list = new ArrayList<>();

    public static void main(String[] args) {
        ClassRoom room = new ClassRoom();
        room.list.add(new Humen("Dima","Rafalovich",24,191,80));
        room.list.add(new Humen("Dima2","Rafalovich",24,191,80));
        room.list.add(new Humen("Koly","Rafalovich",24,191,80));
        room.list.add(new Humen("Koly","Golubitsky",25,185,90));
        room.list.add(new Humen("Koly","Golubbbitsky",25,185,90));
        room.list.add(new Humen("Kotly","Golubbbitsky",25,185,90));
        room.list.add(new Humen("Kotly","Golubbbitsky",22,183,97));
        room.list.add(new Humen("Kotly","BGolubbbitsky",22,183,97));
        room.list.add(new Humen("Kotly","BGolubbbitsky",92,183,99));
        Collections.sort(room.list,new SortByName());
        for (Humen h : room.list) System.out.println(h.getName() + h.getSurname() + h.getAge());
    }
    static class SortByName implements Comparator<Humen> {
        @Override
        public int compare(Humen o1, Humen o2) {
            String s1 = o1.getName();
            String s2 = o2.getName();
            return  s1.compareToIgnoreCase(s2);
        }
    }

    static class SortBySurname implements Comparator<Humen> {
        @Override
        public int compare(Humen o1, Humen o2) {
            String s1 = o1.getSurname();
            String s2 = o2.getSurname();
            return  s1.compareToIgnoreCase(s2);
        }
    }

    static class SortByAge implements Comparator<Humen> {
        @Override
        public int compare(Humen o1, Humen o2) {
            int age1 = o1.getAge();
            int age2 = o2.getAge();
            return age1 - age2;
        }
    }
    static class SortByHeight implements Comparator<Humen> {
        @Override
        public int compare(Humen o1, Humen o2) {
            double height1 = o1.getHeight();
            double height2 = o2.getHeight();
            return (int) ((height1 - height2)/(Math.abs(height1-height2)));
        }
    }
}
