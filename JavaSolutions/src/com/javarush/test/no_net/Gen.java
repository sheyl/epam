package com.javarush.test.no_net;

/**
 * Created by admin on 25.04.2016.
 */
public class Gen <T> {
    T obj;

    public Gen(T obj) {
        this.obj = obj;
    }
    public T getObj() {
        return obj;
    }
    public void showType() {
        System.out.println(obj.getClass().getName());
    }

    public static void main(String[] args) {
        Gen<Integer> i = new Gen (90);
        System.out.println(i.getObj());
        i.showType();
    }
}
