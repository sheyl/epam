package com.javarush.test.no_net;

/**
 * Created by admin on 26.04.2016.
 */
public class Operator {
    private String name;

    public Operator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
