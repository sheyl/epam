package com.javarush.test.no_net;


import java.util.ArrayList;

/**
 * Created by admin on 26.04.2016.
 */
public class Telephone {
    private ArrayList<TelephoneNumber> numbers = new ArrayList<>();

    //public int getCountByOperator(Operator operator) {
    public int getCountByOperator(BelarusOperators operator) {
        int count = 0;
        for (TelephoneNumber number : numbers) {
            if (number.getOperator().equals(operator)) count ++;
        }
        return count;
    }

    public ArrayList<TelephoneNumber> getNumbers() {
        return numbers;
    }

    public void addTelephoneNumber(TelephoneNumber number) {
        numbers.add(number);
    }

    public static void main(String[] args) {
        Telephone telephone = new Telephone();
        //Operator mts = new Operator("MTS");


        //TelephoneNumber myNumber = new TelephoneNumber("+375-33-601-27-81","Dmitry",mts);
        TelephoneNumber myNumber = new TelephoneNumber("+375-33-601-27-81","Dmitry",BelarusOperators.MTS);
        telephone.addTelephoneNumber(myNumber);

        for (TelephoneNumber number : telephone.getNumbers()) {
            System.out.println(number.getNumber() + " " + number.getOwner());

        }

        //System.out.println(mts.getName() + " " + telephone.getCountByOperator(mts));
        System.out.println(BelarusOperators.MTS + " " + telephone.getCountByOperator(BelarusOperators.MTS));

    }

}
