package com.javarush.test.no_net;

/**
 * Created by admin on 26.04.2016.
 */
public class TelephoneNumber {
    private String number;
    private String owner;
    //private Operator operator;
    private BelarusOperators operator;

    //public TelephoneNumber(String number, String owner, Operator operator) {
    public TelephoneNumber(String number, String owner, BelarusOperators operator) {
        this.number = number;
        this.owner = owner;
        this.operator = operator;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    //public Operator getOperator() {
    public BelarusOperators getOperator() {
        return operator;
    }

    //public void setOperator(Operator operator) {
    public void setOperator(BelarusOperators operator) {
        this.operator = operator;
    }
}
