package com.javarush.test.no_net.clone;

/**
 * Created by admin on 13.06.2016.
 */
public class Airplane implements Cloneable{

    private Engen engen;
    int engenCount;

    public Airplane(Engen engen, int engenCount) {
        this.engen = engen;
        this.engenCount = engenCount;
    }

    public Engen getEngen() {
        return engen;
    }

    public void setEngen(Engen engen) {
        this.engen = engen;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "engen=" + engen +
                ", engenCount=" + engenCount +
                '}';
    }
}
