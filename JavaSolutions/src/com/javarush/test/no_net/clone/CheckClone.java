package com.javarush.test.no_net.clone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 13.06.2016.
 */
public class CheckClone {

    public static void main(String[] args) {
        List<Airplane> airplanes = new ArrayList<>();
        Engen engen = new Engen(25);
        Airplane airplane = new Airplane(engen,2);
        airplanes.add(airplane);
        System.out.println(airplanes);
        List<Airplane> copy = new ArrayList<>(airplanes);


        Airplane plane = copy.get(0);
        Engen engen2 = plane.getEngen();
        engen2.setFuelConsempution(27);

        System.out.println(airplanes);
        System.out.println(copy);

    }
}
