package com.javarush.test.no_net.clone;

/**
 * Created by admin on 13.06.2016.
 */
public class Engen implements Cloneable{

    private int fuelConsempution;

    public Engen(int fuelConsempution) {
        this.fuelConsempution = fuelConsempution;
    }

    public int getFuelConsempution() {
        return fuelConsempution;
    }

    public void setFuelConsempution(int fuelConsempution) {
        this.fuelConsempution = fuelConsempution;
    }

    @Override
    public String toString() {
        return "Engen{" +
                "fuelConsempution=" + fuelConsempution +
                '}';
    }
}
