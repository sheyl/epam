package com.javarush.test.no_net;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Admin on 31.07.2015.
 */
public class summa {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean sExit = false;
        int sum = 0;
        while(!sExit) {
            String s1 = reader.readLine();
            int a = Integer.parseInt(s1);
            sum +=a;
            if (a == -1) sExit = true;
        }
        System.out.println(sum);

    }
}
