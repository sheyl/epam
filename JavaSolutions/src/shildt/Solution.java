package shildt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Admin on 06.09.2015.
 */

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int kolIter = Integer.parseInt(reader.readLine());
        int [] mass = new int [kolIter];
        for (int i = 0; i < kolIter ; i++)
            mass[i] = Integer.parseInt(reader.readLine());
        for(int l = mass.length-1 ; l > 0 ; l--)
        {
            for(int j = 0 ; j < l ; j++)
            {
                if( mass[j] > mass[j+1] )
                {
                    int tmp = mass[j];
                    mass[j] = mass[j+1];
                    mass[j+1] = tmp;
                }
            }
        }

        int maximum = mass[mass.length-1];

        System.out.println(maximum);
    }

}
